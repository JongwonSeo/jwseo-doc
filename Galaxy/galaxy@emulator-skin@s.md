1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)
4.  [Galaxy S Series](/galaxy/emulator-skin/s)

*   Galaxy S7![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S7_Black.jpg)
    
    *   Display5.1 inches (~72.10% screen-to-body ratio)
    *   Resolution1440 x 2560 (~577 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Black.zip)
*   Galaxy S7 Edge![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S7_Edge_Black.jpg)
    
    *   Display5.5 inches(~76.10% screen-to-body ratio)
    *   Resolution1440 x 2560 (~534 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_Black.zip)
*   Galaxy S6![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S6_Black.jpg)
    
    *   Display5.1 inches(~70.48% screen-to-body ratio)
    *   Resolution1440 x 2560 (~577 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_Black.zip)
*   Galaxy S5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S5_Black.jpg)
    
    *   Display5.1 inches(~69.76% screen-to-body ratio)
    *   Resolution1080 x 1920(~432 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_Black.zip)
*   Galaxy S4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S4_Black.jpg)
    
    *   Display5.0 inches(~71.91% screen-to-body ratio)
    *   Resolution1080 x 1920(~441 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_Black.zip)
*   Galaxy S3![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S3_Black.jpg)
    
    *   Display4.8 inches(~65.82% screen-to-body ratio)
    *   Resolution720 x 1280(~306 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_Black.zip)
*   Galaxy S2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S2_Black.jpg)
    
    *   Display4.3 inches(~62.75% screen-to-body ratio)
    *   Resolution480 x 800(~218 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S2_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S2_Black.zip)
*   Galaxy S![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S_Black.jpg)
    
    *   Display4.0 inches(~57.88% screen-to-body ratio)
    *   Resolution480 x 800(~233 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S_Black.zip#)