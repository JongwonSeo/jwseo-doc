1.  Devices
2.  [Galaxy](/galaxy)
3.  [Distribute](/galaxy/distribute/validation-policy#)
4.  [Self-Check List](/galaxy/distribute/self-check-list)

# Self-Check List(Mobile Galaxy)

This document has been prepared for developers who submit Android OS applications for Galaxy Apps. Before submitting your application, we recommend that you check below criteria have been met to reduce the time to get an approval for your application.

*   [Self-Check List Mar 28, 2016](/common/download/check.do?actId=864)

## 1\. Basic Information

1.  _1._
    
    Icon, Screenshot, Description and Tag must be consistent with the application\`s content and adhere to 4+ age rating.
    
    Tip
    
    Icon, Screenshot, Description and Tag should be adhered with 4+ age rating because they are exposed to each and every user without downloading the application.
    
2.  _2._
    
    Default Application Title and Description must be written in English if selecting 2 or more countries for sale.
    
    Tip
    
    You can add Application Title and Description in other 37 languages for global users in Additional Language section on Seller Office.
    
3.  _3._
    
    Description must not include other application store’s Public Relations.
    
4.  _4._
    
    Age Restriction and Category must be correct and valid.
    
    Tip
    
    You can find ‘Age Rating Guide’ from Seller Office.
    
5.  _5._
    
    Application should provide the Help option in the menu if Description does not provide enough explanation to understand the application.
    

## 2\. Installation & Execution

1.  _1._
    The size of apk file\*4MB must not exceed the internal memory of device.
    
    Tip
    
    To download and install an application from Galaxy Apps, internal available memory should be at least the size of the apk file\*4MB.
    
2.  _2._
    
    Application must be installed for the targeted devices without any problems.
    
3.  _3._
    
    Application must be uninstalled as user expects.
    
4.  _4._
    
    Application should be executed without any problems.
    
5.  _5._
    
    Application must be closed without any problems by following options.
    
    1.  1\. Using the exit option in application
        
    2.  2\. Using the Task Manager
        

## 3\. Functionality

1.  _1._
    
    If DRM is applied, application must be activated normally with DRM.
    
2.  _2._
    
    All the menus and functions operate successfully without any problems.
    
3.  _3._
    
    Application\`s resolution should be optimized for the targeted device.
    
    Tip
    
    When application\`s resolution is larger than the device\`s one, the application should provide a scroll bar or fold/unfold function.
    
4.  _4._
    
    If application supports both Portrait and Landscape modes, layouts should automatically adapt to screen with consistent UI when the user rotates the device.
    
5.  _5._
    
    Application should go to sleep mode unless the application requires otherwise (e.g. Navigation, Video, etc.).
    
6.  _6._
    
    If reaction time takes more than 5 seconds, the user should be notified by a message or progress bar.
    
7.  _7._
    
    There should be a function to move to other pages or menus.
    
8.  _8._
    
    If application uses network, there should be a message about additional charges.
    

## 4\. Interruption

1.  _1._
    
    Call Event
    
    1.  _1)_
        
        User should be able to accept/reject a Call without any problems while application is running.
        
    2.  _2)_
        
        Sound of application should stop when the user is on the line.
        
    3.  _3)_
        
        Application should resume after user rejects/ends a Call.
        
2.  _2._
    
    Message(SMS/MMS/IM) Event
    
    1.  _1)_User should get a Message alert while application is running.
    2.  _2)_ Message should be received without any problems.
    3.  _3)_ Application should resume after receiving Message.
3.  _3._
    
    Alarm Event
    
    1.  _1)_
        
        Alarm should work without any problems while application is running.
        
    2.  _2)_
        
        Application should resume after the Alarm is turned off.
        
4.  _4._HOLD KEY
    
    1.  _1)_
        
        Application should stop when user presses HOLD KEY while application is running.
        
    2.  _2)_
        
        Application should resume automatically or by selecting resume menu after release Hold mode.
        
    
    Tip
    
    Music playing application is exception.
    
5.  _5._CENTER KEY
    1.  _1)_
        
        Application should stop and switch to IDLE screen when user presses CENTER KEY while application is running.
        
    2.  _2)_
        
        Application should resume when user re-executes the application.
        
6.  _6._
    
    OTHER KEY  
    Application should operate without any problems when user presses H/W KEY while application is running (BACK, CAMERA, etc.).
    

## 5\. Content Review

1.  _1._
    
    Copyright information must be declared if seller owns the copyright of apk, content(e.g. Music, Images, Brand logo, etc) used in application or have the contract with the copyright holder.
    
2.  _2._
    
    Application must not contain Samsung Brand in Application Title, Icon or Content.
    
3.  _3._
    
    Content must not contain any sexually suggestive things (verbal or graphic).
    
    1.  1) pornography or nudity.
    2.  2) direct/indirect expressions of sexually explicit behavior.
    3.  3) exposed male or female genitalia.
4.  _4._
    
    Content must not contain realistic presentation of violence which could instigate copycat crime.
    
5.  _5._
    
    Content must not incite the use of illegal drugs, alcohol or tobacco.
    
6.  _6._
    
    Content must not contain a realistic presentation of criminal activities such as murder, suicide or prostitution which could instigate copycat crime.
    
7.  _7._
    
    Content must not incite gambling involving real money (including cyber money that can be exchanged with real money).
    
8.  _8._
    
    Content must not have political, ideological, racial, religious or sexual prejudice.
    
9.  _9._
    
    Content must not slander or defame others on the basis of politics, ideology, race, religion or sex.
    
10.  _10._
    
    In case of a 18+ game application for the Korean market, Age rating from GRB must be obtained.
    
11.  _11._
    
    Application must not upgrade itself or download other applications(apk file) using other way without Galaxy Apps(Other application store or illegal download).
    
12.  _12._
    
    Re-exam
    
    1.  1) Check if all the defects from the last submission are fixed.
    2.  2) Corrections should be mentioned in ‘Comments to Apps Review Team’ section.
13.  _13._
    
    Revision
    
    1.  1) Revised or added functions should be explained in Description and ‘Comments to Apps Review Team’ section.
    2.  2) Application should be updated to the revised version from the Galaxy Apps Store.

## 6\. Usability

1.  _1._
    
    Application should be designed to be valuable, entertaining, unique or informative. It makes user feel unpleased or noninformative if application just offers the simple web site links or puzzles which only changed puzzle board color.
    
    Tip
    
    Before submitting application, you may know how interesting or useful it is by letting your friends or families use it. If they say that your application is fun and worth to use, so do Galaxy Apps users.
    
2.  _2._
    
    Application should minimize user effort.
    
    1.  1) It is the best to make user learn your application without help or manual. If manual is required, it should be described in a easy-to-understand manner.
    2.  2) User interface should not be complex and it should provide consistent experiences.
3.  _3._
    
    Application should be designed by considering aesthetic factors.
    
    1.  1) User interfaces in application should be arranged harmoniously.
    2.  2) Application should not contain any overlapped or truncated text, graphics distortion, or any kinds of display errors.
    3.  3) Application image should be clearly visible on devices with high resolution.

## 7\. Recommendation

1.  _1._
    
    It should be possible to reinstall the deleted application on the device.
    
2.  _2._
    
    Application screen should be loaded in reasonable time. If loading takes too long on the Splash screen, the application should notify the user that the loading is in progress.
    
    Tip
    
    The user should be notified about loading status through a message or a progress bar.
    
3.  _3._
    
    Application should work normally when executing and terminating the application three times in a row.
    
4.  _4._
    
    Menu name should be related to content to let the user identify application functions through menu name.
    
5.  _5._
    
    Software Keys should be presented in a same way depending on the function. If executing menu is on the left side, other executing menus should be on left side as well.
    
    Tip
    
    OK, SAVE, GO, CONFIRM means execution.  
    BACK, CANCEL, IGNORE means going back to the previous page or returning to the previous status.
    
6.  _6._
    
    There should be no data loss when application uses the data linkage ways. (e.g. Phone number, Message, Call log, Calendar, Media file)
    
7.  _7._
    
    Application should work without any problems when Low Battery popup appears while application is running.