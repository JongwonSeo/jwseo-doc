1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)
4.  [Others](/galaxy/emulator-skin/others)

*   Galaxy A5 (2016) ![new](http://developer.samsung.com/sd2_images/common/text/new.png)
    ![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A5_2016_Black.jpg)
    
    *   Display5.2 inches(~72.5% screen-to-body ratio)
    *   Resolution1080 x 1920(~424 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_Black.zip)
*   Galaxy A3 (2016) ![new](http://developer.samsung.com/sd2_images/common/text/new.png)
    ![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A3_2016_Black.jpg)
    
    *   Display4.7 inches(~69.4% screen-to-body ratio)
    *   Resolution720 x 1280(~312 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_Black.zip)
*   Galaxy A8 (2016)![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A8_2016_Black.jpg)
    
    *   Display5.7 inches(~73.80% screen-to-body ratio)
    *   Resolution1080 x 1920(~386 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_Black.zip)
*   Galaxy A7 (2016)![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A7_2016_Black.jpg)
    
    *   Display5.5 inches(~74.30% screen-to-body ratio)
    *   Resolution1080 x 1920(~401 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_Black.zip)
*   Galaxy A9![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A9_Gold.jpg)
    
    *   Display6.0 inches(~75.90% screen-to-body ratio)
    *   Resolution1080 x 1920(~367 ppi pixel density)
    *   Color[gold](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_Gold.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_Gold.zip)
*   Galaxy A7 (2015)![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A7_2015_Black.jpg)
    
    *   Display5.5 inches(~72.50% screen-to-body ratio)
    *   Resolution1080 x 1920(~401 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_Black.zip)
*   Galaxy A5 (2015)![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A5_2015_Black.jpg)
    
    *   Display5.0 inches(~71.00% screen-to-body ratio)
    *   Resolution720 x 1280(~294 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_Black.zip)
*   Galaxy A3 (2015)![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_A3_2015_Black.jpg)
    
    *   Display4.5 inches(~65.50% screen-to-body ratio)
    *   Resolution540 x 960(~245 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_Black.zip)
*   Neo![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_Neo_Black.jpg)
    
    *   Display3.5 inches(~52.90% screen-to-body ratio)
    *   Resolution480 x 800(~267 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Neo_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Neo_Black.zip)
*   Gio![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_Gio_Black.jpg)
    
    *   Display3.2 inches(~48.07% screen-to-body ratio)
    *   Resolution320 x 480(~180 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Gio_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Gio_Black.zip)
*   Ace![](http://developer.samsung.com/sd2_images/galaxy/emulator/Galaxy_Ace_Black.jpg)
    
    *   Display3.5 inches(~54.08% screen-to-body ratio)
    *   Resolution320 x 480(~165 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Ace_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Ace_Black.zip)