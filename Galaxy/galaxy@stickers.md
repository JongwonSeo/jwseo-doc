1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Samsung Stickers](/galaxy/stickers)

[EN](/en/galaxy/stickers)[KR](/kr/galaxy/stickers)[CN](/cn/galaxy/stickers)

# Samsung Stickers

The Samsung Stickers app provides users in over 185 countries around the world with stickers that can be used in a variety of Samsung apps.

Applying to become Developers:

*   Applying date will be announced later.

#### Overview

## What is Samsung Stickers?

Samsung Galaxy devices support stickers that can be used across a variety of the Samsung apps installed on them. They also offer Samsung Mobile Sticker Editor which can be used to create stickers in the common format so that they can be applied in all the Samsung apps that support stickers.

The apps in which stickers created via Samsung Mobile Sticker Editor can be used are as follows:

![Sticker following ](https://developer.samsung.com//sd2_images/galaxy/content/stickers_landingpage_01.png)

*   Call Stickers
    These stickers can be used when making an outgoing call or sending a call-decline message in the Call app.
    
*   Stickers
    These stickers can be used commonly in the Contacts, Messages, and Calendar apps, and are available in two types: animated GIFs and static PNGs.
    
*   Stamps
    These stickers can be used when editing images and videos in the Gallery app.
    
*   Live Stickers
    These animated stickers can be used while using the Camera app.
    

## Samsung Mobile Sticker Editor

This authoring tool, which is used exclusively for creating stickers, helps designers, even those without software development experience, create stickers easily.

#### Main Characteristics of Samsung Mobile Sticker Editor

1.  It ensures intuitive usability.
    *   You can add multiple images at once via Multiple Images.
    *   You can check changes immediately after editing via Preview.
2.  It supports the creation of a diverse range of stickers, including:
    *   Call Stickers that can be used in the Call app;
    *   Stickers that can be used in the Messages, Contacts, and Calendar apps;
    *   Stamps that can be used in the Photo/Video Editor app; and
    *   Live Stickers that can be used in the Camera app, which can be used in a variety of Samsung apps.
3.  It helps with easy registration in Galaxy Apps.
    *   It creates stickers in .APK format so that you can register them in Galaxy Apps.

#### Screen Layout of Samsung Mobile Sticker Editor

1.  A Sticker list, which shows the types of stickers that can be developed, is displayed.
    
2.  Fields to which stickers are applied are displayed on the screen in detail.
    
3.  On this edit screen, desired images can be added or edited for each field.
    

Request Process

*   Sign up for  Samsung Account
    
    [Non-Samsung Account users](/signup) [sign up](/signup)
*   Partnership Request  Submission
    
    Fill out and submit required information
*   Review   
    
    Review portfolios
*   Content Provision Agreement &  Registration in Stores
    
    Create stickers via Editor and register them in stores

FAQ

<table class="sd2_table"><colgroup><col width="215px"> <col> </colgroup><thead><tr><th>Question</th><th>Answer</th></tr></thead><tbody><tr><td>Is anyone who wants to develop stickers allowed to create one?</td><td>Only those who have been selected through an internal review are allowed to develop and register stickers.</td></tr><tr><td>How do I apply to become a Developer?</td><td>Applications open on the third Wednesday of every odd month and remain open for two weeks. You can submit your Developer application on this page (Get Access button).</td></tr><tr><td>What can I submit as my portfolio?</td><td>You can attach stickers you have developed as a .JPG, .PDF, or .ZIP file, or provide a URL. You may also submit any previous portfolios and designs such as skins, themes, illustrations, photos, and icons.<br>? Make sure that you attach files saved in image format (e.g., .PNG, JPG), .PDF format, or .ZIP archive. If you provide a cloud URL, such as Dropbox or Google Drive, we may be unable to review your portfolio.</td></tr><tr><td>What does the review process involve and how long does it take?</td><td>We review portfolios using comprehensive sticker development criteria such as creativity and originality. The result will be sent to the email address you used to submit your application. It may take up to a month for us to notify you of the review result.</td></tr><tr><td>Is it possible to be excluded from a review?</td><td>Your application may be excluded from a review in the following cases: if you wish to develop stickers for personal use; if you do not submit a portfolio; if you submit an image not created by you; or if you submit the same portfolio multiple times.</td></tr><tr><td>What is Content Provision Agreement?</td><td>In this step, before registering your sticker in stores, you need to agree to the scope within which the content is provided and used on Samsung devices. You will proceed with this via email after the review.</td></tr></tbody></table>

## Restrictions

From the Galaxy Note 8 onwards, this app will be installed on Galaxy devices running on the Android Nougat OS or later. As of November 2017, the service is available in 185 countries, including 99 countries that provide it as a paid service, and it will gradually be extended.

*   The app may not be supported on some devices running on the Android Nougat OS under certain circumstances, such as the device having limited specifications.
*   Live Stickers (for the Camera app) may not be available on some devices depending on the device specifications.

Stickers that are developed using Samsung Mobile Sticker Editor can only be provided via Galaxy Apps.

Samsung Electronics does its best effort best to protect the copyright of stickers provided through this service, it may still have any kind of copyright issue in some apps.

A guide of the copyright issue will be provided in Samsung Mobile Sticker Editor or Galaxy Apps Seller office and your agreement can be requested.

Available Devices

 [![Galaxy](https://developer.samsung.com//sd2_images/services/icon_btn_01_galaxy.png)](/galaxy) [Galaxy](/galaxy)[](/galaxy)