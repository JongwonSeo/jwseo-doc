1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)
5.  [Technical Document](/galaxy/edge#techdocs)
6.  [How to Use the Edge Simulator](/galaxy/edge/how-to-use-edge-simulator)

# How to Use the Edge Simulator Mar 23, 2017

*   [ProgrammingGuide\_Edge\_Simulator\_v1.4.0.pdf](/common/download/check.do?actId=1049)

This document contains information on how to use the Edge Simulator which helps enable developers in creating edge application.

The edge simulator

*   **Supports development of the edge single plus, feeds mode and other features.**
    
*   **Supports Android Studio(Eclipse) Virtual device.**
    
*   **Supports any Android devices with M OS.**
    

![Samsung Printing App Center](https://developer.samsung.com//sd2_images/galaxy/content/post_look_simulator01.jpg)
![ Samsung Printing App Center](https://developer.samsung.com//sd2_images/galaxy/content/post_look_simulator02.jpg)