1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)
5.  [Technical Document](/galaxy/edge#techdocs)
6.  [Release Note](/galaxy/edge/releases)

# Release Note Mar 23, 2017

## Introduction

*   #### Release Version
    1.4.0
    
*   #### Release Date
    Mar 23, 2017
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126"> <col width="123"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Look SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Look APIs, see the <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/look/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, Hello applications, and feature descriptions.</td></tr><tr><th>Tools</th><td class="sub_th">Edge Simulator</td><td>Helps enable developers to create Edge Application with any Android device or Android Virtual Device.</td></tr></tbody></table>
  

## Change History

*   *   Added a new API to set pull to refresh action for the Edge screen.*   Added new APIs to set long click pending intent.
    *   Deprecated, S-pen SDK (SmartClip, PointerIcon). Will be deprecated in N OS as concept of changes and lack of usage.
    *   Removed, deprecated S-Pen SDK (AirButton, WrittingBuddy).
    
*   *   Updated open source announcement
*   *   Added an Edge simulator.
*   *   Added a new action for the Cocktail Provider and an attribute in Cocktail-Provider-Info, implementing the Edge Single Plus panel.*   Added a new API to describe the Edge Single Plus panel.
    *   Added a search filter for the Edge Single Plus panel for Galaxy Apps.
    
*   *   Released enhanced width stability and better performance.
*   *   Added the COCKTAIL\_PANEL feature for stand-alone applications.*   Added a category attribute in CocktailProviderInfo for the feeds panel.
    *   Added a search filter for the feeds panel on Galaxy Apps.
    
*   *   Changed the behavior of Slook that checks the Android version before checking a feature or enhanced stability.*   Released with enhanced stability and better performance.
    
*   *   Removed support for 'Menu List'(UI\_TYPE\_MENU) AirButton for the Note 4 and Note Edge due to a concept change. See sample APK. (3. Air Button > 1. Simple > Menu, 3. Air Button > 3.SurfaceView, 3. Air Button > 4. SurfaceView-Manual)*   Changed how the 'Menu List'(UI\_TYPE\_MENU) opens from pressing the 'sidekey' to 'hovering' for improved usability for the Note 4 and Note Edge.
    
*   *   Added CocktailBar in order to develop a display panel with the Edge screen.*   Added a search filter for the Edge screen’s panels on Galaxy Apps.
    
*   *   Separated the Look from the Samsung Mobile SDK to make an efficient environment for application development.*   Added a new required permission when initializing Look. Refer to the programming guide for more information.
    
*   *   To be compatible with update Android 4.4 policy, permissions are added in the AndroidManifest file of sample.

## Known Issues

Currently none.

## Features

*   Specialized widgets for expanded functions for the Android View System.
*   Edge, SmartClip, PointerIcon.