1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)

# Edge(Look)

## Essential Contents

*   [Edge(Look) SDK Version : 1.4.0Mar 23, 2017](/common/download/check.do?actId=1045)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/look/index.html)
*   [Release Note](/galaxy/edge/releases)
*   [Programming Guide : SPen](/galaxy/edge/spen-guide)
*   [Programming Guide : Edge](/galaxy/edge/edge-guide)
*   [How to Use the Edge Simulator](/galaxy/edge/how-to-use-edge-simulator)
*   [UX\_Design Guideline](/galaxy/edge/ux-design-guideline)

Note

1) Edge applications (API 22 or lower) should be republished to Mashmallow (API 23) as Android 6.0 changes the Runtime permissions and the Android Marshmallow policy.  
2) Look SDK v.1.3.0 supports new UI styles to be developed on Edge applications with richer UIs.  
3) The S-Pen features in the Look SDK(AirButton, SmartClip, WritingBuddy, PointerIcon) will be deprecated from Android 7.0 Nougat. Accordingly, the AirButton and the WritingBuddy features will be deleted starting from the Look SDK 1.4.0. In addition, the SmartClip and the PointerIcon will be not available as of 30th of June, 2017. Please note that the call of the features have no action any more from Android 7.0 Nougat. 
![news](https://developer.samsung.com//sd2_images/common/text/new.png)

## Overview

Edge(Look) offers specialized widgets and service components for extended functions of the Samsung Android devices.  
![](https://developer.samsung.com//sd2_images/galaxy/content/look03_edge.png)   
Edge(Look) supports the following functions:

*   Edge
    *   Edge Single Mode
    *   Edge Single Plus Mode
    *   Edge Feeds Mode
    *   Edge Immersive Mode (will be deprecated in N)
*   S-Pen
    *   SmartClip (will be deprecated in N)
    *   PointerIcon (will be deprecated in N)

#### Edge

![Figure 1: Edge Mode](https://developer.samsung.com//sd2_images/galaxy/content/sms_look_01_20160203.png) 

<center>Figure 1: Edge Mode</center>

From Galaxy S6 Edge M OS, Edge Feed mode and Edge Single Plus mode are supported and this feature has been added to Edge(Look) SDK 1.3.0. For more information, please refer to this document:  
['\[Samsung\_Galaxy edge\] UI\_Design Guideline\_v1.2'](http://img-developer.samsung.com/contents/cmm/sms/pdf/Edge_UI_Design_Guideline_v1.2.pdf)

##### Edge Single Mode

The Application for Edge Single Mode is viewed on the Edge(curved) screen area. You can publish one with the cocktail provider, which is added in the Edge screen area. You can enable some Edge Single Modes in Edge Settings, and change them through the revolving action on Edge service.

##### Edge Single Plus Mode

Edge Single Plus mode is similar with Edge Single Mode, but can provide many contents using the wide UI. Edge(Look) v1.3.0 supports the options where you can define the variable width and API which can describe the Edge Single Plus mode.

Edge Single Plus mode is supported in Galaxy S6 Edge M OS, and Galaxy S6 Edge+ M OS and Galaxy S7 Edge M OS.

Note

Multiple Single Plus Panel can be supported for one packages but Single Mode(160 px) will be hidden automatically if App support Single Plus Panel.

##### Edge Feeds Mode

Edge Feeds Mode is similar with Edge Single Mode but with simpler information.

##### Edge Immersive Mode

You can use the extra edge screen as a sub-window for the main activity to offer wider views.

A screen with a 16:10 expanded view is provided by arranging components of each screen as Edge screen area. This is provided while a specific Application is running, or in a specific screen of the Application and once you get out of the screen or application, this automatically disappears.

Note

Edge Single mode, Edge Single Plus mode, or Edge Feeds mode developed using the SDK may appear on the Lock and Cover of the device. Personal information may be displayed on the application. For privacy reasons, caution is advised.

Note

When you submit an Edge application into Seller Office, please make sure of the following guidelines.

1.  1) Only the same mode of Edge binaries can be registered under one content ID.
    
2.  2) For the multiple registrations, General binaries and Edge binaries cannot be registered at the same time.
    
3.  3) Edge function cannot be added to the registered general application.
    

#### S-Pen

Note

S-Pen SDK will be deprecated from N OS because of concept changes and lack of usage. Please be mindful in using it.

##### SmartClip

SmartClip allows you to capture and extract metadata such as texts and URLs and to crop screenshots using S-Pen. When you point your S-Pen near the target view, press the side button, and select Smart Select. As seen in Figure 3, you can draw on the screen to collect Metadata from the Android View. In addition to simple screen captures, you can also collect information from the contents displayed on the screen and any additional information provided by the application. The information can be converted into text. You can search the clipped contents or send the clipped texts to other applications. You can use the Pinboard application to view or manage clipped contents.

![Figure 3: SmartClip S-Pen crop](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Look_03.jpg) 

<center>Figure 3: SmartClip S-Pen crop</center>

![Figure 4: Metadata management](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Look_04.jpg) 

<center>Figure 4: Metadata management</center>

Images and texts clipped by S-Pen are extracted from the screen. Internal information is not extracted if the application uses CustomView for screen configuration. The Edge(Look) API allows you to provide additional information.

For example, the URL or Deep Link provided for clipping from the application screen allows easier access to the URL or application.

You can directly add metatags to view or register a callback in order to add metadata when the screen is clipped

##### PointerIcon

PointerIcon changes when you place S-Pen on View. For example, you can configure the selected color icon from the drawing application to indicate the current status as displayed in Figure 5.

![Figure 6: PointerIcon](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Look_05.jpg) 

<center>Figure 5: PointerIcon</center>

#### Restrictions

Edge(Look) has the following restrictions:

*   All Edge(Look) API functions require S-Pen excluding Edge.
*   These functions are only available on devices that support Edge(Look).

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/look/index.html)
*   [Release Note](/galaxy/edge/releases)
*   [Programming Guide: Spen](/galaxy/edge/spen-guide)
*   [Programming Guide: Edge](/galaxy/edge/edge-guide)
*   [How to Use the Edge Simulator](/galaxy/edge/how-to-use-edge-simulator)

## Samples

*   [Edge(Look)Sample App](/common/download/check.do?actId=1048)

## FAQ

*   Q01 A
    You have to check if the device supports the Cocktail feature or not. You can check it with Slook instance as shown below :
    
        boolean slookSupported = true;
        Slook slook = new Slook();
        try {
           slook.initialize(this);
        } catch (SsdkUnsupportedException e) {
           // the device unsupporting  Slook
           slookSupported = false;
        }
        if (slookSupported  && Slook.isFeatureEnable(Slook.COCKTAIL_BAR)) {
           setContentView(R.id.main_layout_for_cocktail);
           SlookCocktailSubWindow.setSubContentView(activity, R,id.sub_layout);
        } else {
           setContentView(R.id.main_layout);
        }
        
    
*   Q02 A
    The Cocktail of the bar type should have a vertical layout and a horizontal layout. If you try to implement a ListView in RemoteViews, you have to consider implementing a ListView for the horizontal layout. Unfortunately, Android Framework doesn’t support HorizontalListView as Widget so Samsung Framework included a HorizontalListView widget for RemoteViews. You just add the layout xml implementing HorizontalListView in the layout-land folder of resources. Please refer below :
    
    /res/layout/layout.xml  
    
        <ListView android:id="@+id/widgetlist" android:layout_width="match_parent" android:layout_height="match_parent" />
        
    
    /res/layout-land/layout.xml  
    
        <HorizontalListView android:id="@+id/widgetlist" android:layout_width="match_parent" android:layout_height="match_parent" />
        
    
*   Q03 A
    The Look package depends on a Samsung implementation in the Android Framework’s View System. This means that the Look package is only supported in devices with this implementation.
    
*   Q04 A
    The Look functions will not work on the device, but there will be no build errors or exceptions.
    
*   Q05 A
    It appears when a Pen event occurs while S Pen hovers over the view.
    

[1](#1) [2](#2) [3](#3)

## FAQ

*   Q06 A
    Set the gravity value to GRAVITY\_HOVER\_POINT.
    
*   Q07 A
    In EditText, use the setImageWritingListener method to get the handwritten object as an image. This method is supported only in EditText.
    
*   Q08 A
    You can check it in the Scrapbook application.
    
*   Q09 A
    If the ItemSelectListener method has not been used to hide AirButton, check itemIndex. If the value is -1, then AirButton is hidden.
    
*   Q10 A
    AirButton must be hidden when rotating.
    

[1](#1) [2](#2) [3](#3)

## FAQ

*   Q11 A
    If your CustomView inherits from ViewGroup, then you can use WritingBuddy directly. If not, add a framework for the exterior and use WritingBuddy from a FrameLayout.
    
*   Q12 A
    The result is same. The only difference is when the data is added. The addMetaTag method adds the data when it is called and the setDataExtractionListener method adds the data when the area is clipped.
    

[1](#1) [2](#2) [3](#3)

## Videos

*   
*   

## Event Materials

*May 26, 2016Samsung Developer Conference 2016 - Mobile&Health    
    *   [HEA-19\_Fully\_Realizing\_the\_Power\_of\_S\_Health\_Data\_and\_Service.pdf (2.17MB)](/common/download/check.do?actId=13)
    *   [MBL-36\_Samsung\_Professional\_Audio\_meets\_Android\_Pro-Audio.pdf (4.53MB)](/common/download/check.do?actId=14)
    *   [MBL-50\_What’s\_new\_in\_Samsung\_Galaxy.pdf (2.35MB)](/common/download/check.do?actId=12)
    *   [MBL-60\_Don’t\_Lose\_App\_Revenue\_Over\_Poor\_Customer\_Experience.pdf (2.46MB)](/common/download/check.do?actId=11)
    *   [MBL-61\_Profiling\_Apps\_for\_Better\_Performance\_on\_Samsung\_Devices.pdf (2.38MB)](/common/download/check.do?actId=10)
    *   [MBL-74\_Exploring\_the\_Web\_and\_VR\_with\_Samsung\_Internet.pdf (1.47MB)](/common/download/check.do?actId=9)
    
*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.93MB)](/common/download/check.do?actId=21&statPath=/event/look/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/look/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/look/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.94MB)](/common/download/check.do?actId=24&statPath=/event/look/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/look/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.79MB)](/common/download/check.do?actId=26&statPath=/event/look/download/MWC_SESSION_Group_play_V3.pdf)