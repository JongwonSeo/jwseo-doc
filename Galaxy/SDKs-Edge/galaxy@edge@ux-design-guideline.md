1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)
5.  [UX Guidelines](/galaxy/edge/ux-design-guideline)

# UI Design Guideline

## 1\. Edge screen panels

#### 1-1. Basic interactions

Users can open panels through the handle while the screen is turned on. The panels are looped by the order which the users have set. When the users tap the contents in the panel, the connected link is shown. To close the panel, users will click outside of the panel area.

Users can edit each panel through the Edge panel settings. To edit panels, Edit icons are provided for each panel. The Edit icons show the corresponding setting screen of the panels.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_01.png)

#### 1-2. Design principles

The Edge UX allows users to quickly access frequently used applications and information through panels.

Panels can always be called regardless of the application currently opened. Panels can be used independently. The items are displayed in the order that the users have set.

*   **Quick access type**  
    This is a Bridge. It is the access path of frequently used tasks or applications. It allows users to access tasks from any screen.
    
*   **Quick view type**  
    This is an Overlay widget. It allows users to access and check information at any time without having to open applications.
    
*   **Quick control type**  
    This is a Quick controller. It allows users to control tasks without having to open applications.
    

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_02.png)

#### 1-3. UI overview

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_03.png)

Panels provide contents which users have to check concisely and clearly. When a panel is opened, the previous screen dims and the user cannot interact with it.

##### 1\. Panel title

The title of the currently shown panel is displayed. It can display text or an image. If the panel is connected to an application, the “>” symbol is displayed next to the title. By tapping “>”, the corresponding application is opened.

##### 2\. Contents area

Contents can be arranged freely in the 138 dp according to content type. By default, the orientation of text and images is portrait. When the contents exceed the content screen, users can scroll up and down. To finish the task immediately through panels, additional depth of panel is not recommended.

##### 3\. Panel contents link

Pre-defined actions are provided.

##### 4\. Sub information area

Whenever there are other meaningful or additional information for a panel, a ‘Sub Information’ is displayed on the sub area. Contents of the ‘Sub information’ is composed of a summary, recommendation and related information to the main information, which is always useful to user. Sub area consists of the ‘Header’ and ‘Contents'. Since \`Sub information\` is not required, this area will not be displayed whenever there is no \`Sub information\`.

In consideration of the ‘Panel’ and ‘One handed operation’, the interaction on the \`Sub information\` section is minimal. Moreover, in order to concentrate on major panel, the hierarchy of the 'Main information' and ‘Sub information’ is distinguished by visual division and amount of information. If there is a large amount of information, It can be scrolled. Examples are shown below.

1.  _4.1_
    
    Help area: Displays general description or instruction about the panel.
    
2.  _4.2_
    
    Notification area: Displays main information that user should not miss.
    
3.  _4.3_
    
    Related information area: Displays a summary of the main information or other related information.
    
4.  _4.4_
    
    Category area: It is used as tab to switch contents of the panel.
    

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_04.png)

##### 5\. Interaction (Pull to refresh)

Drag down the uppermost area with one hand quickly whenever you want to update or refresh the information of the panel.

##### 6\. Interaction (Long-press)

Doing a Long-press action onto an item of the panel may provide popup with detailed information, actions, or shortcuts related to the item. Examples are shown below.

1.  _6.1_
    
    Deep link: Displays detail function which is related to the contents (Shortcut).
    
2.  _6.2_
    
    Preview: Provides preview that is related to the contents (Shortcut).
    
3.  _6.3_
    
    Customize: Provides settings that is related to the panel..
    

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_05.png)

#### 1-4. Design Guidelines

We recommend following these guideline for an effective usage of your panel.

##### Main area

It should contain summarized information so that user can identify information at a glance. Items must be converged in the panel to achieve visual balance.. Layout should be arranged in such a way that it could be easily checked and accessed within the 550px area.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_06.png)

##### Sub area

Sub area should be simpler than the main area.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_07.png)

##### Panel setting preview

Preview image should be effectively display characteristics and concepts in order for the panel's preview image to be distinguishable. Preview area should be accurate and images should be clear to effectively convey the panel's function.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_08.png)

#### 1-5. Visual style

##### Density

It is highly recommend that Edge panels should conform to the defined size and form below. Use the following table as guide in defining the size and form of your panel with respect to screen density level.

<table class="sd2_table sd2_mt_20"><colgroup><col width="23%"> <col> <col> <col> </colgroup><thead><tr><th colspan="4" scope="col">DENSITY</th></tr></thead><tbody><tr><td></td><td>560dpi (sw411dp)</td><td>640dpi(sw360dp)</td><td>720dpi(sw320dp)</td></tr><tr><td>Screen width x height(DP)</td><td>411 x 731</td><td>360 x 640</td><td>320 x 569</td></tr><tr><td>Edge panel width x height(DP)</td><td>158 x 731</td><td>138 x 640</td><td>123 x 569</td></tr></tbody></table>

##### Panel Style

Make the layout of the main area like those shown in below images. It is possible to combine panel layout concepts as you desire.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_09.png)
![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_10.png)
![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_11.png)

##### Font style

Choose among the font sizes shown in below images with consideration to readability and panel width.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_12.png)

## 2\. Edge screen feeds

#### 2-1. Basic interactions

If users swipe upwards and downwards on the Edge screen when the screen is off, the feed is displayed. The feeds are looped by the order which the users have set. When the users tap the contents in the feed, the connected link is shown. The feed is displayed only within the timeout settings (Edge feed timeout). If there is no interaction on the feed, the feed disappears after the timeout.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_13.png)

#### 2-2. Design principle

Feeds are displayed as a shortened form of the information which is needed to be checked frequently and quickly. Feeds allow users to check information immediately without turning on the screen. Information displayed is updated consistently to help minimize extra controls.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_14.png)

*   **Glance view type**  
    Glance view allows users to check useful information at any time. The latest information can be checked without launching applications. It is composed of concise information and users are able to see them in a glance. If there are a lot of information, users can scroll through them. The latest information is on top.
    
*   **Information (Possible to provide):** Information which is updated continuously.  
    (ex. News, SNS feed, Weather, Notification, etc)
    
*   **Information (Impossible to provide):** Information which is static or cause additional action.  
    (ex. Clock, User contents, Music control, etc)
    

#### 2-3. UI overview

Feeds are displayed as a shortened form of information which needs to be checked frequently and quickly. Panels can be displayed when the screen is off. The information is only displayed on the Edge screen area.

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_15.png)

##### 1\. Feed title

The feed title is marked with an icon. Upon tapping the icon, the contents settings view or the relevant application will be shown.

##### 2\. Contents area

It is recommended to display contents and information which can be checked quickly. When there are plenty of content, they are provided through a stream. It is recommended to provide a separate navigation button. When you hold and move the contents area from left to right, the contents are refreshed. While they are refreshed, the icon area temporarily changes to the refresh icon. Black GUI is recommended because the information stream works only when the screen is turned off.

#### 2-4. Visual styles

![](https://developer.samsung.com//sd2_images/galaxy/content/edge_ux_16.png)