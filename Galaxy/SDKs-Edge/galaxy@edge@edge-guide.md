1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)
5.  [Technical Document](/galaxy/edge#techdocs)
6.  [Programming Guide: Edge](/galaxy/edge/edge-guide)

# Programming Guide : Edge Mar 23, 2017

*   [Edge(Look) Edge Programming Guide](/common/download/check.do?actId=1047)

Edge(Look) offers specialized widgets that extend the Android View System to make it more usable, visible, and intuitive.

Edge(Look) supports the following features:

*   **Edge**
    
    *   **Edge Single Mode** is a stand-alone view on the Edge(Curved) screen area \[Figure 1\].
    *   **Edge Single Plus Mode** is a stand-alone view and provides many contents via wide UI. \[Figure 1\].
    *   **Edge Feeds Mode** is similar with Edge Single Mode, but with simpler information \[Figure 1\].
    *   **Edge Immersive Mode** is a mode where the main activity uses Edge(Curved) screen area as sub-widow \[Figure 1\].

![Edge Mode](https://developer.samsung.com//sd2_images/galaxy/content/post_look_s6_edge_mode_01_20160125.jpg) 

<center>Figure 1: Edge Mode</center>

*   **Overlaid Edge Screen Style:** Device with a main screen like a normal device.
    
*   **Extra Edge Screen Style:** Device with a main screen and an extra screen.
    

Note

See attached programming guide pdf file for more information.