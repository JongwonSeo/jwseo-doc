1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Edge (Look)](/galaxy/edge)
5.  [Technical Document](/galaxy/edge#techdocs)
6.  [Programming Guide: Spen](/galaxy/edge/spen-guide)

# Programming Guide : SPen Mar 23, 2017

*   [Edge(Look) SPen Programming Guide](/common/download/check.do?actId=1046)

Edge(Look) offers specialized widgets to extend the Android View System making it more usable, visible, and intuitive.

Edge(Look) supports the following:

*   **S-Pen**
    
    *   **SmartClip** allows users to delineate a region to capture a screen shot with S-Pen. As a developer, you can provide metadata from your application to SmartClip \[Figure 1\].
    *   **PointerIcon** provides more image options for the hovering pointer \[Figure 2\].

Note

S-Pen SDK will be deprecated from N OS because of concept changes and lack of usage. Please be mindful in using this.

![ SmartClip](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Look_03.jpg) 

<center>Figure 1: SmartClip</center>

![ PointerIcon](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Look_05.jpg) 

<center>Figure 2: PointerIcon</center>

Note

See attached programming guide pdf file for more information.