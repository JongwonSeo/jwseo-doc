1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)
4.  [Using an Emulator Skin](/galaxy/emulator-skin/guide)

# Using an Emulator Skin

## Prerequisites

*   Android SDK
*   At least one platform installed in Android SDK > Platforms
*   Eclipse or Android Studio
*   Samsung Emulator Skins

## Steps for Using Samsung Emulator Skins Using Eclipse

1.  _1._
    
    Download Samsung Emulator Skins, You can download from [here](/galaxy/emulator-skin).
    
2.  _2._
    
    After downloading, extract the zip file and copy it in the path **Android SDK > Platforms > android-x > skins**. (where x is the platform version number)
    
3.  _3._
    
    Launch Eclipse IDE Integrated with Android SDK.
    
4.  _4._
    
    In Eclipse IDE go to **Window > Android SDK and AVD Manager**.
    
    ![ [Figure 1] Start screen of the example program (main menu)](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_01.jpg) 
    
    <center>\[Figure 1\] Start screen of the example program (main menu)</center>
    
5.  _5._
    
    You may also click on the shortcut icon in the menu bar.
    
    ![ Figure 1.1: AVD shortcut icon](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_02.jpg) 
    
    <center>Figure 1.1: AVD shortcut icon</center>
    
6.  _6._
    
    In the AVD Manager, go to Device Definitions and click "Create Device"
    
    ![ Figure 2: Android AVD Manager](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_03.jpg) 
    
    <center>Figure 2: Android AVD Manager</center>
    
7.  _7._
    
    Fill in the specifications of the device such as the resolution, screen size and RAM and then click "Create Device"
    
    ![Figure 3: Creating a device definition](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_04.jpg) 
    
    <center>Figure 3: Creating a device definition</center>
    
8.  _8._
    
    Select the newly created device definition and click on the “Create AVD” button, as shown in figure 4 below. Alternately, you may go back to the Android Virtual Device tab and click the create button, as shown in figure 5.
    
    ![Figure 4: Android Virtual Devices](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_05.jpg) 
    
    <center>Figure 4: Android Virtual Devices</center>
    
    ![Figure 5: Android Virtual Devices](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_06.jpg) 
    
    <center>Figure 5: Android Virtual Devices</center>
    
9.  _9._
    
    Add the name of the AVD in the Name field. In the device dropdown list, select your newly created device definition. You may also use an existing device definition that has the same specifications as the skin that you want to use. In the Target Field select Android API level (Android API level must be same where the skin has been copied) from the drop down list available. In the Skin dropdown list, select the Samsung emulator skin you downloaded.
    
    ![Figure 6: Creating an AVD](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_07.jpg) 
    
    <center>Figure 6: Creating an AVD</center>
    
10.  _10._
    
    The new skin is created in the list of virtual devices. Select the new skin created and click on start button to launch the new Samsung emulator. Example: GalaxyS6.
    
    ![Figure 7: Launch Skin](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_07.jpg) 
    
    <center>Figure 7: Launch Skin</center>
    
11.  _11._
    
    The following window appears, asking for the display size. Select a screen size that fits on your monitor. Otherwise, the entire emulator window will not be visible.
    
    ![Figure 8: Launch options](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_09.jpg) 
    
    <center>Figure 8: Launch options</center>
    
12.  _12._
    
    The Android emulator takes a few minutes to start, then appears:
    
    ![Figure 9: Samsung Emulator](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_10.jpg) 
    
    <center>Figure 9: Samsung Emulator</center>
    
    The Samsung Emulator has the same functionality as the Generic Android Emulator, but varies with the size and appearance of the device.
    

## Steps for Using Samsung Emulator Skins Using Android Studio

1.  _1._
    
    Download Samsung Emulator Skins, You can download from [here](/galaxy/emulator-skin).
    
2.  _2._
    
    After downloading, extract the zip file and copy it in the path **Android Studio > plugins > android > lib > device-art-resources**. (where x is the platform version number)
    
3.  _3._
    
    Launch Android Studio.
    
4.  _4._
    
    In Android Studio, go to **Tools > Android > AVD Manager**.
    
    ![Figure 1: Android Studio Window](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_11.jpg) 
    
    <center>Figure 1: Android Studio Window</center>
    
5.  _5._
    
    In the AVD Manager, click "Create Virtual Device"
    
    ![Figure 2: Creating a new virtual device](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_12.jpg) 
    
    <center>Figure 2: Creating a new virtual device</center>
    
6.  _6._
    
    In the Virtual Device Configuration, click "New Hardware Profile"
    
    ![Figure 3: Creating a new hardware profile](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_13.jpg) 
    
    <center>Figure 3: Creating a new hardware profile</center>
    
7.  _7._
    
    Fill in the specifications for the skin you would like to use.
    
    ![Figure 4: Configuring hardware profile](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_14.jpg) 
    
    <center>Figure 4: Configuring hardware profile</center>
    
8.  _8._
    
    In the Default skin, select the folder of the zip file you extracted in the device-art-resources folder.
    
    ![Figure 5: Selecting the default skin](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_15.jpg) 
    
    <center>Figure 5: Selecting the default skin</center>
    
9.  _9._
    
    Select the system image you would like to use for your virtual device. Example: Lollipop
    
    ![Figure 6: Selecting a system image](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_16.jpg) 
    
    <center>Figure 6: Selecting a system image</center>
    
10.  _10._
    
    Verify all configurations are correct and click the finish button.
    
    ![Figure 7: Verifying configuration](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_17.jpg) 
    
    <center>Figure 7: Verifying configuration</center>
    
11.  _11._
    
    The newly created virtual device appears on the AVD manager. To launch, select the virtual device and click the play icon.
    
    ![Figure 8: Launch virtual device](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_18.jpg) 
    
    <center>Figure 8: Launch virtual device</center>
    
12.  _12._
    
    The Android emulator takes a few minutes to start, then appears:
    
    ![Figure 9: Samsung Emulator](https://developer.samsung.com//sd2_images/galaxy/content/emulator_skins_19.jpg) 
    
    <center>Figure 9: Samsung Emulator</center>
    

## Tips on Using Emulators

*   To change the orientation to landscape, press either Ctrl-F12 or the number 9 on your keypad with numLock turned off.
*   Pressing F6 starts track ball emulation.
*   You can enable or disable the Emulator Network by pressing F8.
*   The buttons and other controls on the emulator work as they would on the actual device. For example, clicking on the home button will bring up the home screen.
*   Make your emulator faster by using Host GPU. To use this, you will need the following versions of the Android development tools installed:
    *   Android SDK Tools, Revision 17 or higher
    *   Android SDK Platform API 15, Revision 3 or higher
*   Tweak the AVD hardware configuration to speed up your emulator. Increasing the memory and setting the screen to a small size gives a minor performance increase.
*   When starting an AVD, you may select a screen size suitable to your computer to enlarge or reduce the size of the emulator.

## Keyboard Shortcut Keys

The table below summarizes the mappings between the emulator keys and the keys of your keyboard. Note that you must have number lock (Num Lock) disabled in order to use the KEYPAD\_# keys

<table class="sd2_table" summary="Keyboard Shortcut Keys"><caption>Keyboard Shortcut Keys </caption><colgroup><col width="60%"> <col width="40%"> </colgroup><thead><tr><th class="tl" scope="col">Emulated Device Key</th><th class="tl" scope="col">Keyboard Key</th></tr></thead><tbody><tr><td>Home</td><td>HOME</td></tr><tr><td>Menu (left softkey)</td><td>F2 or Page-up button</td></tr><tr><td>Star (right softkey)</td><td>Shift-F2 or Page Down</td></tr><tr><td>Back</td><td>ESC</td></tr><tr><td>Call/dial button</td><td>F3</td></tr><tr><td>Hangup/end call button</td><td>F4</td></tr><tr><td>Search</td><td>F5</td></tr><tr><td>Power button</td><td>F7</td></tr><tr><td>Audio volume up button</td><td>KEYPAD_PLUS or Ctrl-5</td></tr><tr><td>Audio volume down button</td><td>KEYPAD_MINUS or Ctrl-F6</td></tr><tr><td>Camera button</td><td>Ctrl-KEYPAD_5 or Ctrl-F3</td></tr><tr><td>Switch to previous layout orientation (for example, portrait, landscape)</td><td>KEYPAD_7 or Ctrl-F11</td></tr><tr><td>Switch to next layout orientation (for example, portrait, landscape)</td><td>KEYPAD_9 or Ctrl-F12</td></tr><tr><td>Toggle cell networking on/off</td><td>F8</td></tr><tr><td>Toggle code profiling</td><td>F9 (only with -trace startup option)</td></tr><tr><td>Toggle fullscreen mode</td><td>Alt-Enter</td></tr><tr><td>Toggle trackball mode</td><td>F6</td></tr><tr><td>Enter trackball mode temporarily (while key is pressed)</td><td>Delete</td></tr><tr><td>DPad left/up/right/down</td><td>KEYPAD_7 or Ctrl-F11</td></tr><tr><td>Switch to next layout orientation(for example, portrait, landscape)</td><td>KEYPAD_4/8/6/2</td></tr><tr><td>DPad center click</td><td>KEYPAD_5</td></tr><tr><td>Onion alpha increase/decrease</td><td>KEYPAD_MULTIPLY(*) / KEYPAD_DIVIDE(/)</td></tr></tbody></table>

## Emulator Limitations

In this release, the emulator lacks support for the following features:

*   Placing or receiving actual phone calls. You can simulate phone calls (placed and received) through the emulator console, however.
*   USB connections
*   Camera/video capture (input).
*   Device-attached headphones
*   Determining connected state
*   Determining battery charge level and AC charging state
*   Determining SD card insert/eject
*   Bluetooth