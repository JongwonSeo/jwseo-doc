1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Camera](/galaxy/camera)
5.  [Technical Document](/galaxy/camera#techdocs)
6.  [Programming Guide](/galaxy/camera/guide)

# Programming Guide February 08, 2018

*   [ProgrammingGuide\_Camera.pdf](/common/download/check.do?actId=1162)

Camera SDK provides enhanced features and APIs available to users for developing enriched camera-based applications. This SDK offers additional functionalities and capabilities on top of existing Android Camera API2 capabilities. The Camera application interface utilizes the Android Camera APIs. Camera SDK also provides easy-to-use, fast performing APIs for new image processing and computational photography needs.

Camera SDK Processors provide the user with features such as High Dynamic Range, Low-Light environment photography, Depth of Field, and Landscape photography (Panorama).

The Camera SDK can be used to:

*   Develop camera application which uses new features added on top of Android APIs
*   Add image processing features to the application to provide delightful capabilities to the camera application

Note

See attached programming guide pdf file for more information.