1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Camera](/galaxy/camera)
5.  [Technical Document](/galaxy/camera#techdocs)
6.  [Release Note](/galaxy/camera/releases)

# Release Note February 08, 2018

## Introduction

*   #### Release Version
    1.3.0
    
*   #### Release Date
    February 08, 2018
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126"> <col width="123"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Camera SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides sample applications.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Camera APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/camera/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>Programming guide includes Overview, Hello Package Application, Main Class, and Main Features.</td></tr></tbody></table>
  

## Known Issues

*   Some features may not operate properly on some Samsung devices.  Known issues will be fixed on the next OS update.
    
    *   Constrained high speed capture session might not work.
    *   Certain scene modes might not work.
    *   Changing an AF mode might not have an immediate effect.

## Change History

*   *   Known issues list has been updated
*   *   This version has been released with Android O compatibility and new FaceAR effect.
*   *   This version has been released with ‘Light Packaging’ feature and enhanced stability    *   Updated the graphics buffer processing routine.
        *   Added the ‘Light Packaging’ feature to reduce APK footprint.
    
*   *   This version has been released with Android N compatibility.
*   *   Samsung Camera SDK 1.2.0 has been released with new processors.    *   Added the SCameraHazeRemoveProcessor class.
        *   Added the SCameraGifProcessor class.
    
*   *   Samsung Camera SDK 1.1.1 has been released with enhanced stability.
*   *   This version has been released with Android M compatibility, updated Camera 2.0 APIs and enhanced stability.    *   Removed text relocations from shared libraries to comply with the changes of the Android runtime policy.
        *   Updated the Android M Camera 2.0 API in tandem with the SDK changes.
        *   Added a close() method to the SCameraProcessor to explicitly define the life cycle.
        *   Known issues in SDK 1.0.0 had been fixed with the Galaxy device software update.
    
*   *   Samsung Camera SDK 1.0.0 has been released.

## Features

*   Hardware feature control
*   Capture processor
*   Realtime filters
*   Image