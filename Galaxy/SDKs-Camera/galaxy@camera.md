1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Camera](/galaxy/camera)

# Camera

## Essential Contents

*   [Camera SDK Version : 1.3.0Feb 08, 2018](/common/download/check.do?actId=1160)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/camera/index.html)
*   [Release Note](/galaxy/camera/releases)
*   [Programming Guide](/galaxy/camera/guide)

## Overview

The Samsung Galaxy series camera has been leading the race on smartphone and tablet camera technology. It has introduced many enhanced features and technologies over time. However, these camera features were available only with the preloaded camera application. Through the Camera SDK, Samsung is providing easy to use APIs that lets you implement new features into your own application. It also lets you use state of the art functionalities, such as control-based hardware, convenient smart modes, various real-time filters, and fast processing of large size images, in developing enriched camera-based applications.

The newly developed, intuitive Camera SDK facilitates easy development even if you do not have prior knowledge about the camera. API documents, sample codes and programming guides are provided to assist and speed up app development.

Since basic camera functionalities are common across different vendor devices, there is no need to provide different implementations for each vendor/device.

![Figure 1: Camera SDK](https://developer.samsung.com//sd2_images/galaxy/content/sms_camera_01.jpg) 

<center>Figure 1: Camera SDK</center>

#### Hardware Features

With Camera SDK, not only can you implement the traditional features such as exposure time, Auto White Balance (AWB), Auto Exposure (AE) compensation, but also Samsung special features, such as fast phase Auto Focus (AF), real-time HDR, preset metering mode, and Optical Image Stabilization (OIS).

The following are the currently supported Samsung special features:

*   **Fast phase-AF**: Fast auto focus based on phase is supported. The phase information of the on-sensor focus pixel is used.
    
*   **Real-time HDR:** Not only you can take HDR photographs but now you can also display the natural image on the preview even under poor conditions like a backlit environment.
    
*   **Optical Image Stabilization (OIS):** The Optical Image Stabilization motion compensation algorithm attempts to improve quality of both the preview as well as captured images.
    
*   **Preset Metering modes:** Support for traditional metering modes like Matrix, Center, and Spot is provided.
    

![Figure 2: Real-time HDR](https://developer.samsung.com//sd2_images/galaxy/content/sms_camera_02.jpg) 

<center>Figure 2: Real-time HDR</center>

#### Processors

The much-loved, “Smart Mode” setting loaded in the Samsung camera application is now available for you to implement on your application through image “Processor”.

The supported processors are:

*   **HDR:** You can get impressive high definition images even from hardware devices that do not support real-time HDR.
    
*   **Low Light:** You can enhance image capturing under low light environments and create clearer and brighter photographs.
    
*   **Panorama:** Beautiful panoramic photos can be captured to give you a complete view of a particular landscape. This photography technique allows you to capture a wider perspective of the landscape by stitching together several images into a single photo.
    
*   **Effect:** With effects, you can chose from a wide range of preloaded image filters like Tint, Cartoon, Vintage, etc. and apply them as previews on real-time basis and use them for photo capturing and video recording.
    
*   **Depth of Field:** Depth of field can be used to create a dramatic kind of effect in your images. It allows you to capture shallow depth photographs in which the background becomes blurred and the focused object stands out.
    

![Figure 3: Low Light Photography](https://developer.samsung.com//sd2_images/galaxy/content/sms_camera_03.jpg) 

<center>Figure 3: Low Light Photography</center>

#### Filter

Camera SDK Filter package supports following features:

*   **Real-time preview effect:** Effects can be applied not only on a captured image but also on the real-time preview frames of the camera.
    
*   **Still image and video recording with effects:** Effects can be applied on high-resolution images and full HD recoding of a moving object.
    
*   **Post effect processing:** Post processing on various images, such as jpeg file, bitmap object.
    
*   **Downloadable Filters:** Apart from preinstalled filters in the SDK, Galaxy device users can download the filters from Galaxy App stores and the application can load downloaded filters.
    

Following filters are included in SDK:

<table class="sd2_table"><caption>Table 1: Preloaded Filters </caption><colgroup><col width="34%"> <col width="33%"> <col width="33%"> </colgroup><tbody><tr><td>Negative</td><td>Breeze</td><td>Vivid</td></tr><tr><td>Soft</td><td>Serene</td><td>Brightness</td></tr><tr><td>Temperature</td><td>Contrast</td><td>Saturate</td></tr><tr><td>Tint Control</td><td>Highlights and Shadows</td><td>Black and White</td></tr><tr><td>Emboss</td><td>Posterize</td><td>Thermal Vision</td></tr><tr><td>Ripple</td><td>Mosaic</td><td>Beauty</td></tr><tr><td>Vignette</td><td>Vintage</td><td>Faded Color</td></tr><tr><td>Greyscale</td><td>Tint</td><td>Cartoon</td></tr><tr><td>Moody</td><td></td><td></td></tr></tbody></table>

<center>Table 1: Preloaded Filters</center>

![Figure 4: Real time filter example](https://developer.samsung.com//sd2_images/galaxy/content/sms_camera_04.jpg) 

<center>Figure 4: Real time filter example</center>

#### Image

Image package provides features which work on a heterogeneous environment, thereby providing maximum benefits in terms of performance for the application developer.

This feature supports basic operations like load/save image in raw/jpeg and resizing to high performing algorithms like convolution, edge operations, histogram equalization and mathematical operations.

The following are the currently supported features:

*   Large and various image data I/O interface
    *   Large image handling : Bitmap object, jpeg file, raw file, etc.
    *   Various image format : RGBA, NV21, YUYV, RGB888,JPEG
*   Fast image processing algorithm that utilizes various hardware resources
    *   Edge operation, convolution, color space conversion, image transformation, etc.

![Figure 5: Image processing example](https://developer.samsung.com//sd2_images/galaxy/content/sms_camera_05.jpg) 

<center>Figure 5: Image processing example</center>

#### Restrictions

Camera SDK has the following restrictions:

*   Devices with Android 5.0 Lollipop (API level 21) or above.
    Note
    
    Some features will be supported in Android 5.1 Lollipop (API level 22).
    
*   The supported functions might change according to the hardware performance.

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/camera/index.html)
*   [Release Note](/galaxy/camera/releases)
*   [Programming Guide](/galaxy/camera/guide)

## Samples

*   [CameraSample App](/common/download/check.do?actId=1161)

## FAQ

*   Q01 A
    Depth of Field (DOF) needs two images which have different focus positions. In the sample application, the focus position is set manually to get images. Some features on Galaxy S6, including manual focus, will be supported after software update to Android version 5.1 becomes available.
    
*   Q02 A
    Image is a package which works in a heterogeneous environment. This is so that you will get the maximum benefit in terms of performance. It can handle large sized images which are not supported by Android’s default APIs.  
    
*   Q03 A
    It is important to get good quality images for multi processing. Zero shutter lag and fast shutter speed are necessary in order to get a fine quality output image. Hence, we have to hold the camera steady while taking the picture.
    
*   Q04 A
    Camera SDK offers a wide range of preloaded image filters like Tint, Cartoon, Vintage etc. You can apply them in preview or on a real time basis and use them for shooting pictures or video recording. The following preloaded filters are supported:
    
      • Negative  
      • Soft  
      • Temperature  
      • Tint Control  
      • Emboss  
      • Ripple  
      • Vignette  
      • Greyscale  • Moody  
      • Breeze  
      • Serene  
      • Contrast  
      • Highlights  
      • Posterize  
      • Mosaic  
      • Vintage  • Tint  
      • Vivid  
      • Brightness  
      • Saturate  
      • Black and White  
      • Thermal Vision  
      • Beauty  
      • Faded Color  
      • Cartoon  
    
*   Q05 A
    Panorama stitching might fail when the device is shaking during capture or if the camera is moving in the wrong direction. Image stitching might also fail if you capture the last area and then move the camera in the same direction that the stitch was going, the stitch will fail when you hit the stop button.
    

[1](#1) [2](#2) [3](#3) [4](#4)

## FAQ

*   Q06 A
    The panorama processor supports flipped frames when using the front camera. This needs to be handled in the application.
    
*   Q07 A
    It is not recommended to use preview with resolutions higher than HD because panorama capture takes a long time as it computes how to stitch the images and the best stitching paths.
    
*   Q08 A
    Depth of Field (DOF) refers to the area in front and behind the object in your image that appears in focus, whereas all other areas appear to be blurred or out of focus. DOF is used to create a dramatic effect on your images. There are two ways to describe the qualities of depth of field - shallow DOF or deep DOF. Shallow DOF is when the included focus range is very narrow, which ranges from a few inches to several feet. Deep DOF is when the included range is a couple of yards to infinity. Currently, only Shadow DOF is supported.
    
*   Q09 A
    HdrProcessor is similar to Rich Tone HDR mode available in Samsung Galaxy devices which lets you take impressive high definition images. For best results, hold the device steady while capturing photos. The device will capture three photos at different exposure levels and combine them into a single photo.  
    With Camera SDK, not only you can take HDR photographs but also display brighter & vivid image on the preview. This feature can be used through Live HDR to stream landscapes, portraits in sunlight, and low-light/backlight scenes.  
    While HdrProcessor is available as software functionality, Live HDR is Samsung specific hardware feature.  
    Some devices might not support Live HDR, so you should check the availability before using this feature.
    
*   Q10 A
    HDR and Low Light processors mostly use hardware resources to process which could be time consuming. Use small input image to reduce the processing time.
    

[1](#1) [2](#2) [3](#3) [4](#4)

## FAQ

*   Q11 A
    Metering helps the camera in measuring the quantity of light. The camera measures the amount of light in a scene, and in many of its modes, uses the measurement to adjust various settings.
    
    Types of Metering:
    
    *   Center-weighted: The camera calculates a broader area than the camera does in spot mode. It sums up the amount of light in the center portion of the shot (60–80 %) and that of the rest of the shot (20–40 %).
    *   Spot: The camera calculates the amount of light in the center. When you take a photo in conditions where there is a strong backlight behind a subject, the camera adjusts the exposure to shoot the subject correctly.
    *   Matrix: The camera calculates the amount of light in multiple areas. This mode is suitable for general photos.
    *   Manual: Samsung's hardware specific metering mode is turned off. Android metering API can be used.
    
      
    
*   Q12 A
    Phase AF allows users to quickly focus on the subject. Phase AF uses phase information during focusing which makes it faster than the traditional AF.
    
*   Q13 A
    Check whether all required libraries are included in libs folder. Please refer to the Programming Guide for a list of libraries needed.
    
*   Q14 A
    Check whether Android permission below is declared in your AndroidManifest.xml file:
    
        <uses-permission android:name="android.permission.CAMERA" />
        
    
      
    
*   Q15 A
    Yes, it is possible to use Camera SDK on other devices which support features similar to Android Camera2. You would need to check the availability of Camera features on that particular device.
    

[1](#1) [2](#2) [3](#3) [4](#4)

## FAQ

*   Q16 A
    Camera SDK supports Android 5.0 or above.
    
*   Q17 A
    You should be aware of basic Android Camera2 APIs to understand the preview and capture code flow.
    
*   Q18 A
    The Samsung Camera SDK offers additional functionalities and capabilities on top of the existing Android Camera2 API capabilities. With Camera SDK, the traditional features will not only be available to you, but also, Samsung special features such as fast phase Auto Focus (AF), real-time HDR, preset metering mode, Optical Image Stabilization (OIS) could also be integrated into your app.
    

[1](#1) [2](#2) [3](#3) [4](#4)

## Videos

*