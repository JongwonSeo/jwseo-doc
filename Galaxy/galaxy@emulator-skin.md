1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)

Get under the skin

Emulator Skin is not just about looks. It's about giving  
developers the closest possible experience to the real device.

[Using an Emulator Skin](/galaxy/emulator-skin/guide)
![Get under the skin](https://developer.samsung.com//sd2_images/galaxy/landing/emulator_landing_img.png)

*   Galaxy A3 (2016) ![new](https://developer.samsung.com//sd2_images/common/text/new.png)
    ![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A3_2016_Black.jpg)
    
    *   Display4.7 inches(~69.4% screen-to-body ratio)
    *   Resolution720 x 1280(~312 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2016_Black.zip)
*   Galaxy A5 (2016) ![new](https://developer.samsung.com//sd2_images/common/text/new.png)
    ![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A5_2016_Black.jpg)
    
    *   Display5.2 inches(~72.5% screen-to-body ratio)
    *   Resolution1080 x 1920(~424 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2016_Black.zip)
*   Galaxy A7 (2016)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A7_2016_Black.jpg)
    
    *   Display5.5 inches(~74.30% screen-to-body ratio)
    *   Resolution1080 x 1920(~401 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2016_Black.zip)
*   Galaxy A8 (2016)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A8_2016_Black.jpg)
    
    *   Display5.7 inches(~73.80% screen-to-body ratio)
    *   Resolution1080 x 1920(~386 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A8_2016_Black.zip)
*   Galaxy S7![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S7_Black.jpg)
    
    *   Display5.1 inches (~72.10% screen-to-body ratio)
    *   Resolution1440 x 2560 (~577 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Black.zip)
*   Galaxy S7 Edge![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S7_Edge_Black.jpg)
    
    *   Display5.5 inches(~76.10% screen-to-body ratio)
    *   Resolution1440 x 2560 (~534 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S7_Edge_Black.zip)
*   Galaxy A9![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A9_Gold.jpg)
    
    *   Display6.0 inches(~75.90% screen-to-body ratio)
    *   Resolution1080 x 1920(~367 ppi pixel density)
    *   Color[gold](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_Gold.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A9_Gold.zip)
*   Note 5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_5_Black.jpg)
    
    *   Display5.7 inches(~76.62% screen-to-body ratio)
    *   Resolution1440 x 2560(~518 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_Black.zip)
*   Galaxy A3 (2015)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A3_2015_Black.jpg)
    
    *   Display4.5 inches(~65.50% screen-to-body ratio)
    *   Resolution540 x 960(~245 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A3_2015_Black.zip)
*   Galaxy A5 (2015)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A5_2015_Black.jpg)
    
    *   Display5.0 inches(~71.00% screen-to-body ratio)
    *   Resolution720 x 1280(~294 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A5_2015_Black.zip)
*   Galaxy A7 (2015)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_A7_2015_Black.jpg)
    
    *   Display5.5 inches(~72.50% screen-to-body ratio)
    *   Resolution1080 x 1920(~401 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_A7_2015_Black.zip)
*   Galaxy S6![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S6_Black.jpg)
    
    *   Display5.1 inches(~70.48% screen-to-body ratio)
    *   Resolution1440 x 2560 (~577 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S6_Black.zip)
*   Note 4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note4_Black.jpg)
    
    *   Display5.7 inches(~74.39% screen-to-body ratio)
    *   Resolution1440 x 2560(~515 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_Black.zip)
*   Galaxy S5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S5_Black.jpg)
    
    *   Display5.1 inches(~69.76% screen-to-body ratio)
    *   Resolution1080 x 1920(~432 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S5_Black.zip)
*   Tab A 9.7![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_A_9.7_Black.jpg)
    
    *   Display9.7 inches(~71.98% screen-to-body ratio)
    *   Resolution768 x 1024(~132 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_Black.zip)
*   Note 10.1 (2014)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_10.1_2014_Black.jpg)
    
    *   Display10.1 inches(~70.97% screen-to-body ratio)
    *   Resolution2560 x 1600(~299 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_Black.zip)
*   Tab S 8.4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Tab_S_8.4_Black.jpg)
    
    *   Display8.4 inches(~76.61% screen-to-body ratio)
    *   Resolution1600 x 2560(~359 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_Black.zip)
*   Tab S 10.5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_S_10.5_Black.jpg)
    
    *   Display10.5 inches(~72.88% screen-to-body ratio)
    *   Resolution2560 x 1600(~288 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_Black.zip)
*   Tab 4 7.0![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_4_7_0_Black.jpg)
    
    *   Display7.0 inches(~70.40% screen-to-body ratio)
    *   Resolution800 x 1280(~216 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_Black.zip)
*   Tab 4 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_4_10.1_Black.jpg)
    
    *   Display10.1 inches(~68.96% screen-to-body ratio)
    *   Resolution1280 x 800(~149 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_Black.zip)
*   Tab Pro 12.2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_Pro_12.2_Black.jpg)
    
    *   Display12.2 inches(~71.57% screen-to-body ratio)
    *   Resolution2560 x 1600(~247 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_Black.zip)
*   Tab Pro 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Tab_Pro_10.1_Black.jpg)
    
    *   Display10.1 inches(~70.97% screen-to-body ratio)
    *   Resolution2560 x 1600(~299 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_Black.zip)
*   Tab Pro 8.4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_Pro_8.4_Black.jpg)
    
    *   Display8.4 inches(~72.71% screen-to-body ratio)
    *   Resolution1600 x 2560(~359 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_Black.zip)
*   Galaxy S4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S4_Black.jpg)
    
    *   Display5.0 inches(~71.91% screen-to-body ratio)
    *   Resolution1080 x 1920(~441 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S4_Black.zip)
*   Note 3![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note3_Black.jpg)
    
    *   Display5.7 inches(~74.78% screen-to-body ratio)
    *   Resolution1080 x 1920(~386 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note3_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note3_White.zip)
    
*   Galaxy S3![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S3_Black.jpg)
    
    *   Display4.8 inches(~65.82% screen-to-body ratio)
    *   Resolution720 x 1280(~306 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S3_Black.zip)
*   Note 2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note2_Black.jpg)
    
    *   Display5.5 inches(~69.90% screen-to-body ratio)
    *   Resolution720 x 1280(~265 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_Black.zip)
*   Note 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_10.1_Black.jpg)
    
    *   Display10.1 inches(~62.72% screen-to-body ratio)
    *   Resolution1280 x 800(~149 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_Black.zip)
*   Note![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note1_Black.jpg)
    
    *   Display5.3 inches(~66.80% screen-to-body ratio)
    *   Resolution800 x 1280(~285 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_Black.zip)
*   Galaxy S2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S2_Black.jpg)
    
    *   Display4.3 inches(~62.75% screen-to-body ratio)
    *   Resolution480 x 800(~218 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S2_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S2_Black.zip)
*   Galaxy S![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_S_Black.jpg)
    
    *   Display4.0 inches(~57.88% screen-to-body ratio)
    *   Resolution480 x 800(~233 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_S_Black.zip#)
*   Neo![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Neo_Black.jpg)
    
    *   Display3.5 inches(~52.90% screen-to-body ratio)
    *   Resolution480 x 800(~267 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Neo_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Neo_Black.zip)
*   Gio![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Gio_Black.jpg)
    
    *   Display3.2 inches(~48.07% screen-to-body ratio)
    *   Resolution320 x 480(~180 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Gio_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Gio_Black.zip)
*   Ace![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Ace_Black.jpg)
    
    *   Display3.5 inches(~54.08% screen-to-body ratio)
    *   Resolution320 x 480(~165 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Ace_Black.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Ace_Black.zip)