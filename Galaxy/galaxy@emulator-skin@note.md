1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)
4.  [Galaxy Note Series](/galaxy/emulator-skin/note)

*   Note 5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_5_Black.jpg)
    
    *   Display5.7 inches(~76.62% screen-to-body ratio)
    *   Resolution1440 x 2560(~518 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_5_Black.zip)
*   Note 4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note4_Black.jpg)
    
    *   Display5.7 inches(~74.39% screen-to-body ratio)
    *   Resolution1440 x 2560(~515 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note4_Black.zip)
*   Note 10.1 (2014)![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_10.1_2014_Black.jpg)
    
    *   Display10.1 inches(~70.97% screen-to-body ratio)
    *   Resolution2560 x 1600(~299 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_2014_Black.zip)
*   Note 3![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note3_Black.jpg)
    
    *   Display5.7 inches(~74.78% screen-to-body ratio)
    *   Resolution1080 x 1920(~386 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note3_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note3_White.zip)
    
*   Note 2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note2_Black.jpg)
    
    *   Display5.5 inches(~69.90% screen-to-body ratio)
    *   Resolution720 x 1280(~265 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note2_Black.zip)
*   Note 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note_10.1_Black.jpg)
    
    *   Display10.1 inches(~62.72% screen-to-body ratio)
    *   Resolution1280 x 800(~149 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note_10.1_Black.zip)
*   Note![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Note1_Black.jpg)
    
    *   Display5.3 inches(~66.80% screen-to-body ratio)
    *   Resolution800 x 1280(~285 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Note1_Black.zip)