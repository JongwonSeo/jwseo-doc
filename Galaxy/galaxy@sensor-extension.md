1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Sensor Extension](/galaxy/sensor-extension)

# Sensor Extension

Sensor Extension SDK is available to allowed developers, please complete the request form and we will get back to you through email in developer information.

[Get Access](/sdk/request/form.do?sdkCd=01) 

*   Register  Samsung Account
    
    Register Samsung Account [here.](/signup)
*   Apply for  SDK Request
    
    Complete required information and submit the [SDK Request](/sdk/request/form.do?sdkCd=01) page
*   Approval   
    
    An approval mail is sent to you if there is no issue and you can download the SDK.
*   Develop Your App   
    
    Develop your Application with Sensor Extension SDK.

## Overview

## What is Sensor Extension?

Sensor Extension allows you to use sensors which are not supported by Google Android. It provides Meaningful data from sensors in the Samsung Device.

Sensor Extension SDK supports the feature below.

*   Heart Rate Monitor(HRM) Sensor IR/RED/GREEN/BLUE Signal
*   Ultraviolet (UV) Index

According to further updates, the supported sensor will also be added.

#### HRM Sensor Signal

The HRM Sensor is located on the rear panel of the phone. If you place your finger, measurement starts.

The photoplethysmogram (PPG) signal means the amount back of IR / RED / Green /Blue light that reflected from blood vessel in the finger.

![Figure 1: HRM sensor](https://developer.samsung.com//sd2_images/galaxy/content/HRMsensor.PNG) 

<center>Figure 1: HRM sensor</center>

#### UltraViolet Index

The Ultra Violet feature provides UV Index from the outdoor environment.

![Figure 2: UV sensor](https://developer.samsung.com//sd2_images/galaxy/content/img_sensorextension01.jpg) 

<center>Figure 2: UV sensor</center>

To measure the ultraviolet rays, the device uses the sensor located at the rear side of the phone.  
The sensor needs to be placed directly under the sun for 5 seconds in order to measure the ultraviolet rays.  
UV index values have 12 levels.

<table class="sd2_table" summary="UV Index, Rating, Description"><caption>UV index values have 12 levels. </caption><colgroup><col width="18%"> <col width="22%"> <col width="60%"> </colgroup><thead><tr><th scope="col">UV Index</th><th scope="col">Rating</th><th scope="col">Description</th></tr></thead><tbody><tr><td class="sd2_tc">&lt;2</td><td class="sd2_tc">Low</td><td>No danger to the average person</td></tr><tr><td class="sd2_tc">3 ~ 5</td><td class="sd2_tc">Moderate</td><td>Little risk of harm from unprotected sun exposure</td></tr><tr><td class="sd2_tc">6 ~ 7</td><td class="sd2_tc">High</td><td>High risk of harm from unprotected sun exposure</td></tr><tr><td class="sd2_tc">8 ~ 10</td><td class="sd2_tc">Very High</td><td>Very high risk of harm from unprotected sun exposure</td></tr><tr><td class="sd2_tc">11+</td><td class="sd2_tc">Extreme</td><td>Extreme risk of harm from unprotected sun exposure</td></tr></tbody></table>

Note

In natural indoor environment, UV Index is observed to be almost at level 0.

## Restrictions

Sensor Extension SDK has the following restrictions:

*   HRM Sensor IR/RED Signal
    *   Devices with Android 5.0 (Lollipop API level 21) or higher is required.
    *   Galaxy S5 and Galaxy S5mini are excluded.
    *   Only supported by Samsung device with HRM sensor.
*   HRM Sensor GREEN/BLUE Signal
    *   Devices with Android 8.0 (Oreo API level 26) or higher is required.
    *   Supported from device where the sensor is loaded. Ex: Galaxy S9.
    *   Only supported by Samsung device with HRM sensor.
*   Ultraviolet Index
    *   Devices with Android 4.4 Kit Kat (API level 19) or higher is required.
    *   Only supported by Galaxy Note4.
    *   Only supported by Samsung device with Ultraviolet sensor.