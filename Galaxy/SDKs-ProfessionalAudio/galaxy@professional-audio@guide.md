1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Professional Audio](/galaxy/professional-audio)
5.  [Technical Document](/galaxy/professional-audio#techdocs)
6.  [Programming Guide](/galaxy/professional-audio/guide)

# Programming Guide May 24, 2018

*   [ProgrammingGuide\_ProfessionalAudio.pdf](/common/download/check.do?actId=1040)

Professional Audio allows you to create virtual instrument applications with Android. You can connect and share audio devices and synchronize low-latency shared devices.

Professional Audio improves the environment in which virtual instruments are created by adding high-performance audio processing logic. You can use Professional Audio to create applications without background knowledge in hardware and high-performance drivers. There is no need to worry about connecting devices between applications. Using the provided modules and a USB MIDI driver, you can create virtual instrument applications with ease.

You can use the Professional Audio package to:

*   Create professional musical instrument applications using JACK Audio Connection Kit features.
*   Send Musical Instrument Digital Interface (MIDI) data, control audio/MIDI ports, access/use added plug-in information, and synchronize virtual instruments.
*   Create new sound modules and process high-speed audio signals.

Note

See attached programming guide pdf file for more information.