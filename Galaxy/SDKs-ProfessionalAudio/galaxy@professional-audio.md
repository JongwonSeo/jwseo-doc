1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Professional Audio](/galaxy/professional-audio)

# Professional Audio

## Essential Contents

*   [Professional Audio SDK 3.0.17 Version : 3.0.17May 24, 2018](/common/download/check.do?actId=1191)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/professional-audio/index.html)
*   [Release Note](/galaxy/professional-audio/releases)
*   [Programming Guide](/galaxy/professional-audio/guide)

Note

As of Jan. 1, 2019, the Professional Audio SDK is deprecated on all devices. We regret any inconvenience.
![new](https://developer.samsung.com//sd2_images/common/text/new.png)

## Overview

Professional Audio SDK allows you to create virtual instrument applications with Android. You can connect and share audio devices and synchronize low-latency shared devices.

Professional Audio SDK improves the environment in which virtual instruments are created by adding high-performance audio processing logic. You can use Professional Audio to create applications without background knowledge in hardware and high-performance drivers. You need not worry about connecting devices between applications. Using the provided modules and a USB MIDI driver, you can create virtual instrument applications with ease.

![Figure 1: Professional Audio SDK](https://developer.samsung.com//sd2_images/galaxy/content/SMS_ProfessionalAudio_01.jpg) 

<center>Figure 1: Professional Audio SDK</center>

Professional Audio SDK provides the following features:

*   Musical Instrument Creation
    *   API for creating professional instrument applications
    *   Support for all functions of the JACK Audio Connection Kit
*   Plug-ins
    *   Plug-ins for acoustic piano, steel guitar and standard drum kit
    *   Support for USB Audio devices
    *   Support for Audio input
    *   Usage of real-time scheduler
    *   It can make a connection between apps at SDK level
    *   It is easier to move to other apps and support its remote control

#### Musical Instrument Creation

Professional Audio SDK provides an API for creating professional instrument applications using an SDK and NDK.

You can use the SDK to send MIDI notes, control audio/MIDI ports, access/use added plug-in information, and synchronize virtual instruments.

You can use the NDK to create new sound modules and process high-speed audio signals.

![Figure 2: Musical Instrument Creation](https://developer.samsung.com//sd2_images/galaxy/content/SMS_ProfessionalAudio_new_02.jpg) 

<center>Figure 2: Musical Instrument Creation</center>

Professional Audio SDK supports all the JACK Audio Connection Kit functions. A separate low-latency audio environment is required for its use.

About JACK

JACK is a system for low-latency real-time processing of audio and MIDI signals. It allows multiple applications to be connected to an audio device and sharing between applications. You can place the client within the processor or on the JACK server. JACK can also perform diffused processing between networks (inter-network transmission is currently not supported by Professional Audio due to performance issues). JACK is compatible with a variety of operating systems including GNU/Linux, Solaris, FreeBSD, OS X and Windows. For more information, click on the link below: [http://jackaudio.org](http://jackaudio.org)

#### Plug-ins

You can use the Professional Audio plug-ins to improve the framework functions. To create a piano application for example, you can use the piano provided by the synthesizer plug-in to send MIDI notes. You can create applications without expert knowledge in virtual instruments and audio signal processing.

The Professional Audio SDK includes acoustic piano, steel guitar and standard drum kit plug-ins.

A wave table synthesizer and a variety of effect plug-ins are available on the support website.

![Figure 3: Synthesizer](https://developer.samsung.com//sd2_images/galaxy/content/SMS_ProfessionalAudio_new_03.jpg) 

<center>Figure 3: Synthesizer</center>

#### Restrictions

Professional Audio SDK has the following restrictions:

*   Can only work on devices with Professional Audio Framework
*   Framework performance depends on device specifications
*   When Professional Audio operates in background mode, it may affect the performance of other applications

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/professional-audio/index.html)
*   [Release Note](/galaxy/professional-audio/releases)
*   [Programming Guide](/galaxy/professional-audio/guide)
*   [Soundcamp Quick Guide](/galaxy/professional-audio/soundcamp-quick-guide)

## Samples

*   [Professional AudioSample App](/common/download/check.do?actId=1192)

## FAQ

*   Q01 A
    You can make a folder or file in your local app directory which set readable permission for processing module.  
    The processing module can read the folder and file.  
      
    Please refer to below source code.  
       
    \- Sample code -  
      
    1\. project file directory  
      
       <project-root>  
           -assets  
            -font  
             - file\_test\_1.txt  
      
    2\. The Java copy assets  
      
        public void copyAssetFileToLocal(){  
                 AssetManager assetManager = getAssets();  
                 String\[\] files = null;  
            try {  
                files = assetManager.list("font");  
                for(int i=0 ; i < files.length; i++){  
                    File outFile = new File(getApplicationInfo().dataDir + "/" + files\[i\].toString());  
                    outFile.createNewFile();  
                     
                    // The permissions should be setted for native processing module  
                    outFile.setReadable(true,false);  
                     
                    InputStream in = assetManager.open("font/" + files\[i\].toString());  
                    OutputStream out = new FileOutputStream(outFile);  
                     
                    // copy files to the local directory  
                    int data = 0;  
                    while((data = in.read()) != -1 ) {  
                        out.write(data);;  
                    }  
                     
                    in.close();  
                    out.flush();  
                    out.close();  
                }  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
      
    3\. The processing module read the file.  
      
        void readFile(){  
            // the file path to read : /data/data/\[package\_name\]/your\_files"  
            // Assme that the package name is "com.samsung.android.sdk.professionalaudio.sample.simpleclient"  
            // And, file\_test\_1.txt has a simple short text message like "good".  
       
            FILE\* fp = fopen("/data/data/com.samsung.android.sdk.professionalaudio.sample.simpleclient/file\_test\_1.txt","r+");  
       
            if(fp!=NULL)  
            {  
                char myStr\[20\];  
                fgets(myStr,20,fp); // get the simple text message  
                // TODO  
                fclose(fp);  
            }  
        }  
       
    \- Brief process -  
      
    You can make a folder in your local app directory which set executable or writable permission for processing module.  
    The processing module can write to the folder and file and Java also can read it.  
      
    Please refer to below source code.  
       
    \- Sample Code -  
      
    1\. Java make a directory  
      
           public void makeSharedFolder(){  
                 try{  
                        File sharedDir = new File(getApplicationInfo().dataDir + "/native/");  
                        sharedDir.mkdir();  
                        sharedDir.setWritable(true, false);  
                        sharedDir.setExecutable(true, false);  
                 }catch(Exception e){  
                        e.printStackTrace();  
                 }  
           }  
       
    2\. Native make a file and add a readable permission for Java  
      
           void writeFileC(){  
                 FILE\* fp = fopen("/data/data/com.example.rwsample/native/file\_from\_native.txt","w+");  
       
                 if(fp!=NULL)  
                 {  
                        fputs("successFromNative", fp); // add a text message  
                        fflush(fp);  
                        fclose(fp);  
                        LOGD("success to write from native C");  
       
                        // add a read permission for others  
                        chmod("/data/data/com.example.rwsample/native/file\_from\_native.txt", S\_IRWXU | S\_IROTH);  
                 }  
           }  
       
    3\. Java can read a file  
      
           public String readFileFromNative(){  
                 try {  
                        byte\[\] buffer = new byte\[100\];  
                        FileInputStream in = new FileInputStream(new File(getApplicationInfo().dataDir + "/native/file\_from\_native.txt"));  
                        in.read(buffer);  
                        return new String(buffer);  
                 } catch (Exception e) {  
                        e.printStackTrace();  
                 }  
                 return new String("fail to read");  
           }
    
*   Q02 A
    In this case, Your app will get a notification.  
      
    The onAppDeactivated() will be called. If your app was launched from the app, you can handle your service with this listener.  
    Of course, You would need to know the caller information (package name) that launches me. It is send by the Intent of onStartCommand().  
      
    You can get it using the code below.  
      
    "intent.getStringExtra("com.samsung.android.sdk.professionalaudio.key.callerpackagename");"  
      
    3 samples (SapaInstrumentSample, SapaMidiInstrumentSample, SapaEffectSample) were included in this code.
    
*   Q03 A
    Please use below ports. \[ described in the programming guide. \]  
      
    “system:capture\_x” -> “in:capture\_x”  
    “system:playback\_x” -> “out:playback\_x”  
       
    And, You also could query in native like the codes below.  
      
    const char \*\*capturePorts = jack\_get\_ports (jackClient, "in:capture\_\*", NULL, 0);  
    const char \*\*playbackPorts = jack\_get\_ports (jackClient, "out:playback\_\*", NULL, 0);
    
*   Q04 A
    It will be the trigger to tune the device for the best performance when your activity is launched.  
    The OS will tune the performance automatically.  
                        
    <application>  
            <activity>  
                <intent-filter>  
                    <action android:name="com.samsung.android.sdk.professionalaudio" />                 
                </intent-filter>      
            </activity>  
    </application>
    
*   Q05 A
    Make sure that the JACK client is active before connecting the audio port. All connections are terminated when the client is deactivated by jack\_deactivate(). Call the jack\_activate() method to activate the client before connecting.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q06 A
    Yes, this 'mono audio' means 'one channel'.
    
*   Q07 A
    Soundcamp is changing master volume when it receives "android.media.VOLUME\_CHANGED\_ACTION".  
      
    If other apps want to change the master volume, please change AudioManager.STREAM\_MUSIC.  
      
    Sample is below.  
      
    // MIN\_VOL\_DB = -42.0f;  
    // MAX\_VOL\_DB = 6.0f;  
       
    float db = ??;  
    AudioManager am = (AudioManager) getSystemService(AUDIO\_SERVICE);  
    float maxVol = am.getStreamMaxVolume(AudioManager.STREAM\_MUSIC);  
    float vol = (db - Volume.MIN\_VOL\_DB) \* maxVol / (Volume.MAX\_VOL\_DB - Volume.MIN\_VOL\_DB);  
    am.setStreamVolume(AudioManager.STREAM\_MUSIC, (int) vol, 0);  
       
    Plus,  
    Soundcamp is using following formula.  
      
    dB = 20\*log10(Vr)  
       
    There are no interactions between volume control and JACK-based audio, because it streamed to ALSA driver directly without help of android's gain control.  
      
    It seems to implement volume controls using event (e.g. KEYCODE\_VOLUME\_UP or KEYCODE\_VOLUME\_DOWN) in your activity.  
      
     public boolean onKeyDown(int keyCode, KeyEvent event) {  
      if (keyCode == KeyEvent.KEYCODE\_VOLUME\_UP) {  
       //control here  
      } else if (keyCode == KeyEvent.KEYCODE\_VOLUME\_DOWN) {  
       //control here  
      }  
      return true;  
     }
    
*   Q08 A
    In the case of communication platform, sampling rate is 8K. It is common.  
    We use the 48K sampling rate. The 48k is multiple of 8k and 240 buffer intervals is 5ms.  
    Mobile audio sub system needs to mix 8K audio (for speech call). So in the case of 48K, sampling rate will be more effective than the 44.1K for audio mixer module( the 44.1K needs to change to 48K. To do this, audio sub system requires a complex sample converter for high quality).  
    Additionally, Most of mobile H/W platform use 48K or 96K audio clock.
    
*   Q09 A
    Generally, You could use the Google API.  
    [http://developer.android.com/guide/topics/connectivity/usb/index.html](http://developer.android.com/guide/topics/connectivity/usb/index.html "open the new window")  
    [http://developer.android.com/guide/topics/connectivity/usb/host.html](http://developer.android.com/guide/topics/connectivity/usb/host.html "open the new window")  
    You can get a broadcast or intent-filter like the links above.
    
*   Q10 A
    Yes, Your app can get the BPM information.  
    When the BPM is changed, SapaAppStateListener.onAppChanged(...) will be called.  
    Please reference the sample code.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q11 A
    Yes, the parameters are always passed. Argv\[0\] is always the identifier name. It has to be used as the main jack client name.
    
*   Q12 A
    You should use the library name as “libwave.so” that is specified on the Android.mk in Jni directory of your project.
    
*   Q13 A
    Yes, you can keep your existing business models. For more details, please refer to [http://developer.samsung.com/](http://developer.samsung.com/)
    
*   Q14 A
    For now, the Soundcamp(Samsung DAW app) will be linked to Samsung store only.  
      
    The apps can be uploaded to any store, but in order to be recommended by Samsung, they must be available in Samsung store.
    
*   Q15 A
    You can activate your JACK client by calling the jack\_activate() method. This also calls the callback method jack\_set\_process\_callback(). You can deactivate the client by calling the jack\_deactivate() method. When you deactivate your JACK client, all connections are cut off and the process callback is not executed, which means CPU resources are not used.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q16 A
    There are many possible reasons. A common problem is the return value in your jack\_set\_process\_callback() callback method. If this is not ‘0’, the JACK server decides that there is an error in the client and terminates it immediately. Set the return value to ‘0’.
    
*   Q17 A
    Note3 / Galaxy 4, 5 : 1920\*1080, Note 10.1 , Galaxy tap 10.1, Galaxy tap pro : 2560\*1600
    
*   Q18 A
    It’s not identified.
    
*   Q19 A
    Simultaneous I/O for the analog headset jack and USB MIDI are supported. However, for Simultaneous I/O on USB peripherals and mixing through a USB digital in and analog headset output is not available so far. We are now looking for solutions for this, and we can give you the answer (Yes or No) by the end of May.
    
*   Q20 A
    Professional Audio API supports 240, 480 and 960 frame sizes at 48 kHz sample rate.  
    When the case of using: Galaxy Note 3 Samsung Exynos 5 Octa (Exynos 5420), 48khZ, 240-frame  
    touch input latency : 56.84ms  = Touch device (average 32.34ms) + UI framework (average 12ms) + Audio-Front-End-delay(2.5ms) + buffer latency (240-frame, 2 buffers = 10ms)  
    audio output latency (without touch) : 12.5ms = AFE (2.5ms) + buffer latency (10ms)  
    audio input to output latency : 12.5ms = AFE (2.5ms) + buffer latency (10ms)  
      
    \*\* DEPENDING ON CHIP SET ARCHITECTURE LESS THAN 10ms ADDITIONAL LATENCY MIGHT BE REQUIRED
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q21 A
    Make sure that the JACK client is active before connecting the audio port. All connections are terminated when the client is deactivated by jack\_deactivate(). Call the jack\_activate() method to activate the client before connecting.
    
*   Q22 A
    Use Transport to synchronize multiple JACK clients. You can share time, frames, and beats among your clients. Your clients can also listen to each other’s Transport events.  
      
    For more information on how to use Transport, see [http://jackaudio.org/files/docs/html/transport-design.html](http://jackaudio.org/files/docs/html/transport-design.html "open the new window")
    
*   Q23 A
    It depends on the source of the audio and how the user plans to output the data.  
      
    For analog TRRS input (MIC) to analog output (headphones):  
    Full audio support including low-latency audio I/O will be available on upcoming Devices with the new version of the SDK (v. 2.0). But in the currently released version, high-performance audio is only partly supported for output and is not supported for older devices. The new SDK will later be applied to other devices by updating system binaries, but the specific schedule and targets have not yet been decided.  
      
    For digital USB input to analog output (headphones) or digital USB input and output:  
    High-performance USB Audio drivers are currently under development, and we are looking for partners who can co-work with us. We are very welcome if Samsung and partners can collaborate on this development.  
      
    For digital USB MIDI input to analog audio output (headphones):  
    The current USB MIDI Driver is implemented using Android USB Host API  
    [(https://github.com/kshoji/USB-MIDI-Driver)](https://github.com/kshoji/USB-MIDI-Driver "open the new window")
    
*   Q24 A
    We don’t think so. Single buffer has some practical problems such as Jitter noise, especially in multi-tasking environment.
    
*   Q25 A
    In our experiments, iOS touch device latency was 29.9ms (h/w touch latency + UI framework), while our system showed 45ms (Touch device(average 32.34) + UI framework (average 12ms)). We know that touch latency is a key element for virtual music instruments, and we are doing our best to reduce it.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## Event Materials

*May 26, 2016Samsung Developer Conference 2016 - Mobile&Health    
    *   [HEA-19\_Fully\_Realizing\_the\_Power\_of\_S\_Health\_Data\_and\_Service.pdf (2.17MB)](/common/download/check.do?actId=13&statPath=/event/professional-audio/download/HEA-19_Fully_Realizing_the_Power_of_S_Health_Data_and_Service.pdf)
    *   [MBL-36\_Samsung\_Professional\_Audio\_meets\_Android\_Pro-Audio.pdf (4.53MB)](/common/download/check.do?actId=14&statPath=/event/professional-audio/download/MBL-36_Samsung_Professional_Audio_meets_Android_Pro-Audio.pdf)
    *   [MBL-50\_What’s\_new\_in\_Samsung\_Galaxy.pdf (2.35MB)](/common/download/check.do?actId=12&statPath=/event/professional-audio/download/MBL-50_What’s_new_in_Samsung_Galaxy.pdf)
    *   [MBL-60\_Don’t\_Lose\_App\_Revenue\_Over\_Poor\_Customer\_Experience.pdf (2.46MB)](/common/download/check.do?actId=11&statPath=/event/professional-audio/download/MBL-60_Don’t_Lose_App_Revenue_Over_Poor_Customer_Experience.pdf)
    *   [MBL-61\_Profiling\_Apps\_for\_Better\_Performance\_on\_Samsung\_Devices.pdf (2.38MB)](/common/download/check.do?actId=10&statPath=/event/professional-audio/download/MBL-61_Profiling_Apps_for_Better_Performance_on_Samsung_Devices.pdf)
    *   [MBL-74\_Exploring\_the\_Web\_and\_VR\_with\_Samsung\_Internet.pdf (1.47MB)](/common/download/check.do?actId=9&statPath=/event/professional-audio/download/MBL-74_Exploring_the_Web_and_VR_with_Samsung_Internet.pdf)
    
*Dec 8, 2014Samsung Developer Conference 2014 - Mobile    
    *   [A\_new\_real-time\_solution\_for\_mobile\_music\_creation\_Using\_the\_Soundcamp\_and\_ProfessionalAudio\_SDK.pdf (3.18MB)](/common/download/check.do?actId=6&statPath=/event/professional-audio/download/A_new_real-time_solution_for_mobile_music_creation_Using_the_Soundcamp_and_ProfessionalAudio_SDK.pdf "A_new_real-time_solution_for_mobile_music_creation_Using_the_Soundcamp_and_ProfessionalAudio_SDK.pdf (3.18MB)")
    *   [Connecting\_users\_with\_Samsung\_Wallet.pdf (4.18MB)](/common/download/check.do?actId=8&statPath=/event/professional-audio/download/Connecting_users_with_Samsung_Wallet.pdf)
    *   [Creating\_a\_Path\_to\_the\_World\_of\_Art\_A\_look\_at\_the\_PEN.UP\_SDK.pdf (2.63MB)](/common/download/check.do?actId=7&statPath=/event/professional-audio/download/Creating_a_Path_to_the_World_of_Art_A_look_at_the_PEN.UP_SDK.pdf)