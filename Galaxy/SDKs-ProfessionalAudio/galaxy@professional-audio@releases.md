1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Professional Audio](/galaxy/professional-audio)
5.  [Technical Document](/galaxy/professional-audio#techdocs)
6.  [Release Note](/galaxy/professional-audio/releases)

# Release Note May 24, 2018

## Introduction

*   #### Release Version
    3.0.17
    
*   #### Release Date
    May 24, 2018
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126px"> <col width="123px"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Professional Audio SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Professional Audio APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/professional-audio/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, Hello application, and feature descriptions.</td></tr></tbody></table>
  

## Change History

*   *   Support Android O(8.0).
*   *   Fix Bugs in Logging functions
*   *   Add RECORD\_AUDIO Permission
*   *   Change the logging functions
*   *   Change the logging functions
*   *   Support Android Marshmallow(6.0)
*   *   Added the audio device control listener
*   *   Support the Android targetSdkVersion 21.
*   *   USB audio device is supported. ( device dependent feature )*   Input device is supported. ( device dependent feature )
    *   The connection APISs that could integrated with the Soundcamp is supported.
    
*   *   The Professional Audio is separated from the Samsung Mobile SDK to make an efficient environment for the application development.*   New permission will be required when you initialize Professional Audio. Refer to programming guide for more information.
    
*   *   2 samples without GPL(Jack library) are added in this release.    *   SapaSimpleClient and SapaSimplePlugin.
    
*   *   Some samples use GPL code (Jack library). To prevent potential license problems, 3 samples are removed    *   SapaSimpleClient, SapaSimplePlugin, SapaSimplePluginClient are removed.
    

## Features

*   Create professional instrument applications.
*   Improve the framework functions with Professional Audio plug-ins.