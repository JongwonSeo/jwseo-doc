1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Professional Audio](/galaxy/professional-audio)
5.  [Technical Document](/galaxy/professional-audio#techdocs)
6.  [Soundcamp Quick Guide](/galaxy/professional-audio/soundcamp-quick-guide)

# Soundcamp Quick Guide May 24, 2018

*   [ProgrammingGuide\_ProfessionalAudio(Soundcamp\_QuickGuide).pdf](/common/download/check.do?actId=384)

## What is Soundcamp?

Soundcamp is a Music Creation Application for Android OS.

*   A mobile Digital Audio Workstation (DAW) and Music Instrument Digital Interface (MIDI) sequencer application based on Samsung Professional Audio SDK
*   Host applications for Soundcamp are compatible applications which provide
    *   Entry point for Soundcamp compatible apps
    *   Easy download of more Soundcamp compatible apps in GALAXY Apps
    *   Simultaneous playback and editing of MIDI and audio tracks from various Soundcamp compatible apps

Key Features

*   Simultaneous playback of 8 MIDI and audio tracks
*   USB MIDI control support
*   Provide Mixing Console
*   Provide Multi Track Recorder with MIDI and audio track editing functionalities
*   Provide Bundle Instrument Applications