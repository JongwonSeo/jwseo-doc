1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Rich Notification](/galaxy/rich-notification)
5.  [Technical Document](/galaxy/rich-notification#techdocs)
6.  [Release Note](/galaxy/rich-notification/releases)

# Release Note Jun 23, 2016

## Introduction

*   #### Release Version
    1.1.3
    
*   #### Release Date
    Jun 23, 2016
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126px"> <col width="123px"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Rich Notification SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Rich Notification APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/rich-notification/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, Hello application, and feature descriptions.</td></tr></tbody></table>
  

## Known Issue

*   Some features may not operate properly on Samsung Gear S.  Known issues will be fixed on the next Gear S OS update.
    
    *   If the Gear receives an updated notification while displaying a original notification, it will not display the updated notification and the screen becomes black. The updated notification will be displayed properly after moving to the next notification.
    *   If you set character limit parameter to 1 character on input action, then inputted character is removed after max character alert popup. The parameter value is greater than 2, setcharacterLimit function work normally.
    *   If the more option button is clicked an notification that contains 5 actions, empty space will display on more option popup down. This issue does not occur when 4, 6...actions.
    *   If the SrnSecondaryTemplate is set long text to small icon text, then small icon text is cut on Gear side screen.

## Change History

*   *   Samsung Rich Notification SDK 1.1.3 has been released with enhanced stability.
*   *   Samsung Rich Notification SDK 1.1.2 has been released with enhanced stability.
*   *   This version has been released with Android M(6.0) compatibility.*   Enhanced security for performing actions in rich notification from gear.
    *   Enhanced security in communication between rich SDK and Gear Manager.
    
*   *   Support for the Samsung Gear S2.*   Support for the Andorid phones, not limited to Samsung.  \* Supported devices vary depending on your region, operator and device brand.
        
    *   Added new API to check permission for Call and SMS actions.
    
*   *   Samsung Rich Notification SDK 1.0.0 has been released with enhanced stability and better performance.

## Features

*   Rich Notification : It provides the flexibility to personalize and brand content, making it compelling and actionable.
*   Action : Actions are important part of Rich Notifications. They allow users to engage with the content and perform tasks. There are two types of Actions: Primary Actions and More Options.
*   Template : Developers can choose from a variety of templates that provide alternative visual layouts and content structures. There are two types of templates: Primary and Secondary.