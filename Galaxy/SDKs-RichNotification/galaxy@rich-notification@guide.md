1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Rich Notification](/galaxy/rich-notification)
5.  [Technical Document](/galaxy/rich-notification#techdocs)
6.  [Programming Guide](/galaxy/rich-notification/guide)

# Programming Guide Jun 23, 2016

*   [Rich Notification Programming Guide](/common/download/check.do?actId=401)

The goal of the Rich Notifications SDK is to offer a way for developers to reach users with a minimum effort. It provides the flexibility to personalize and brand content, making it compelling and actionable.

![Gear User Interface](https://developer.samsung.com//sd2_images/galaxy/content/post_richnotif_01.jpg) 

<center>Figure 1: Gear User Interface</center>

Note

See attached programming guide pdf file for more information.