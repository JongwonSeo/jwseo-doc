1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Rich Notification](/galaxy/rich-notification)

# Rich Notification

## Essential Contents

*   [Rich Notification SDK 1.1.3 Version : 1.1.3Jun 23, 2016](/common/download/check.do?actId=862)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/rich-notification/index.html)
*   [Release Note](/galaxy/rich-notification/releases)
*   [Programming Guide](/galaxy/rich-notification/guide)

Note

As of Jan. 1, 2019, the Rich Notification SDK is deprecated on all devices. We regret any inconvenience.
![new](https://developer.samsung.com//sd2_images/common/text/new.png)

## Overview

The goal of the Rich Notifications SDK is to offer a way for developers to reach users with a minimal amount of effort. It provides the flexibility to personalize and brand content, making it compelling and actionable.

#### Design Principles

Rich Notifications have the following characteristics:

**Glance-able:** Rich Notifications are a lightweight way for service providers to send updates to users in real-time. They are concise and informative, offering bite-sized updates that highlight the most important details at a glance.

**Actionable:** Actions are placed on the top level of Rich Notifications, allowing users to take control with single touch. Users can quickly perform actions such as responding to a message, navigating to a destination, or liking a photo without having to pull out their phone or dig through Apps.

**Delightful:** Rich Notifications were designed with personalization in mind. They support full color graphics, rich text, and flexible content layouts. They offer a stylish and engaging interface for users to interact with their favorite services.

#### Content Structure

A standard notification is made of the following components:

![Figure 1: Rich Notification Components](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification01.jpg) 

<center>Figure 1: Rich Notification Components</center>

##### Tap to View More Details

Developers have the option to include secondary content with any notification. This content will be shown when the user taps on the body of the notification

![Figure 2: Tap to expand a notification and view more details](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification02_160118.jpg) 

<center>Figure 2: Tap to expand a notification and view more details</center>

#### Actions

Actions are an important part of Rich Notifications. They allow users to engage with the content and perform tasks. There are two types of Actions: Primary Actions and More Options.

##### Primary Actions

Primary Actions are the most important action associated with the notification. These actions can work in one of two ways: single tap and a selectable list of commands.

![Figure 3: Action task with a single tap](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification03_160118.jpg) 

<center>Figure 3: Action task with a single tap</center>

##### More Options

The “More Options” icon produces a menu of additional actions that the user might want to take on the notification. These options are less accessible by design, placing the emphasis on the Primary Action.

![Figure 4: Tap on the More Options icon for additional actions](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification04.jpg) 

<center>Figure 4: Tap on the More Options icon for additional actions</center>

#### Templates

Developers can choose from a variety of templates that provide alternative visual layouts and content structures. There are two types of templates: Primary and Secondary.

![Figure 5: Example Primary Templates.](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification06_160118.jpg) 

<center>Figure 5: Example Primary Templates.</center>

![Figure 6: Example Secondary Templates (Optional).](https://developer.samsung.com//sd2_images/galaxy/content/img_richnotification07_160118.jpg) 

<center>Figure 6: Example Secondary Templates (Optional)</center>

#### Restrictions

Rich Notification SDK has the following restrictions:

*   Devices with Android 4.4 KitKat (API level 19) or higher support Rich Notification SDK.
*   Rich Notification SDK requires Gear Manager to connect Wearable Device.

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/rich-notification/index.html)
*   [Release Note](/galaxy/rich-notification/releases)
*   [Programming Guide](/galaxy/rich-notification/guide)

## Samples

*   [Rich NotificationSample App](/common/download/check.do?actId=182)

## FAQ

*   Q01 A
    No, rich notification will only be supported on Gear S and newer models.
    
*   Q02 A
    The Rich Notification SDK supports Android 4.4 or above.
    
*   Q03 A
    The Rich Notification SDK provides you to post “Glance-able”, “Actionable”, and “Delightful” notifications to the connected device. You could apply various templates to your notifications that make your notifications more attractive.
    
*   Q04 A
    An add-in emulator of the Gear SDK provides the functionality of connecting, posting, and receiving “Rich Notifications”. If you do not have “Rich notification capable” wearable devices, you can use the Gear Emulator in Gear SDK.
    
*   Q05 A
    We provide 5 primary templates which could contain important notification information, and 2 secondary tempaltes for introducing more detailed information.
    

[1](#1)

## Videos

*   

## Event Materials

*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.93MB)](/common/download/check.do?actId=21&statPath=/event/rich-notification/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/rich-notification/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/rich-notification/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.94MB)](/common/download/check.do?actId=24&statPath=/event/rich-notification/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/rich-notification/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.79MB)](/common/download/check.do?actId=26&statPath=/event/rich-notification/download/MWC_SESSION_Group_play_V3.pdf)