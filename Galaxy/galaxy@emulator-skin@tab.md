1.  Devices
2.  [Galaxy](/galaxy)
3.  [Emulator Skins](/galaxy/emulator-skin)
4.  [Galaxy Tab Series](/galaxy/emulator-skin/tab)

*   Tab A 9.7![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_A_9.7_Black.jpg)
    
    *   Display9.7 inches(~71.98% screen-to-body ratio)
    *   Resolution768 x 1024(~132 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_A_9.7_Black.zip)
*   Tab S 10.5![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_S_10.5_Black.jpg)
    
    *   Display10.5 inches(~72.88% screen-to-body ratio)
    *   Resolution2560 x 1600(~288 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_S_10.5_Black.zip)
*   Tab S 8.4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Tab_S_8.4_Black.jpg)
    
    *   Display8.4 inches(~76.61% screen-to-body ratio)
    *   Resolution1600 x 2560(~359 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Tab_S_8.4_Black.zip)
*   Tab 4 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_4_10.1_Black.jpg)
    
    *   Display10.1 inches(~68.96% screen-to-body ratio)
    *   Resolution1280 x 800(~149 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_10.1_Black.zip)
*   Tab 4 7.0![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_4_7_0_Black.jpg)
    
    *   Display7.0 inches(~70.40% screen-to-body ratio)
    *   Resolution800 x 1280(~216 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_4_7_0_Black.zip)
*   Tab Pro 8.4![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_Pro_8.4_Black.jpg)
    
    *   Display8.4 inches(~72.71% screen-to-body ratio)
    *   Resolution1600 x 2560(~359 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_8.4_Black.zip)
*   Tab Pro 12.2![](https://developer.samsung.com//sd2_images/galaxy/emulator/Galaxy_Tab_Pro_12.2_Black.jpg)
    
    *   Display12.2 inches(~71.57% screen-to-body ratio)
    *   Resolution2560 x 1600(~247 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Galaxy_Tab_Pro_12.2_Black.zip)
*   Tab Pro 10.1![](https://developer.samsung.com//sd2_images/galaxy/emulator/Tab_Pro_10.1_Black.jpg)
    
    *   Display10.1 inches(~70.97% screen-to-body ratio)
    *   Resolution2560 x 1600(~299 ppi pixel density)
    *   Color[black](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_Black.zip) [white](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_White.zip)
    
    [Download Skin](http://img-developer.samsung.com/images/emulator_skin/Tab_Pro_10.1_Black.zip)