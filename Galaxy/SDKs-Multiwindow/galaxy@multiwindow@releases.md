1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [MultiWindow](/galaxy/multiwindow)
5.  [Technical Document](/galaxy/multiwindow#techdocs)
6.  [Release Note](/galaxy/multiwindow/releases)

# Release Note May 26, 2016

## Introduction

*   #### Release Version
    1.3.2
    
*   #### Release Date
    May 26, 2016
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126px"> <col width="123px"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung MultiWindow SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about MultiWindow APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/multiwindow/index.html" target="_balnk">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, Hello MultiWindow application, and feature descriptions.</td></tr></tbody></table>
  

## Change History

*   *   The version code is updated.
*   *   This version has been released with enhanced OS compatibility.
*   *   The following issues have been resolved in this version: When the application using MultiWindow-SDK1.2.5 is running Android 4.4 and lower. It occured runtime exception(getScaleInfo does not exist in the source code) when trying to create a SMultiWindowActivity.
*   *   Samsung MultiWindow SDK 1.2.5 has been released with enhanced stability and better performance.
*   *   Samsung MultiWindow SDK 1.2.3 has been released with enhanced stability and better performance.
*   *   The MultiWindow is supporting penwindow API which is used for changing and checking status and starting penwindow.
*   *   The MultiWindow is separated from the Samsung Mobile SDK to make an efficient environment for the application development.*   New permission will be required when you initialize MultiWindow. Refer to programming guide for more information.
    

## Features

*   Applications registered with MultiWindow can run with other applications on Traybar.
    *   Add application to the Multi-Instance environment.
    *   Paired windows.
    *   Resize applications with MultiWindow Centerbar (displayed between applications).