1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [MultiWindow](/galaxy/multiwindow)
5.  [Technical Document](/galaxy/multiwindow#techdocs)
6.  [Programming Guide](/galaxy/multiwindow/guide)

# Programming Guide May 26, 2016

*   [MultiWindow Programming Guide](/common/download/check.do?actId=357)

MultiWindow allows you to run multiple resizable applications simultaneously.

MultiWindow consists of the MultiWindow UI and MultiWindow Framework. The MultiWindow UI has a MultiWindow launcher called Traybar. You can use it to manage MultiWindow applications. You can launch the Traybar by long-pressing the Back key and use it to run multiple applications. MultiWindow Framework is a system service connected to Activity Manager, Window Manager, and View System to configure applications in MultiWindow.

You can register your application as a MultiWindow application and add it to the Traybar by declaring it in your Android manifest file.

You can use MultiWindow to:

*   check the status of your MultiWindow application
*   change the status
*   check the status change

Note

See attached programming guide pdf file for more information.