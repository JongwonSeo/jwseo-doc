1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Motion](/galaxy/motion)
5.  [Technical Document](/galaxy/motion#techdocs)
6.  [Release Note](/galaxy/motion/releases)

# Release Note Jan 18, 2018

## Introduction

*   #### Release Version
    2.2.2
    
*   #### Release Date
    Jan 18, 2018
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126px"> <col width="123px"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Motion SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Motion APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/motion/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, feature descriptions and the Hello Motion application.</td></tr></tbody></table>
  

## Change History

*   *   Samsung Motion SDK 2.2.2 has been released in order to be compatible with Android.*   Replaced deprecated API (PackageManager.getSystemFeatureLevel()) with new one (PackageManager.hasSystemFeature()).
    
*   *   Added isFeatureSupported(int type) for checking supported activities.
*   *   Samsung Motion SDK 2.1.4 has been released in order to be compatible with Android.*   Replaced deprecated Android API, PowerManager.isScreenOn(), in android API Level 20 with new one, Display.getState().
    
*   *   Added updateInfo() API*   Added isUpdateInfoBatchModeSupport()
    *   Added @since tag
    
*   *   Samsung Motion SDK 2.0.0 has been released with enhanced stability and better performance.
*   *   Samsung Motion SDK has been separated from the Samsung Mobile SDK to make an efficient environment for the application development.*   Added a new permission to be required when you initialize Motion. Refer to programming guide for more information.
    
*   *   Added SmotionActivity and SmotionActivityNotification classes.*   Added IllegalStateException in each constructor when initialize() is not successful or not called.
    *   Changed Sample application structure to improve legibility.
    
*   *   Added IllegalArgumentException when listener is null or type is invalid public boolean isFeatureEnabled(int type) public void start(SmotionCall.ChangeListener listener) public void start(SmotionPedometer.ChangeListener listener)*   Changed the structure of Sample App in order to raise legibility.
    
*   *   Fixed Null exception problem in SmotionPedometer.getInfo() API. It occurred when this API is called during the screen off status.

## Features

*   Recognize call motion.
*   Listen for and process pedometer event data.
*   Recognize the motion status (stationary, walk, run, in a vehicle).
*   Recognize the accuracy of detected activities by using various sensors on the device.