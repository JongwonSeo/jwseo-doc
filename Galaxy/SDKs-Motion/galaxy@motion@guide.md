1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Motion](/galaxy/motion)
5.  [Technical Document](/galaxy/motion#techdocs)
6.  [Programming Guide](/galaxy/motion/guide)

# Programming Guide Jan 18, 2018

*   [Motion Programming Guide](/common/download/check.do?actId=1159)

Motion allows you to retrieve pedometer and activity information in your application. Motion processes raw data from the device motion sensors to collect pedometer and activity information.

*   Access pedometer information.
*   Access activity information.

Note

See attached programming guide pdf file for more information.