1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Motion](/galaxy/motion)

# Motion

## Essential Contents

*   [Motion SDK Version : 2.2.2Jan 18, 2018](/common/download/check.do?actId=1164)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/motion/index.html)
*   [Release Note](/galaxy/motion/releases)
*   [Programming Guide](/galaxy/motion/guide)

Note

As of Jan. 1, 2019, the Motion SDK is deprecated on all devices. We regret any inconvenience.
![new](https://developer.samsung.com//sd2_images/common/text/new.png)

## Overview

Motion allows you to collect motion information from the device and use it in your applications. You can collect from the device sensors raw data related to pedometer or activity recognition or the motions associated with answering the phone.

![Figure 1: Motion](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Motion_01_2.jpg) 

<center>Figure 1: Motion</center>

You can use the Motion SDK to:

*   Detecting call motion
*   Using pedometer data
*   Recognizing activities

#### Detecting Call Motion

Motion recognizes the action when the user first watches the device and then subsequently brings the device up to the ear. You can use this motion to answer an incoming call. If the user is reading a message, you can use the motion to call the number from which the message was sent.

![Figure 2: Call Motion](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Motion_02.jpg) 

<center>Figure 2: Call Motion</center>

#### Using Pedometer Data

The pedometer provides various information, such as the number of steps the user has taken, the status of the current steps, the number of used calories, and the distance traveled by using various sensors on the device. The pedometer data is used in Samsung S Health application’s Pedometer, where the pedometer calculates the user’s calorie consumption, distance, and speed based on their height and weight. You can use the S Health application for configuring the user height and weight.

![Figure 3: Pedometer data](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Motion_03.jpg) 

<center>Figure 3: Pedometer data</center>

#### Recognizing Activities

Motion recognizes the status (Stationary, Walk, Run, Vehicle) and accuracy of detected activities by using various sensors on the device.

*   You can use the following activity modes:

<table class="sd2_table"><caption>Recognizing Activities </caption><colgroup><col width="33%"> <col width="34%"> <col width="33%"> </colgroup><thead><tr><th>Real time</th><th>Batch</th><th>Notification</th></tr></thead><tbody><tr><td>When the status or accuracy changes, your application can receive activity information while the device screen is on.</td><td>The batch FIFO stores the timestamp, status and accuracy. When the FIFO is full, your application can receive activity information.</td><td>When the device detects an activity that the user has selected, your application can receive a notification.</td></tr></tbody></table>

#### Restrictions

Motion has the following restrictions:

*   Devices with Android 4.3 Jelly Bean (API level 18) or higher support Motion.
*   Devices with Android 4.4KitKat (API level 19) or higher support activity recognition.
*   Devices with a proximity, acceleration, and a gyro sensor support call motion detection.
*   Devices with an acceleration and a pressure sensor support pedometer data usage. The device cannot recognize the number of up/down steps and the status of up/down steps without the pressure sensor.

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/motion/index.html)
*   [Release Note](/galaxy/motion/releases)
*   [Programming Guide](/galaxy/motion/guide)

## Samples

*   [MotionSample App](/common/download/check.do?actId=1163)
*   [Creating an app to control your run with Mot...](/common/download/check.do?actId=143 "Creating an app to control your run with Motion Library")

## FAQ

*   Q01 A
    The Motion package allows you to use Samsung smart device pedometer features and motions in your application.
    
*   Q02 A
    The sensor data, which used to be processed by the application processor, is now processed by the sensor hub. The sensor hub (or Micro Control Unit) uses very little energy to process the data while the application processor is sleeping.The sensor data, which used to be processed by the application processor, is now processed by the sensor hub. The sensor hub (or Micro Control Unit) uses very little energy to process the data while the application processor is sleeping.
    
*   Q03 A
    Yes, it is possible. You can use the real time and batch mode at the same time by setting the mode to MODE\_ALL when you add a listener for activity tracking with the SmotionActivity.start() method call. You can also use the notification mode by adding a notification listener for activity tracking with the SmotionActivityNotification.start() method call.
    

[1](#1)

## Event Materials

*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.93MB)](/common/download/check.do?actId=21&statPath=/event/motion/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/motion/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/motion/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.94MB)](/common/download/check.do?actId=24&statPath=/event/motion/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/motion/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.79MB)](/common/download/check.do?actId=26&statPath=/event/motion/download/MWC_SESSION_Group_play_V3.pdf)