1.  Devices
2.  [Galaxy](/galaxy)
3.  [Distribute](/galaxy/distribute/validation-policy#)
4.  [Validation Policy](/galaxy/distribute/validation-policy)

# Validation Policy [EN](/en/galaxy/distribute/validation-policy) [KR](/kr/galaxy/distribute/validation-policy) [CN](/cn/galaxy/distribute/validation-policy)

## App Validation for Samsung Galaxy Apps Distribution

Samsung at its sole discretion and without the consent of any other parties reserves the right to distribute in, withhold distribution from, and remove from distribution in the Galaxy Apps those applications (_apps_) submitted for distribution validation and those apps being distributed.

To be validated for distribution, apps must:

*   Meet all Samsung App Validation Policy (_policy_) statements.
*   Comply with all local laws of the countries that apps are to be distributed to.

Note

If an app meets all policy requirements but does not comply with the local laws or customs of 1 or more countries specified for distribution in the app’s registration, those countries will be removed from the app’s distribution.

During app distribution, if app content or functions are found to be objectionable to users, to no longer comply with local laws of distribution countries, or to no longer comply with Samsung policy, Samsung can stop app distribution and remove the app from the Galaxy Apps.

## Samsung App Validation Process

![Validation Process](https://developer.samsung.com//sd2_images/gear/content/img_ac_policy01_2_en.jpg)

After you complete app registration in Seller Office, validation team confirms whether or not your app functions and content meet the requirements of the Galaxy Apps Policy. Validation is performed in an initial phase (Pre Review) and then in a final phases (Device Test).

*   **Pre Review**: Validation Team confirms app basic functions (including but not limited to, installation, execution, and uninstallation) and contents meet Validation policy requirements.  
    During testing, if an app does not meet requirements of a country, then those countries and devices will be removed from distribution, but other distribution countries and devices will not be affected.  
    If an app passes an initial phase, app undergoes comprehensive function test in the final phase of validation.  
    If an app fails an initial phase, app distribution will be rejected and you will be notified of the reason.
    
*   **Device Test**: If app passes an initial phase, validation team confirms app comprehensive functions.  
    If app has functional problem, app distribution will be suspended.
    

During both phases of validation, you can review your app’s testing status in the My Applications tab of Seller Office ([http://seller.samsungapps.com](http://seller.samsungapps.com)).

Your app can progress through the following testing states:

<table class="sd2_table" summary="UV Index, Rating, Description"><colgroup><col width="35%"> <col width="65%"> </colgroup><tbody><tr><td class="sd2_tc">Registering</td><td class="sd2_tc" style="text-align:  left;">App registration is not complete, and/or the app has not been submitted for validation.<br>In this state, you can register new and change existing app information and files before they are tested.</td></tr><tr><td class="sd2_tc">For sale</td><td class="sd2_tc" style="text-align:  left;">App has passed validation testing, and the app is being distributed and its sales are enabled.</td></tr><tr><td class="sd2_tc">Suspended</td><td class="sd2_tc" style="text-align:  left;">App has validation testing, or has passed but has failed to meet Gear policy requirements. In all cases, the app is not being distributed, and app sales are not enabled.</td></tr></tbody></table>

## Samsung App Validation Policy

This section stipulates the requirements that apply to apps that run on Samsung devices and are to be distributed via the Galaxy Apps.

#### 1\. Performance

This section related to app operations.

##### 1.1 Functionality

1.  _1.1.1_
    
    App installation, launch, termination, and uninstallation must succeed without errors.
    
2.  _1.1.2_
    
    App features must not crash or cause functional problems.
    
3.  _1.1.3_
    
    Apps must not include hidden features.
    
4.  _1.1.4_
    
    Trial or Beta version binaries must not be submitted for certification. .
    
5.  _1.1.5_
    
    For apps that require user login, login info (such as user ID and password) for a user accountto be used to test the app must be provided during app registration
    
6.  _1.1.6_
    
    Apps must not include malware and must not transmit viruses.
    
7.  _1.1.7_
    
    Apps must not initiate or support push notifications.
    
8.  _1.1.8_
    
    Apps must not generate icon shortcuts or bundles.
    
9.  _1.1.9_
    
    Apps must not initiate or support automatic update.
    

##### 1.2 Usability

1.  _1.2.1_
    
    Apps must be valuable, entertaining, unique or informative.
    
2.  _1.2.2_
    
    The same app must be registered only one time.
    
3.  _1.2.3_
    
    Apps must not contain an excessive number of advertisements, web clippings, website links, or videos that degrade the user experience
    
4.  _1.2.4_
    
    App graphics must be visible.
    
5.  _1.2.5_
    
    App text must be readable and not be truncated or distorted.
    
6.  _1.2.6_
    
    App screens must fill the device display screen.
    
7.  _1.2.7_
    
    Paid apps must not deceive customers with unreasonably high prices.
    
8.  _1.2.8_
    
    Store type app which offers app download inside the app is not allowed.
    

##### 1.3 Metadata

1.  _1.3.1_
    
    App metadata must be appropriate for users of all ages.
    
2.  _1.3.2_
    
    If app registration specifies two or more distribution countries, app metadata must support English as the default language.
    
3.  _1.3.3_
    
    App registration preview images, screenshot images, and descriptions must accurately show and describe app functionality that is available.
    
4.  _1.3.4_
    
    If the app provides for in-app item purchases or advertisements, this must be accurately shown and described in the app registration preview images, screenshot images, and descriptions.
    
5.  _1.3.5_
    
    App metadata must not include irrelevant, misleading, or fraudulent keywords.
    
6.  _1.3.6_
    
    App registration must specify the age rating and category that are appropriate for the app.
    
    Note
    
    If app registration does not specify an appropriate age restriction or category, Samsung can appropriately change them.
    
7.  _1.3.7_
    
    Metadata must not include references to or relevant images of: app stores other than Samsung app stores, or mobile platforms for promotional purpose.
    
8.  _1.3.8_
    
    All included and specified URLs must not cause functional or content problems.
    

##### 1.4 Hardware Compatibility

1.  _1.4.1_
    
    Apps must not make sounds in silent mode.
    
2.  _1.4.2_
    
    Apps must not change default settings
    
3.  _1.4.3_
    
    Apps must not restart the user device.
    
4.  _1.4.4_
    
    Apps must not cause problems of the embedded device features (including but not limited to: Bluetooth, G-sensor, Wi-Fi, Camera, Call, Volume/Hold Key, Alarm, and SMS/MMS).
    
5.  _1.4.5_
    
    Apps must not cause problems for hardware and system events.
    
6.  _1.4.6_
    
    Apps must not crash when the device is rotated and when device accessories (including but not limited to ear phones) are plugged into or unplugged from the device.
    
7.  _1.4.7_
    
    Apps must not consume excessive battery current, generate excessive heat, or rapidly drain the device battery.
    

#### 2\. App Content and Behavior

This section relates to app material and actions.

##### 2.1 Sexually Explicit Content

1.  _2.1.1_
    
    Apps must not visually or audibly present or encourage overt sexual concepts or content (including but not limited to: explicit nudity, exposed male or female genitalia, pornography, pedophilia, bestiality, sexually explicit behavior, and sexually suggestive poses).
    
2.  _2.1.2_
    
    Apps must not visually or audibly present or encourage exploitative sexual behavior (including but not limited to: sexual abuse, sexual assault, and bestiality)
    
3.  _2.1.3_
    
    Apps must not provide a method to access websites that have a sexual emphasis (including but not limited to: adult friend finder and dating websites).
    

##### 2.2 Violence

1.  _2.2.1_
    
    Apps must not visually or audibly present or encourage murder, suicide, torture, or abuse.
    
2.  _2.2.2_
    
    Apps must not visually or audibly present or encourage violence or criminal behavior that could instigate a crime.
    
3.  _2.2.3_
    
    Apps must not visually or audibly present or encourage violent threats toward people or animals
    
4.  _2.2.4_
    
    Apps must not visually or audibly present or encourage recklessly gruesome content (including but not limited to excessive bleeding).
    
5.  _2.2.5_
    
    Apps must not visually or audibly present or encourage use in the real world of weapons, bomb, terrorist actions, or any other dangerous objects.
    

##### 2.3 Alcohol, Tobacco and Drugs

1.  _2.3.1_
    
    Apps must not visually or audibly present or encourage the illegal use of alcohol, tobacco or drugs.
    
2.  _2.3.2_
    
    Apps must not visually or audibly present or encourage the sales of alcohol, tobacco or drugs to minors.
    
3.  _2.3.3_
    
    Apps must not encourage excessive consumption of or make unnecessary references to alcohol, tobacco or drugs.
    
4.  _2.3.4_
    
    Apps that present medical information to users must notify users that the medical information could be inaccurate.
    

##### 2.4 Defamation and Vulgarity

1.  _2.4.1_
    
    Apps must not visually or audibly present content that could defame (by slander or libel) individual persons or groups of people based on race, gender, sexual preference or identity, ethnicity, nationality, disability, religion, political identity, or ideology.
    
2.  _2.4.2_
    
    Apps must not visually or audibly present excessively unpleasant, repellent, obscene, or vulgar language or expressions.
    
3.  _2.4.3_
    
    Apps must not visually or audibly present offensive, discriminatory, or inflammatory content about specific religious, social, or political groups or concepts.
    
4.  _2.4.4_
    
    Apps must not visually or audibly present content that reasonable public consensus may find to be improper or inappropriate
    
5.  _2.4.5_
    
    Apps must pass all censorship requirements (including but not limited to: political, social, conflict, and security censorship)
    

##### 2.5 Game and Gambling

1.  _2.5.1_
    
    Apps must not offer real money or prizes.
    
2.  _2.5.2_
    
    Apps must not support or promote gambling (including but not limited to: lotteries, casino activities, sweepstakes, and sports betting).
    
3.  _2.5.3_
    
    For game apps with an 18+ age restriction that are to be distributed in South Korea, the apps must be granted Game Rating and Administration Committee (“GRAC”) certificate.
    

##### 2.6 User Generated Content

1.  _2.6.1_
    
    Apps with user-generated content must have a mechanism to filter restricted content from the app.
    
2.  _2.6.2_
    
    Apps with user-generated content must have measures to cope with intellectual property infringement.
    
3.  _2.6.3_
    
    Apps with user-generated content must provide users with a method and instructions for users to report to the app’s registering person or entity issues of restricted content or intellectual property infringement.
    

##### 2.7 Advertisements

Apps must not contain or present advertisements that have any type of the following content:

*   Violence toward or abuse of humans or animals
*   Sexual content (including but not limited to: pornography, pedophilia, and bestiality)
*   Sites (including but not limited to: adult friend finder and dating site) that have a sexual emphasis or adult toys, videos, or products
*   Obscene, vulgar, or inappropriate language content
*   Defamatory, libelous, slanderous, or unlawful content
*   Promotion of or unnecessary references to alcohol, tobacco, and drugs
*   Offensive references or discrimination towards individual persons or groups of people based on race, gender, sexual preference or identity, ethnicity, nationality, disability, religion, political identity, or ideology
*   Overtly political communication
*   Illegal activities, services , or substances
*   Any description, depiction, or encouragement of an illegal substance
*   Illegal, false, or deceptive investment or money-making advice, promotions, or opportunities
*   Pharmaceutical products that are not certified in the countries the apps are to be distributed to
*   Content that reasonable public consensus may find to be improper or inappropriate

#### 3\. Legal

This section related to lawful matters.

##### 3.1 Privacy

1.  _3.1.1_
    
    Apps that access, collect, use, transmit, or share user data (including but not limited to: user location, calendar, or SMS/MMS information) must comply with applicable local laws and [Samsung Service Terms and Conditions](https://account.samsung.com/membership/terms)
    
2.  _3.1.2_
    
    Apps that access, collect, use, transmit, or share user data must display the applicable user data privacy policy in their apps and submit the policy in the Privacy Policy URL field during registration.
    
3.  _3.1.3_
    The app’s privacy policy must include the following information.
    
    *   Collected user data items and types
    *   Purposes of using user data
    *   List of third-parties that the app shares user data and shared data types
    *   User data items and data types that the app shares with third-parties
    *   User data retention period and user data deletion (for example, upon account deletion or app uninstallation)
    *   Method of notifying users when the privacy policy is revised
    *   User data-related privileges (such as reading, revising, or deleting data) that can be requested by users
4.  _3.1.4_
    
    When the user data privacy policy is revised, users must be notified and the Privacy Policy URL in the app registration must be updated.
    
5.  _3.1.5_
    
    3.1.5 Apps must not access, collect, or use user data without legitimate user consent in accordance with local laws.
    
6.  _3.1.6_
    
    Apps must not require that the user grant more permissions or provide more personal information than the minimum necessary for the app to successfully support its features.
    
7.  _3.1.7_
    
    Apps must not display advertisements or push messages based on user data without first getting user consent to do so.
    
8.  _3.1.8_
    
    Apps must not initiate or support security warnings or other malicious means that try to obtain user data.
    

##### 3.2 Intellectual Property

1.  _3.2.1_
    
    Apps must not copy aspects of any app being distributed via the Galaxy Apps store.
    
2.  _3.2.2_
    
    Apps must not support the download of any other app by a direct method from inside the app (for example, via an APK).
    
3.  _3.2.3_
    
    Apps must not display, depict, or use the Samsung brand, trademark, logo, or other Samsung identifiers.
    
4.  _3.2.4_
    
    Apps must not contain any reference that suggests that the app or its registering person or entity has a relationship with Samsung or misleads users about any Samsung device.
    
5.  _3.2.5_
    
    Apps that include Free and Open Source Software (FOSS) must comply with applicable open source software license terms.
    
6.  _3.2.6_
    
    Apps must not include, present, or use any material whose use is protected by the laws of any country the app is being distributed (including but not limited to copyrighted, trademarked, and patented material) without first getting the permission of the rightful owner.
    
7.  _3.2.7_
    
    3.2.7. For apps that include, present, or use protected material or support a method to share or download material not owned by the person or entity who registered the app, they must first get the permission of the rightful owner and maintain evidence of the permission, and must present a copy of the permission to Samsung upon demand.
    
    Note
    
    If you find protected material to inappropriately be inside an app or find material to inappropriately be available via an app distributed in a Samsung app store, please directly contact the person or entity who registered the app or [submit a claim to Samsung](https://help.content.samsung.com/csseller/main/main.do).
    

##### 3.3 Kids Apps

1.  _3.3.1_
    Apps to be distributed in the Kids category of the Samsung Galaxy Apps store:
    
    *   Must comply with applicable children’s privacy laws and statutes of the countries the app is distributed to, which includes but is not limited to the Children’s Online Privacy Protection Act (COPPA).
    *   Must be designed for children under 13 years of age.
    *   Must not include in-app purchase items, advertisements, or purchasing functionality.
    *   Must not contain links to outside of the app.

##### 3.4 Miscellaneous

1.  _3.4.1_
    
    Apps must comply with all local laws of the countries that apps are to be distributed to.
    
2.  _3.4.2_
    
    Apps must observe and comply with all legal requirements and local customs of the countries that the app is distributed to.
    
3.  _3.4.3_
    For apps that are to be distributed in South Korea:
    
    *   Apps must comply with the Act on Promotion of Information and Communications Network Utilization and Information Protection, and all other relevant Republic of Korea laws.
    *   App registration must define the required and optional permissions describe why and how they are used.
4.  _3.4.4_
    Apps must not visually or audibly present or encourage any type of the following content:
    
    *   Overtly political communication
    *   Illegal activities, services, or substances
    *   Illegal, false, or deceptive investment or money-making advice, promotions, or opportunities
    *   Pharmaceutical products that are not certified in the countries the apps are to be distributed to