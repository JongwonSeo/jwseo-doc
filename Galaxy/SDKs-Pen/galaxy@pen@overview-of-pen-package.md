1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pen](/galaxy/pen)
5.  [Technical Document](/galaxy/pen#techdocs)
6.  [Overview of Pen package](/galaxy/pen/overview-of-pen-package)

# Overview of the Pen packageSep 28, 2017

## Introduction

Mobile phone touch interface has evolved and changed through the years. Along with this change came a need for touch precision and accuracy in various applications. The introduction of the S Pen stylus addresses this need by giving users a pen like feel may it be a memo application where notes are created by free hand writing, a drawing application where images are hand crafted to the artists liking and a game that requires precision in movement to name a few.

## 1\. Implementation

#### 1.1. Step 1: Initialization

The Samsung Mobile SDK 1.0.4 implementation starts with initializing the instance of the Spen class by calling the initialize() method requiring a context as parameter.

#### 1.2. Step 2: Creating an S Pen Surface View

Simply create an object of the SpenSurfaceView class then set the object as parameter to the Relative layout addView() method.

#### 1.3. Step 3: Adding the Pen Setting View

To add a Pen Setting View, add the same instance of the Relative layout used in creating the S pen surface view to the instance of the SpenSettingPenLayout through its constructor method requiring it as a parameter.

#### 1.4. Step 4: Adding the Eraser Setting View

To add a Pen Setting View, add the same instance of the Relative layout used in creating the S pen surface view to the instance of the SpenSettingEraserLayout through its constructor method requiring it as a parameter.

## 2\. Dependencies

The S Pen 5.0 has the following system requirements:  
a) Samsung S Pen Capable Device (minimum resolution of 800 x 480)  
b) Android Version 5.0 (Lollipop API 21) default operating system

## 3\. Uses

The S Pen can be used in a wide variety of applications. The following presents some of the usual and not so usual cases where this can be utilized.

#### 3.1. Drawing

The S Pen gives users the ability to create simple doodles to complex sketches.

![Image 1 : Simple Doodle from the Sample Application](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview01.jpg) 

<center>Image 1 : Simple Doodle from the Sample Application</center>

#### 3.2. Note Taking

Having the versatility of creating free hand notes using an electronic device; S Pen capable devices give users the edge in having a handy notepad anytime..

![Image 2 : Notes using Samsung Notes Application](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview02.jpg) 

<center>Image 2 : Notes using Samsung Notes Application</center>

#### 3.3. Photo Editing

Modifying images requires accurate tools and the S Pen gives just that.

![Image 3: Image Editing using Photo Editor Application](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview03.jpg) 

<center>Image 3: Image Editing using Photo Editor Application</center>

#### 3.4. Gaming

Getting an edge over the competition, the S Pen offers a more precise touch control for games that demand it..

## 4\. Sample Application Screenshots

The S Pen sample application that is included in the SDK package varies greatly with the first release of the S Pen library with the newest release. Delving deeper into this, a few screenshots with brief description are provided.

![Image 4: Main Menu of the Old and New Sample Application](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview06.jpg) 

<center>Image 4: Main Menu of the Old and New Sample Application</center>

The old sample application presents 3 main options each of which transition to more options while the new application immediately shows all the necessary applications that showcase the features of S Pen.

![Image 5: Old and New Sample Application Basic Pen Stroke](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview07.jpg) 

<center>Image 5: Old and New Sample Application Basic Pen Stroke</center>

The writing style looks the same but the difference lies in the area interacted with the pen. The old application’s absolute minimum zoom view is the default screen and the maximum scales up in a sense that the canvas presents the strokes with high pixilation. The newer provides a default drawable canvas that can be zoomed in and out. Pixilation when zooming in is not the same as the old application but is still present. Zooming out decreases the canvas size, this is to determine the zoomed out state.

![Image 6: Old and New Sample Application Basic Pen Stroke Maximum Zoom-In](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview08.jpg) 

<center>Image 6: Old and New Sample Application Basic Pen Stroke Maximum Zoom-In</center>

![Image 7: New Sample Application Basic Pen Stroke Maximum Zoom-Out](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview09.jpg) 

<center>Image 7: New Sample Application Basic Pen Stroke Maximum Zoom-Out</center>

S Pen 5.0 introduces a new view, SpenComposerView which provides you a new way to compose a note. It supports various content types allow taking note in text, handwriting, drawing, attach image, voice and webcart.

![Image 8: New Sample Application with SPenComposerView](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview13.jpg) 

<center>Image 8: New Sample Application with SPenComposerView</center>

In addition SPen 5.0 offers seven types of specialized drawing brushes such as Water Color, Oil, Calligraphy, Pencil, Crayon, Airbrush, and Marker.

![Image 9: The oil painting example](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview14.PNG) 

<center>Image 9: The oil painting example</center>

## 5\. Advantages of the new SDK

Most of the new features of the latest SDK are internal to the service which means that they may not be necessarily seen but can definitely be felt in terms of user experience. The following are the most notable improvements that make the new SDK.

#### 5.1. Redesigned Architecture

The new architecture is based on a Model-View-Control (MVC) pattern. The main strength of the MVC pattern is to segregate where components and method should be placed. This makes it easier for developers to work with an organized code environment in order to easily extend or maintain code.

![Image 10 : Model-View-Architecture Pattern of S Pen 3.0](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview10.jpg) 

<center>Image 10 : Model-View-Architecture Pattern of S Pen 5.0</center>

#### 5.2. Improved PerformanceModel-View-Architecture Pattern of S Pen 5.0

The internal engine of the SDK has been revamped from the native layer going up. The native layer houses C/C++ codes which communicate with the kernel better than the Java layer because they both have the same language. The engine gives off an added 30% improvement to performance of the S Pen.

![Image 11 : S Pen 3.0 Native and Java Layer](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview11.jpg) 

<center>Image 11 : S Pen 5.0 Native and Java Layer</center>

#### 5.3. New Packaging

Using the new S Pen SDK in projects only requires the pen-v5.0.0.aar file and the sdk-v1.0.1.jar file. The .so files are no longer needed since they were only needed for a mobile device’s architecture support. The architecture of the mobile device is now blurred from the user making it irrelevant focusing more on the mechanism of the pen. This makes it easier to enable S Pen features in applications.

![Image 12 : S Pen 3.0 Native and Java Layer](https://developer.samsung.com//sd2_images/galaxy/content/pen_overview12.jpg) 

<center>Image 12 :S Pen 5.0 Native and Java Layer</center>

## 6\. Summary

The S Pen 5.0 is currently the most up to date version of the library that includes all the features that can be used to create Pen intuitive applications. It is often better to use a newer SDK to get the latest enhancements, features and fixes.

## 7\. References

*   [http://developer.samsung.com](http://developer.samsung.com)
*   [http://homepages.inf.ed.ac.uk/jbednar/writingtips.html](http://homepages.inf.ed.ac.uk/jbednar/writingtips.html)
*   [http://developer.samsung.com/s-pen-sdk/sdk/S-Pen-SDK-2-3](http://developer.samsung.com/s-pen-sdk/sdk/S-Pen-SDK-2-3)
*   [http://samsungdevus.com/sites/default/files/GettingStartedwithSPenintheNewSamsungMobileSDK-Cheng.pdf](http://samsungdevus.com/sites/default/files/GettingStartedwithSPenintheNewSamsungMobileSDK-Cheng.pdf)
*   [http://www.samsung.com/us/support/supportOwnersHowToGuidePopup.do?howto\_guide\_seq=8645&prd\_ia\_cd=N0000004&map\_seq=58035](http://www.samsung.com/us/support/supportOwnersHowToGuidePopup.do?howto_guide_seq=8645&prd_ia_cd=N0000004&map_seq=58035)
*   [http://apps.samsung.com/mercury/topApps/topAppsDetail.as?productId=000000509170&srchClickURL=|@sn=SAPS|@qh=-162165613|@qid=SAPS.SRCH.CJK.PRX|@q=beeline|@tot=1|@idx=0|@doc=G00010056237|@title=nil](http://apps.samsung.com/mercury/topApps/topAppsDetail.as?productId=000000509170&srchClickURL=|@sn=SAPS|@qh=-162165613|@qid=SAPS.SRCH.CJK.PRX|@q=beeline|@tot=1|@idx=0|@doc=G00010056237|@title=nil)
*   [http://www.youtube.com/user/SamsungMobile/search?query=Pen](http://www.youtube.com/user/SamsungMobile/search?query=Pen)
*   [http://www.samsung.com/us/support/supportOwnersHowToGuidePopup.do?howto\_guide\_seq=8645&prd\_ia\_cd=N0000004&map\_seq=58035](http://www.samsung.com/us/support/supportOwnersHowToGuidePopup.do?howto_guide_seq=8645&prd_ia_cd=N0000004&map_seq=58035)