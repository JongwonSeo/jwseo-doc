1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pen](/galaxy/pen)

# Pen

## Essential Contents

*   [Pen SDK Version : 5.1Mar 15, 2018](/common/download/check.do?actId=1190)
*   [API Reference - Full version](http://img-developer.samsung.com/onlinedocs/sms/pen/index.html)
*   [API Reference - Light version](http://img-developer.samsung.com/onlinedocs/sms/pen-light/index.html)
*   [Programming Guide - Full Version](/galaxy/pen/guide)
*   [Programming Guide - Light Version](/galaxy/pen/light/guide)
*   [Release Note](/galaxy/pen/releases)

Note

*   As of Jan. 1, 2019, the Pen SDK is deprecated on all devices. We regret any inconvenience.
*   We are planning to host S Pen Remote SDK for BLE remote access capability on Galaxy Note 9 device.(coming soon)![new](https://developer.samsung.com//sd2_images/common/text/new.png)

Note

1) In the Pen SDK v5.0, Dynamic version is not supported, only it supports Static version.

2) Static version has two types, namely:

*   Full Version : Supports all features of Pen SDK.
*   Light version : Only supports simple drawing features of Pen SDK.  Please download the Full Version for advanced drawing brushes
    

3) If you want to download the Pen SDK v4.1.2, you can download [here](/common/download/check.do?actId=1002)

## What Is Pen SDK?

Pen SDK allows you to develop applications, such as Samsung Notes, that use handwritten input. Pen SDK uses pens, fingers or other kinds of virtual pens for input and makes it feel like you are actually writing on a notepad with various useful editing functions.

![Figure 1: Pen SDK](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_1.png) 

<center>Figure 1: Pen SDK</center>

With Pen SDK, creating Android applications that handle handwritten inputs is quite simple. All that needs to be done is add a view provided by Pen SDK onto the application’s layout. Through this view, inputs like pressure and speed from the S Pen are captured and processed precisely to give the realistic feel of a real pen and paper to users. Created contents such as handwritings, drawings or shapes via Pen SDK can also be edited with the various advanced editing features provided by the SDK, which can increase the satisfaction level of application users.

Pen SDK 5.0 has been improved performance-wise and has added new features. The overall performance has been boosted up with the use of hardware such as GPU adaptively while reducing the power consumption. Paragraph formatting of the text box is improved to provide users with the richer ways of editing texts. And the newly added recognition engine enables the new method to add or edit various shapes without accessing the menus.

Pen SDK supports the following features:

*   Advanced drawing brushes and canvas.
*   Editing features for the MS-Word usage.
*   Handwriting recognition for mathematical formula, shapes and 60 languages.

#### Advanced drawing brushes and canvas

Pen SDK provides advanced ways to edit documents effectively with pens on relatively small screens. Basic objects like handwritings, drawing or shapes can be selected with a rectangle or an arbitrary shape using the lasso. Properties of the selected objects can changed with the built-in functions: group/ungroup, change z-order, color or size, etc.

![Figure 2: Advanced editing](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_2-1.png)
![Figure 2: Advanced editing](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_2-2.png)
![Figure 2: Advanced editing](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_2-3.png)

<center>Figure 2: Advanced editing</center>

The drawing experience has also been overhauled, enabling users to fully unleash their creativity with new drawing tools, and more easily share their digital artworks with the world.

The oil paintbrush allows users to mix multiple colors and create different textures, and the watercolor paintbrush enables color layering. Additionally, with the calligraphy brush, users can change the thickness of their strokes by applying different pressure levels...

![Figure 3: Gogh by kstar_ssong from PENUP](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_3.jpg)

<center>Figure 3: Gogh by kstar\_ssong from PENUP</center>

![Figure 4: La Siesta by kstar_ssong from PENUP](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_4.jpg)

<center>Figure 4: La Siesta by kstar\_ssong from PENUP</center>

#### Editing features for the MS-Word usage

S Pen 5.0 introduces a new view, SpenComposerView which provides you a new way to compose a note. With it, users can now create, edit and manage all S Pen creations and text notes easily from one convenient location. It supports various content types allow taking note in text, handwriting, attach images and even you can create an artwork on your notes.

![Figure 5: Composing feature](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_5.jpg) 

<center>Figure 5: Composing feature</center>

Another useful addition is S Pen 5.0’ optimized text input options. With new support for check boxes, numbering and font colors, organizing to-do and shopping lists is a breeze.

![Figure 6: Text box](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_6.PNG) 

<center>Figure 6: Text box</center>

#### GHandwriting recognition for mathematical formula, shapes and 60 languages

The needs of using shapes while making contents have been there for a while to make richer documents. Limitations of the mobile environment, such as small screen size, no hardware input devices or handheld usage, still exist, but the recognition engine inside Pen SDK 5.0 will automatically converts the user’s rough inputs into much refined shapes. Once shapes have been recognized and added to documents, more editing options can be applied to the added shapes, which can be compatible to those widely used word processors on PC.

![Figure 7: Recognition based shape editing](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_7.jpg) 

<center>Figure 7: Recognition based shape editing</center>

Pen SDK can convert input strokes into various useful data format with the built-in recognition and beautification engine. Using the text recognition, user’s handwritten texts on screen can be converted into meaningful text that can be used for searching or can be highlighted.

![Figure 8: Searching handwritten text](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_8.jpg) 

<center>Figure 8: Searching handwritten text</center>

Also, using formula recognition and shape beautification, handwritten can be converted into mathematic equations with better looking objects.

![Figure 9: Sketch to vector](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_9.jpg) 

<center>Figure 9: Sketch to vector</center>

![Figure 10: Formula recognition](https://developer.samsung.com//sd2_images/galaxy/content/sms_pen_over_10.jpg) 

<center>Figure 10: Formula recognition</center>

#### Restrictions

Pen SDK has the following restrictions:

*   Pen SDK supports devices with Android 5.0 (Lollipop API level 21) or higher.
*   Some functions such as writing pressure or hover will be limited on the devices without the built-in S Pen.

## Technical Document

*   [API Reference: Full Version](http://img-developer.samsung.com/onlinedocs/sms/pen/index.html)
*   [API Reference: Light Version](http://img-developer.samsung.com/onlinedocs/sms/pen-light/index.html)
*   [Release Note](/galaxy/pen/releases)
*   [Programming Guide: Full Version](/galaxy/pen/guide)
*   [Programming Guide: Light Version](/galaxy/pen/light/guide)
*   [Overview of Pen package](/galaxy/pen/overview-of-pen-package)
*   [Using Pen SDK in Eclipse development environment](/galaxy/pen/using-pen-sdk-in-eclipse)

## Samples

*   [PenSample App](/common/download/check.do?actId=1119)

## FAQ

*   Q01 A
    You can use the Pen package on any Samsung Galaxy series device, but the Galaxy Note series devices offer you more functions when you use your S Pen stylus. You can also use the Pen package on devices with Android 4.0 Ice Cream Sandwich or higher.
    
*   Q02 A
    Files saved in S Pen SDK 2.3 can be properly loaded in the Pen package.
    
*   Q03 A
    The UnsatisfiedLinkError exception usually occurs when you try to use the Pen package without initializing it. Call the Spen.initialize() method to initialize the Pen package before you use it.   
    
*   Q04 A
     Yes. You can use the setToolTypeAction() method in the Pen package to select what hapens for different tools. The sample code below shows you how to configure the action type for finger (TOOL\_FINGER) to stroke (ACTION\_ STROKE), which allows you to draw with your finger.  
    
    SpenSurfaceView.setToolTypeAction(SpenSurfaceView.TOOL\_FINGER, SpenSurfaceView.ACTION\_STROKE);
    
*   Q05 A
    The Pen package methods are not thread-safe, but you can use the methods in multiple threads when they do not share instances. Instance Sharing, which uses a single instance for multiple threads, is not supported. Create a new instance internally to use it in a separate thread.
    

[1](#1) [2](#2) [3](#3)

## FAQ

*   Q06 A
    The SecurityException is an essential permission of S Pen package. Add the following permission in your Android manifest file (AndroidManifest.xml):  
       <uses-permission android:name="com.samsung.android.providers.context.permission.WRITE\_USE\_APP\_FEATURE\_SURVEY"/>  
    
*   Q07 A
    To disable the SpenControlShape, you must use API setTouchEnabled(false) of SpenControlShape.<br />
    
*   Q08 A
    In Pen SDK 5.0, only SpenPaintingSurfaceView & SPenPaintingView support foreground image for canvas by using API setLayerBackgroundBitmap() instead of API setClearImageBitmap().
    
*   Q09 A
    You can’t open the note because you are using constructor of notedoc incorrectly. You should use SpenNoteDoc(Context context, String filePath, int width, int mode) API with mode SpenNoteDoc.MODE\_READ\_ONLY or SpenNoteDoc.MODE\_WRITABLE.
    
*   Q10 A
    To erase the whole line, you must use API SpenSurfaceView.setToolTypeAction(int toolType, int action) with toolType is TOOL\_SPEN or TOOL\_FINGER and action is ACTION\_STROKE\_REMOVER.
    

[1](#1) [2](#2) [3](#3)

## FAQ

*   Q11 A
    You can SpenObjectLine to draw variety of lines.
    
*   Q12 A
    S-Pen's document is only saved by format .spd and .png.
    
*   Q13 A
    Yes, to do disable writing and drawing , you should use API setToolTypeAction(int toolType, int action) of SpenSurfaceView with toolType = TOOL\_FINGER and action = ACTION\_GESTURE.
    

[1](#1) [2](#2) [3](#3)

## Event Materials

*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.93MB)](/common/download/check.do?actId=21&statPath=/event/pen/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/pen/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/pen/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.94MB)](/common/download/check.do?actId=24&statPath=/event/pen/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/pen/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.79MB)](/common/download/check.do?actId=26&statPath=/event/pen/download/MWC_SESSION_Group_play_V3.pdf)