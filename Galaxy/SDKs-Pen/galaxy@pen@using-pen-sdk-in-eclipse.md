1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pen](/galaxy/pen)
5.  [Technical Document](/galaxy/pen#techdocs)
6.  [Using Pen SDK in Eclipse development environment](/galaxy/pen/using-pen-sdk-in-eclipse)

# Using Pen SDK in Eclipse development environmentSep 28, 2017

*   Download Pen SDK library from the developer site  [http://developer.samsung.com/galaxy/pen](http://developer.samsung.com/galaxy/pen)
    
*   Extract the ZIP file and find AAR file. Then extract the AAR file to AAR folder, which has structure as below :

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_01.jpg)

*   Do 3 steps as below:
    *   In the extracted folder rename the contained file classes.jar to whatever you like (in this example spensdk5.0.jar) and move it to the libs folder within the extracted folder.
    *   Copy armeabi-v7a folder in jni folder to the libs folder within the extracted folder.
    *   After, delete jni folder

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_02.png) 

<center>Extracted folder</center>

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_03.png) 

<center>lib folder</center>

*   Now in Eclipse you can File -> New -> Project -> Android Project from existing source, and point to the extracted folder content.

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_04.png)

*   Open the .project file in extracted folder content and look for the XML name tag and replace the contents of it with spensdk5.0 (or whatever you called your jar file above) and save.

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_05.png)

*   After import right click on the newly created project, select Properties -> Android, and check Is Library.

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_06.png)

*   In your main project that you want to use the library for, also go to Properties -> Android and add the newly added myProjectLib to the list of dependencies.
*   Add files as below to libs folder of your projects.

![](https://developer.samsung.com//sd2_images/galaxy/content/pen_eclipse_dev_environment_07.png)