1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pen](/galaxy/pen)
5.  [Technical Document](/galaxy/pen#techdocs)
6.  [Programming Guide: Light Version](/galaxy/pen/light/guide)

# Programming Guide - Light VersionSep 28, 2017

*   [ProgrammingGuide\_Pen\_Light.pdf](/common/download/check.do?actId=1110)

Pen SDK Light allows you to develop applications that use handwritten inputs. It uses a S pen, finger, or other kinds of virtual pens to provide faster and more precise user input. This means that Pen SDK Light offers a richer set of features than existing input tools. Because it senses the pressure underneath its tip, Pen SDK Light makes it feel more like you are actually writing or drawing on the device.

Pen SDK Light provides functions for verifying if the Spen is activated, identifying event coordinates, sensing the pressure, verifying if the side button is pressed, processing hover events and more for your application.

You can use Pen SDK Light to:

*   Draw using a finger and/or S pen
*   Set user preferences for pens and erasers
*   Edit and save input stroke objects as a file
*   Manage history for undo and redo commands

Note

See attached programming guide pen light pdf file for more information.