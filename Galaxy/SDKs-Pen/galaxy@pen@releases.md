1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pen](/galaxy/pen)
5.  [Technical Document](/galaxy/pen#techdocs)
6.  [Release Note](/galaxy/pen/releases)

# Release Note Mar 15, 2018

## Introduction

*   #### Release Version:
    5.1
    
*   #### Release Date:
    March 15th, 2018
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126"> <col width="123"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Pen SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Pen SDK APIs, see below.</td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, see below.</td></tr></tbody></table>
  

## Known Issues

*   Some features may not operate properly on low-end devices which have GPU does not support OpenGL ES 2.0 or above and extensions GL\_EXT\_unpack\_subimage, GL\_EXT\_blend\_minmax. Known issues will be fixed on the next SPen SDK update.
    *   Erasing strokes operates incorrectly.
    *   Drawing stroke operates incorrectly.
    *   Canvas is blink while zoom canvas or draw strokes slowly.
    *   All of object are broken when move them.

## Change History

*   *   Samsung Pen SDK 5.1 has been released to improve text performance.*   Advanced drawing brushes on Lite version is deprecated.
    *   Bugs fixed in Android O.
    
*   *   New features.    *   Advanced drawing brushes and canvas.
        *   Editing features for the MS-Word usage.
        *   Handwriting recognition for mathematical formula, shapes and 60 languages.
    
*   *   Support Android N OS.*   Some bugs were fixed.
        *   Broken emoticons with object shape.
        *   Recognition failed with Chinese and Korean.
        *   Canvas is crack after undo/redo page layer.
        *   Shape is not recognized correctly with Marker Pen.
        *   Forced close after calling API retakeStrokeFrame() many times.
    
*   *   Some bugs were fixed.    *   Broken tiles with Mali GPU.
        *   Support Samsung Flag font.
        *   Forced closed issue with object shape.
    
*   *   Only supports static mode.    *   Since this version, all application has to include the library and .so files (SDK core).
        *   Your APK size should be increased but user cannot install additional Pen SDK APK.
        *   For APK size, SDK offer the light version. If application uses View and Stroke Object only, it can use the light version. (But, if application want to load a file which include all object types, it have to use the full version).  For more detail information of light version, refer to “Supported Features” chapter in Programming Guide of light version.
    *   Support Android Studio.
        *   Since this version, SDK will be released by AAR format which include all library, native shared objects and image/xml resources.
        *   If developer just adds the ARR file into application project, you can include Pen SDK all.  For more detail information, refer to Programming Guide document.
        *   If you want to use the Eclipse yet, please refer to “Technical Document” in developer web site.
    *   com.samsung.android.sdk.pen.recognition package.
        *   In this release, new API added.*   SpenSignatureVerification.completeRegistration()
            *   SpenSignatureVerification.isRegistrationCompleted()
            *   SpenSignatureVerification.isRegistrationPrepared()
            *   SpenSignatureVerification.prepareRegistration(List )
    *   com.samsung.android.sdk.pen.document package.
        *   In this release, new API added.*   SpenObjectLine.SpenObjectLine(int type, PointF startPoint, PointF endPoint, boolean isTemplateObject)
    *   com.samsung.android.sdk.pen.settingui package.
        *   In this release, some classes are removed.*   SpenSettingTextLayout.java
            *   SpenSettingDockingLayout.java
    *   com.samsung.android.sdk.pen package.
        *   The field Spen.SPEN\_DYNAMIC\_LIB\_MODE is removed. (Samsung Pen SDK 4.1.19 only supports static mode).
    *   com.samsung.android.sdk.pen.engine package.
        *   In this release, new classe is added.*   SpenSimpleSurfaceView*   This class supports simple drawing (Stroke / Eraser / Remover).
        *   Some APIs are removed.*   SpenSimpleView.clearScreen()
            *   SpenSimpleView.getShapeSettingInfo() / setShapeSettingInfo(SpenSettingShapeInfo)
            *   SpenSimpleView.getTextSettingInfo() / setTextSettingInfo(SpenSettingTextInfo)
            *   SpenMultiView.getRemoverSettingInfo() / setRemoverSettingInfo(SpenSettingRemoverInfo)
            *   SpenMultiView.getTextSettingInfo() / setTextSettingInfo(SpenSettingTextInfo)
            *   SpenMultiView.getShapeSettingInfo() / setShapeSettingInfo(SpenSettingShapeInfo)
            *   SpenMultiView.getSelectionSettingInfo() / setSelectionSettingInfo(SpenSettingSelectionInfo)
    
*   *   Samsung Pen SDK 4.0.7 has been released to fix memory management with stabilized recognition libraries.
*   *   Samsung Pen SDK 4.0.5 has been released to fix backward compatibility issue in Android ICS (4.0.3).
*   In this release, we include some new APIs:
    *   Samsung Pen SDK 4.0.0 has been released with stabilized shape recognition engine.
    *   com.samsung.android.sdk.pen.document package.
        *   In this release, new objects are added:*   SpenObjectShapeBase*   Base class of SpenObjectShape and SpenObjectLine.
                *   This manages outline properties and connection feature.
            *   SpenObjectShape*   Defines a generic “shape” in Pen SDK.
                *   Supports the predefined shape types.
                *   Supports the outline and filling properties.
                *   Supports the rich-text properties.
                *   SpenObjectLine can connect to the specified point of SpenObjectShape.
            *   SpenObjectLine*   Defines a generic “line” in Pen SDK.
                *   Connect between the SpenObjectShapes.
                *   Supports the line properties and arrow types.
                *   If the connected SpenObjectShape is moved, SpenObjectLine will be changed automatically.
            *   Refer to “4.2.4 Inserting Shape/Line Objects” of the programming guide document.
        *   SpenNoteFileSignature class is added for lock/unlock using user signature.
        *   The base class of SpenObjectTextBox and SpenObjectImage is changed from SpenObjectBase to SpenObjectShape.
        *   The text span and paragraph in SpenObjectTextBox.*   The internal classes for text span and paragraph are separated from SpenObjectTextBox*   SpenObjectTextBox.TextSpanInfo→SpenTextSpanBase
                *   SpenObjectTextBox.BoldStyleSpanInfo→SpenBoldSpan
                *   SpenObjectTextBox.FontNameSpanInfo→SpenFontNameSpan
                *   SpenObjectTextBox.FontSizeSpanInfo→SpenFontSizeSpan
                *   SpenObjectTextBox.ForegroundColorSpanInfo→SpenForegroundColorSpan
                *   SpenObjectTextBox.HyperTextStyleSpanInfo→SpenHyperTextSpan
                *   SpenObjectTextBox.ItalicStyleSpanInfo→SpenItalicSpan
                *   SpenObjectTextBox.UnderlineStyleSpanInfo→SpenUnderlineSpan
                *   SpenObjectTextBox.TextParagraphInfo→SpenTextParagraphBase
                *   SpenObjectTextBox.AlignParagraphInfo→SpenAlignmentParagraph
                *   SpenObjectTextBox.LineSpacingParagraphInfo→SpenLineSpacingParagraph
            *   SpenObjectTextBox.BackgroundColorSpanInfo and SpenObjectTextBox.TextDirectionSpanInfo are deprecated.
            *   Old classes should be converted to new classes internally.
            *   SpenBulletParagraph is added.
            *   setTextSpan(), setTextParagraph(), findTextSpan() and findTextParagraph() … are added for new separated classes.
            *   SpenObjectShape can use the span and paragraph classes.
        *   Some features are deprecated or changed.*   SpenObjectImage doesn’t support “image border” and “crop rect” anymore.
            *   In SDK 3.1, when an image set to flipped SpenObjectImage, flip status of this object should be reset and it display the non-flipped image. But since SDK 4.0, flip status is not reset and the flipped image should be displayed.
            *   Adds “compatible mode” parameter to SpenNoteDoc.save(). If “compatible mode” is “false”, shape and line cannot be displayed on SDK 3.1. If “true”, SDK 3.1 can display shape and line object to image type, but save performance should be worse.
        *   Some APIs are added.*   SpenNoteDoc.appendPage(int width, int height)
            *   SpenNoteDoc.insertPage(int width, int height)
            *   SpenNoteFile.releaseCoverImagePath(String coverImagePath)
            *   SpenObjectTextBox.setBulletType(int type)
            *   The “threshold” parameter is added to SpenPageDoc.findTopObjectAtPosition() and findObjectXXX().
            *   SpenPageDoc.setDefaultSaveOption(boolean compatibleMode)
            *   SpenPageDoc.getObjectRectList(int typeFilter)
    *   com.samsung.android.sdk.pen.engine package.
        *   In this release, new classes are added:*   SpenControlShape*   Manages shape object.
            *   SpenControlLine*   Manages line object.
        *   In SpenControlBase and SpenContextMenu, some ‘protected’ variables are removed.
        *   Some APIs are added.*   SpenView. setShapeSettingInfo(SpenSettingShapeInfo info)
            *   SpenView.getShapeSettingInfo()
            *   SpenSurfaceView. setShapeSettingInfo(SpenSettingShapeInfo info)
            *   SpenSurfaceView.getShapeSettingInfo()
            *   SpenSimpleView.setShapeSettingInfo(SpenSettingShapeInfo info)
            *   SpenSimpleView.getShapeSettingInfo()
    *   com.samsung.android.sdk.pen.settingui package.
        *   In this release, Some APIs are added.*   SpenSettingTextLayout.SpenSettingTextLayout(Context context)
            *   SpenSettingTextLayout.SpenSettingTextLayout(Context context, AttributeSetattrs)
            *   SpenSettingTextLayout.SpenSettingTextLayout(Context context, AttributeSetattrs, intdefStyle)
            *   SpenSettingTextLayout.construct(Context context, String customImagePath, HashMap<String, String>fontName,RelativeLayoutrelativeLayout)
    *   com.samsung.android.sdk.pen package.
        *   In this release, new Class is added.*   SpenSettingShapeInfo
            *   This class contains shape settings information.
        *   Some fields are added.*   SpenSettingTextInfo.bulletType
            *   SpenSettingTextInfo.isRTLmode
            *   SpenSettingViewInterface.ACTION\_RECOGNITION
        *   Some APIs are added.*   SpenSettingViewInterface.setShapeSettingInfo(SpenSettingShapeInfo info)
            *   SpenSettingViewInterface.getShapeSettingInfo()
    
*   *   Samsung Pen SDK 3.1.8 supports German handwriting recognition.
*   *   Samsung Pen SDK 3.1.7 has been released to fix backward compatibility issue in Android Jelly Bean (4.1.2).
*   *   Samsung Pen SDK 3.1.6 has been released to improve backward compatibility and have better support for Galaxy S6.*   The number of support languages for Text Recognition is limited to 3 (en\_US, ko\_KR,zn\_CN).
    
*   *   Samsung Pen SDK 3.1.5 has been released to improve 64bit compatibility and have better support for Galaxy S6.*   New API removeCached(Context, String) in com.samsung.android.sdk.pen.document.SpenNoteFile is added.
    
*   In this release, we include some new APIs:
    *   com.samsung.android.sdk.pen.document.SpenObjectStroke
        *   addPoint(PointF, float, int, float, float) is added.
        *   getOrientations() is added.
        *   getTilts() is added.
        *   setPoints(PointF\[\], float\[\], int\[\], float\[\], float\[\])is added.
        *   SpenObjectStroke(String, PointF\[\], float\[\], int\[\], float\[\], float\[\]) is added.
        *   SpenObjectStroke(String, PointF\[\], float\[\], int\[\], float\[\], float\[\], boolean) is added.
    *   com.samsung.android.sdk.pen.document.SpenNoteDoc
        *   attachTemplatePage(String, String, int) is added.
        *   copyPage(SpenPageDoc, int) is added.
        *   getTemplatePageCount() is added.
        *   getTemplatePageName(int) is added.
        *   insertPages(String, int, int) is added.
        *   reviseObjectList(ArrayList<SpenObjectBase>) is added.
        *   _close(boolean)_ is added.
    *   com.samsung.android.sdk.pen.document.SpenPageDoc
        *   appendObjectList(ArrayList<SpenObjectBase>) is added.
        *   getGeoTagState() is added.
        *   removeGeoTag() is added.
    *   com.samsung.android.sdk.pen.settingui.SpenSettingPenLayout
        *   getPenList() is added.
        *   getPenPresetInfoList() is added.
        *   setExtendedPresetEnable(boolean) is added.
        *   setPenList(ArrayList<String>) is added.
    *   com.samsung.android.sdk.pen.settingui.SpenSettingPenLayout.PresetListener
        *   onChanged(int) is added.
    *   com.samsung.android.sdk.pen.engine.SpenView
        *   getZoomPadRect() is added.
        *   isZoomPadDrawing() is added.
        *   setPenButtonSelectionEnabled(boolean) is added.
    *   com.samsung.android.sdk.pen.engine.SpenControlTextBox
        *   isSelectByKey() is added.
    *   com.samsung.android.sdk.pen.engine. SpenSurfaceView
        *   setTextCursorEnabled(boolean) is added.
        *   setZoomPadBoxPosition(PointF) is added.
        *   getZoomPadRect() is added.
        *   getZoomPadRect() is added.
        *   isZoomPadDrawing() is added.
        *   isZoomPadStrorking() is added.
        *   setPenButtonSelectionEnabled(boolean) is added.
    *   com.samsung.android.sdk.pen.Spen
        *   initialize(Context, int, int) is added.
        *   _initialize(Context, int, int, boolean)_ is added.
    
*   *   Samsung Pen SDK 3.1.3 has been released to resolve issues in Android L.
*   *   Samsung Pen SDK 3.1.2 has been released to fix some minor issues in Android L.
*   *   Samsung Pen SDK 3.1.1 has been released to enhanced stability with Android L.
*   *   Samsung Pen SDK 3.1.0 has been released in order to be compatible with Android.*   Documents / Programming Guides: Initialization and related exception flow should be concerned. Please refer to chapter 3 in programming guide for more information.
    
*   *   Samsung Pen SDK 3.0.19 has been released with enhanced stability and better performance.*   ShapeRecognition example is added. (com.samsung.android.sdk.pen.pg.example5\_9).
    
*   *   Improved performance compares to 3.0.18 in re-drawing, selection & moving functions.
*   *   Pen SDK is separated from the Samsung Mobile SDK to make an efficient environment for the application development.*   New permission will be required when you initialize Pen SDK, Refer the programming guide for more information.
    
*   *   Remove ON-KEYDown() from SpenControlBase.java    *   This file is only used internally. So we decided to remove this.
    *   Add onPopulateAccessibilityEvent to SpenTextBox.java
        *   To support android accessibility system. This is not added for SDK user.
    
*   *   com.samsung.android.sdk.pen.document.SpenNoteDoc    *   revertToTemplatePage(intpageIndex, String absolutePath) is added.
    *   com.samsung.android.sdk.pen.settingui.SpenSettingPenLayout
        *   setPresetSaveEnabled(boolean enable) is added.
        *   setSeekBarChangeListener(SpenSettingPenLayout.SeekBarChangeListener listener) is added.
    *   com.samsung.android.sdk.pen.settingui.SpenSettingPenLayout.SeekBarChangeListener
        *   onProgressChanged() is added.
    *   com.samsung.android.sdk.pen.engine.SpenSurfaceView
        *   isZoomPadEnabled() is added.
    *   com.samsung.android.sdk.pen.engine.SpenView
        *   isZoomPadEnabled() is added.
    

## Features

*   Drawing engine based on Stroke offers advanced edit functions, multiple selections, group/ungroup, move forward/backward, and zoom in/out.
*   Various tools, such as brushes and color pens.
*   Recognition function to convert objects written on the canvas to drawings.