1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Accessory](/galaxy/accessory)
5.  [Technical Document](/galaxy/accessory#techdocs)
6.  [Programming Guide](/galaxy/accessory/guide)

# Programming Guide Aug 23, 2018

*   [ProgrammingGuide\_Accessory.pdf](/common/download/check.do?actId=1229)

Accessory allows you to develop applications on Samsung Smart Devices and Accessory Devices. You can connect Accessory Devices to Samsung Smart Devices without worrying about connectivity issues or network protocols.

You can use Accessory to:

*   Advertise and discover Accessory Services.
*   Set up and close Service Connections with one or more logical Service Channels.
*   Support Service Connections using a range of connectivity options.
*   Configure Accessory Service Profiles and roles for Accessory Peer Agents.

Note

See attached programming guide pdf file for more information.