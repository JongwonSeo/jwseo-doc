1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Accessory](/galaxy/accessory)
5.  [Technical Document](/galaxy/accessory#techdocs)
6.  [Release Note](/galaxy/accessory/releases)

# Release Note Aug 23, 2018

## Introduction

*   #### Release Version :
    2.6.1
    
*   #### Release Date :
    August 23, 2018
    
#### Release Contents :
  
  
<table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126"> <col width="123"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Accessory SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides sample applications.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Accessory, see the <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/accessory/index.html" target="_blank">API Reference</a></td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, sample applications, and feature descriptions.</td></tr></tbody></table>
    

## Resolved Issues

## Change History

*   [Samsung Accessory SDK 2.6.1 (Aug 23, 2018)](/galaxy/accessory/releases#)
    *   Fixed minor bugs in SDK and Sample codes.
    
*   [Samsung Accessory SDK 2.6.0 (Jun 14, 2018)](/galaxy/accessory/releases#)
    *   SAAgentV2 is added to avoid background execution limits in Android 8.0(Oreo).
        *   SAAgentV2 has same functionalities with SAAgent
        *   New methods and callback class are added for getting instance of SAAgentV2
        *   Please refer to the programming guide to implement SAAgentV2 or migrate SAAgent to SAAgentV2
    *   Deprecated APIs are removed following the previous notice.
        *   SAAgent.onError(String, int)
        *   SAAgent.onFindPeerAgentResponse(SAPeerAgent, int)
        *   SAAgent.onPeerAgentUpdated(SAPeerAgent, int)
        *   SAAgent.onServiceConnectionResponse(SASocket, int)
    *   Updated Android sample applications.
    
*   [Samsung Accessory SDK 2.5.3 (Nov 09, 2017)](/galaxy/accessory/releases#)
    *   Enable using startForeground in SAAgent to upgrade for Android API 26.
    
*   [Samsung Accessory SDK 2.5.2 (Jun 22, 2017)](/galaxy/accessory/releases#)
    *   Added logic for gathering usages of SDK.
    *   Enabled to start Sub-class of SAAgent.
    *   Removed unused codes.
    *   Fixed minor bugs.
    
*   [Samsung Accessory SDK 2.5.1 (Mar 23, 2017)](/galaxy/accessory/releases#)
    *   Enhanced stability on Android 7.0.
    *   Updated the log for error code to obtain detailed information.
    *   Fixed minor bugs.
    
*   [Samsung Accessory SDK 2.5.0 (Nov 24, 2016)](/galaxy/accessory/releases#)
    *   Support for Accessory Message which enables to send or receive a message without Service Connection between peer devices.
    *   Added several new APIs for supporting Accessory Message.
        *   Added new SAMessage class and additional APIs
        *   Updated Programming guide
        *   Updated API reference
        *   Added new Sample application
        *   Refer to Programming guide, API reference and Sample application in more detail
    *   Fixed minor bugs.
    
*   [Samsung Accessory SDK 2.4.0 (Sep 29, 2016)](/galaxy/accessory/releases#)
    *   Update Programming guide descriptions
        *   Add class diagram for service profile APIs
        *   Remove unnecessary images & descriptions
    *   Update API reference descriptions
        *   Remove unnecessary descriptions for service profile APIs
    
*   [Samsung Accessory SDK 2.4.0 (Sep 8, 2016)](/galaxy/accessory/releases#)
    *   Added several new APIs for getting service profile information
        *   SAAgent.getServiceChannelId()
        *   SAAgent.getServiceChannelSize()
        *   SAAgent.getServiceProfileId()
        *   SAAgent.getServiceProfileName()
    *   Fixed minor bugs.
    
*   [Samsung Accessory SDK 2.3.3 (Jul 21, 2016)](/galaxy/accessory/releases#)
    *   Fixed minor bugs.
    *   Support content URI based file transfer.
    
*   [Samsung Accessory SDK 2.3.2 (Mar 24, 2016)](/galaxy/accessory/releases#)
    *   Fixed minor bugs.
    *   Modified sample applications for multiple Peer Agents.
    *   Added constants for actions.
        *   SAAgent. ACTION\_REGISTRATION\_REQUIRED
        *   SAAgent. ACTION\_SERVICE\_CONNECTION\_REQUESTED
    *   Added error constant for binding failure.
        *   SAFileTransfer.ERROR\_FATAL
    
*   [Samsung Accessory SDK 2.3.0 (Sep 3, 2015)](/galaxy/accessory/releases#)
    *   Support in sending files or folders regardless its location whether public or private folder.
    *   Support for the Android phones, not limited to Samsung.  \* Supported devices vary depending on your region, operator and device brand.
        
    *   Replaced sample provider applications with applications utilized in GEAR.
    *   Added two more sample applications.
    *   New intent actions will be required when you initialize Accessory and Accessory File Transfer. Refer to the programming guide for more information.
    *   Introduced several new APIs supporting file transfer.
        *   SAFileTransfer.cancelAll()
        *   SAFileTransfer.EventListener.onCancelAllCompleted()
        *   SAFileTransfer.close()
    *   Added several new APIs for supporting multiple peers properly.
        *   SAAgent.onFindPeerAgentsResponse()
        *   SAAgent.onPeerAgentsUpdated()
    
    Note
    
    Can get a compilation error (unimplemented methods) unless methods are implemented or overridden.
    
*   [Samsung Accessory SDK 2.2.2 (Feb 12, 2015)](/galaxy/accessory/releases#)
    *   Merged accessory file transfer library into accessory library.
    *   Fixed Android 5.0 explicit intent exception issue.
    
*   [Samsung Accessory SDK 2.1.11 (Nov 20, 2014)](/galaxy/accessory/releases#)
    *   Replaced Gallery sample application as linked type.
    
*   [Samsung Accessory SDK 2.1.11 (Sep 16, 2014)](/galaxy/accessory/releases#)
    *   This version has been released with enhanced stability and better performance.
    *   Fixed inconsistent synchronization issue.
    *   Fixed unmarshalling error in service connection indication from old framework.
    
*   [Samsung Accessory SDK 2.1.8 beta](/galaxy/accessory/releases#)
    *   Added more arguments into the APIs below and a new API for supporting multiple devices properly.
        *   SAAgent.onServiceConnecionResponse()
        *   SAAgent.onError()
        *   SAPeerAccessory.getAccessoryId()
    *   Added error constants for Permission
        *   ERROR\_PERMISSION\_DENIED
        *   ERROR\_PERMISSION\_FAILED
    
*   [Samsung Accessory SDK 2.0.19 beta](/galaxy/accessory/releases#)
    *   The Accessory is separated from Samsung Mobile SDK to make an efficient environment application development.
    *   New permission will be required when you initialize Accessory and Accessory File Transfer. Refer to the programming guide for more information.
    

## Features

*   Accessory
    *   Controls the smart device remotely (for example, controlling music volume).
    *   Provides notification relay or alarm.
    *   Locates the smart device.
    *   Provides the interface to enable any application’s features between the smart device and the accessory device.
*   Accessory File Transfer
    *   Transfers files using Samsung Accessory Service Framework