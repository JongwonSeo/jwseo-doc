1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Accessory](/galaxy/accessory)

# Accessory

## Essential Contents

*   [Accessory SDK Version : 2.6.1 Aug 23, 2018](/common/download/check.do?actId=1227)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/accessory/index.html)
*   [Release Note](/galaxy/accessory/releases)
*   [Programming Guide](/galaxy/accessory/guide)

Note

*   We introduce SAAgentV2 as a workaround solution to avoid Android's new policy - "Background Execution Limit".
*   Previously, you could avoid this policy only by starting SAAgent(Service) in foreground with a notification.
*   However, SAAgentV2 whose functionalities is identical to SAAgent no longer inherits an Android Service. Instead of Android Service, JobService is working seperately from SAAgentV2 in SDK to handle background executions. So you can use SAAgentV2 without a notification.
*   Please refer to Programming Guide and JavaDoc for more details. In particular, Migration Guide attached to the Programming Guide will help you switch from SAAgent to SAAgentV2.
*   If interested in Android's new background process policy, please refer to "Google's official guides for Android 8.0(Oreo)" in the following link: [https://developer.android.com/about/versions/oreo/background](https://developer.android.com/about/versions/oreo/background)

## Overview

Accessory SDK allows you to connect accessory devices to Samsung smart devices. With Accessory SDK, you can define a new service between the accessory and smart device, enabling you to use the various smart device functions from the accessory device. The service is compatible with various connectivity environments, which makes accessory development efficient and convenient.

Accessory SDK adds new functions to the service as Samsung smart devices improve. Future updates will enable the accessory and the smart device to exchange more information and support more interworking.

![Figure 1: Samsung Smart Device and Accessories](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Accessory_01.jpg) 

<center>Figure 1: Samsung Smart Device and Accessories</center>

The Samsung Accessory Eco-system consists of Samsung smart devices and accessory devices which use the Accessory SDK. Samsung smart devices are equipped with the Samsung Accessory Service Framework, which supports various accessory services. Accessory devices use the framework to interwork with Samsung smart devices.

The Accessory SDK provides a single protocol that supports multiple connectivity technologies, such as Wi-Fi, Bluetooth classic, and BLE (Bluetooth v4.0). The Samsung Accessory Service Framework supports service discovery that is independent of the connectivity technology, and establishes connections between applications for data exchange. You do not need technical knowledge of each connectivity model to develop Accessory services.

Accessory devices communicate with Samsung smart devices through the various connectivity channels supported by the devices. Samsung smart devices can be connected to many accessory devices to implement services in applications. Establishing a connection between the Samsung smart device and an accessory device allows the latter to offer additional functions supported by the Samsung smart device.

You can use the Accessory SDK to:

*   Control the smart device remotely (for example, controlling music volume)
*   Send and receive files
*   Provide notification relay or alarm
*   Find the smart device

You can also define and implement your own functionality.

#### Using Remote Control

With Accessory SDK, you can listen to music while carrying your smart device in a bag, and at the same time control the music selection and volume with the Gear accessory device on your wrist. Similarly, you can remotely control other smart device features, like the camera.

![Figure 2: Using an accessory device for remote control](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Accessory_02.jpg) 

<center>Figure 2: Using an accessory device for remote control</center>

#### Providing Notification Relay

The notification relay is a feature that enables you to receive notifications and messages on your smart device remotely (for example, you are walking around the office while your phone is on your desk), by relaying them to your Gear accessory device to let you know that something is going on.

![Figure 3: Notification relay in the accessory device](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Accessory_03.jpg) 

<center>Figure 3: Notification relay in the accessory device</center>

#### Finding the Device

The Accessory SDK provides the “Find My Phone” feature, which allows you to use an accessory device to find your smart device.

![Figure 4: Locating the device with the accessory](https://developer.samsung.com//sd2_images/galaxy/content/SMS_Accessory_04.jpg) 

<center>Figure 4: Locating the device with the accessory</center>

#### Sending and receiving files

Accessory SDK allows you to transfer files between devices using any type of connectivity supported by the Samsung Accessory Service Framework. You can, for example, develop applications that record a voice memo and send it to the smart device (for example, the Voice Memo application on the Gear sends the recorded memo to the Galaxy Note 3 device).

With Accessory File Transfer, you can send files of any size over all connectivity types supported by the Samsung Accessory Service Framework.

#### Restrictions

Accessory SDK has the following restrictions

*   Accessory SDK only supports the GALAXY Gear (from January 2014 version), Gear 2, Gear 2 Neo, Gear S, Gear S2 and Gear S3.
*   Both the smart and accessory device must have the Samsung Accessory Service Framework installed.
*   Devices with Android 4.3 or higher support Accessory SDK (since they are equipped with the Samsung Accessory Service Framework).

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/accessory/index.html)
*   [Release Note](/galaxy/accessory/releases)
*   [Programming Guide](/galaxy/accessory/guide)

## Samples

*   [AccessorySample App](/common/download/check.do?actId=1228)

## FAQ

*   Q01 A
    At first, check your ProGuard configuration. If not, add lines to exclude Accessory SDK. The application cannot work fine or communicate with peer application.
    
*   Q02 A
    A Service is a collection of features tied to a specific application on a Samsung smart device or accessory device. For example, an alarm service can provide features for adding and deleting alarms, enabling and disabling alarms, and receiving alarm alert notifications.  
      
    An Accessory Service Profile defines the service roles, the message formats, and the message sequences for the service. For example, the alarm service mentioned above can define JSON messages to get a list of alarms set on a Samsung smart device so that alarm data can be synced.  
      
    A Service Provider is an application that runs on either a Samsung smart device or an accessory device that provides a service.  
      
    A Service Consumer is an application that runs on either a Samsung smart device or an accessory device that uses information provided by a service provider application.  
      
    For example, the alarm service mentioned above can consist of an alarm service provider application running on a Samsung smart device and an alarm service consumer application running on an accessory device. The alarm service consumer sends a JSON message to get the list of alarms set on a Samsung smart device and the alarm service provider responds with a JSON message that contains a list of alarms set on the Samsung smart device.
    
*   Q03 A
    The Accessory SDK helps you to create applications that benefit from connections between Samsung smart devices and accessory devices. For example, you can sync alarms set on a smart device with a Samsung GEAR series. The Samsung Accessory Service Framework and the Accessory SDK provide an abstraction layer for different connectivity technologies and message transfer mechanisms. You can define messages (for instance, using JSON) and indicate your preferred connectivity method at the application level. The Samsung Accessory Service Framework and the Accessory SDK manage the connections and messaging between the smart device and the accessory device. This lets you focus on the use cases that you want to include in your application without thinking about messaging and connectivity.
    
*   Q04 A
    No. The Accessory SDK provides an abstraction layer for all this information. You only need to know the terminologies defined by the package and the classes and methods exposed by Accessory.
    
*   Q05 A
    Service providers and service consumers use a _SASocket_ object to exchange messages. The `send()` and `onReceive()` methods are used to send and receive data. For more information on the SASocket class, see the Accessory API reference. The actual messages exchanged by a service provider and service consumer can be in JSON or Binary formats. For a detailed explanation of the supported JSON messages, see the relevant Service Profile descriptions.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q06 A
    You use the Accessory Services XML file to register your service provider or service consumer with the Samsung Accessory Service Framework to allow the framework to advertise services. For more information on the XML tags defined in _accessoryservices.xml_, see the “_Using Accessory_” section of Programming Guide.
    
*   Q07 A
    Create an Accessory Services file (accessoryservices.xml) and store it in the /res/xml path of your application. Add the following lines in your Android manifest file (AndroidManifest.xml) for automatic registration:
    
        <receiver android:name="com.samsung.android.sdk.accessory.RegisterUponInstallReceiver">
            <intent-filter>
                <action android:name="com.samsung.accessory.action.REGISTER_AGENT" />
              </intent-filter>
        </receiver>
        
    
    Using the information in your Accessory Services file, the Accessory SDK automatically registers your application with the Samsung Accessory Service Framework when your application is installed.
    
*   Q08 A
    Add the lines below to your proguard-project.txt to exclude Accessory SDK for the ProGuard.
    
    \- keepclassmembers class com.samsung.\*\* { \*;}
    - keep class com.samsung.\*\* { \*; }
    - dontwarn com.samsung.\*\*
    - keepattributes InnerClasses
    - keepclassmembers class \[Application’s SASocket or SAAgent extended class\].\*\* { \*; }
    - keep class \[Application’s SASocket or SAAgent extended class\]\*\* { \*; }
    
    Please refer to the “_Obfuscating The Application Using ProGuard_” section of Programming Guide for more details.
    
*   Q09 A
    Check whether Android permissions below are declared in your AndroidManifest.xml file:
    
        <uses-permission android:name="android.permission.BLUETOOTH" /> <uses-permission android:name="com.samsung.accessory.permission.ACCESSORY_FRAMEWORK" /> <uses-permission android:name="com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY" />
    
*   Q10 A
    Yes. If the application is using the Accessory File Transfer Service, it must also include the Accessory library in its libraries. But from 2.3.0, the Accessory and Accessory File Transfer libraries are combined into one. So, you don’t need to care about it.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q11 A
    No. Applications can use the Accessory File Transfer Service whether they have a service connection with their peer or not.
    
*   Q12 A
    Check whether the broadcast receiver for incoming file transfer is declared on the receiver side. The broadcast receiver must be declared in the AndroidManifest.xml file:
    
        <receiver android:name="com.samsung.android.sdk.accessoryfiletransfer.SAFileTransferIncomingRequestReceiver"> <intent-filter> <action android:name="com.samsung.accessory.ftconnection" /> </intent-filter> </receiver>
    
*   Q13 A
    Yes. The Accessory File Transfer Service runs on a separate thread, as do the update callbacks. Hence, UI processing has no effect on the file transfer functionality. However, it’s better to avoid heavy UI tasks on the callbacks.
    
*   Q14 A
    Yes. You can list the properties of all the service providers in your Accessory Services file. Also, you can create a single service consumer application for multiple service providers. But, within that application, you need to have separate Service Implementation classes derived from _SAAgent_ for each service provider. For example, if you have a call and SMS service providers in one application, your service consumer application needs to have two separated implementation classes (like Call\_Consumer and Sms\_Consumer, both derived from the _SAAgent_ class) and handle the application logic accordingly
    
*   Q15 A
    Usually yes. But for convenience, Accessory SDK initializes internally when the first call to any Accessory method is made. When your application’s implementation extends the _SAAgent_ class, the Accessory SDK is initialized in `SAAgent.onCreate()`. This means that you do not need to initialize the Accessory SDK as long as your application’s _SAAgent_ subclass does not override `SAAgent.onCreate()`.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q16 A
    The Accessory SDK provides a number of error codes that are propagated to your application for connectivity failures and other error conditions. For a detailed explanation of the error codes, see “Field Detail” for the SAAgent and SASocket classes in the Accessory API reference. You can handle the error codes in your applications to recover from errors. For example, you can display an error popup to the user or try to reestablish a connection after a defined amount of time expires.
    
*   Q17 A
    Yes. You can do this with the `SAAgent.findPeerAgents()` method. When you call `SAAgent.findPeerAgents()`, the Accessory SDK looks for services that match your service profile ID and are peers with your application (for example, if your application is a service consumer, the Accessory SDK returns matching service providers), and indicates the search result in the `SAAgent.onFindPeerAgentResponse(SAPeerAgent, int)` method. If the result parameter in this method is `SAAgent.PEER_AGENT_FOUND`, it means that the specific service is supported in the peer device
    
*   Q18 A
    Your service provider application is registered with the Samsung Accessory Service Framework when your application is installed. When a service consumer peer application begins service discovery and requests a service connection, the Accessory SDK sends your service provider the request using the `SAAgent.onServiceConnectionRequested(SAPeerAgent)` method. You can accept or reject the service connection request using the `SAAgent.acceptServiceConnectionRequest(SAPeerAgent)` or `SAAgent.rejectServiceConnectionRequest(SAPeerAgent)` method. Once the service connection is established, your service provider is up and running. You can start exchanging messages between the service consumer and service provider.
    
*   Q19 A
    No. A service connection can also be initiated by the service provider. The connection establishment logic is the same as in the sample code; service discovery is followed by the establishment of a service connection.
    
*   Q20 A
    Check the Accessory Services description file (accessoryservices.xml) of the service consumer and service provider:  
      
      • The id field of the `<serviceProfile>` tag must be the same for both like `id=”/sample/hello”`.  
      • The id field of the `<serviceChannel>` tag must be the same for both like `<serviceChannel id=”101”`.  
      
    Check also the detailed error codes that are propagated in the `SAAgent.onFindPeerAgentResponse(SAPeerAgent, int)` and `SAAgent.onServiceConnectionResponse(SASocket, int)` methods. For more information on the error codes and on handling error conditions in your application, see the Accessory API reference.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## FAQ

*   Q21 A
    Check whether Android permissions below are declared in your AndroidManifest.xml file:
    
        <uses-permission android:name="android.permission.BLUETOOTH" /> <uses-permission android:name="com.samsung.accessory.permission.ACCESSORY_FRAMEWORK" /> <uses-permission android:name="com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY" />
    
*   Q22 A
    Service, Accessory Service Profile, Service Provider and Service Consumer.
    
*   Q23 A
    The Samsung Accessory Protocol uses the concept of a Service to simplify implementations. Applications define a service profile for communication between peers to support specific use cases. The Accessory SDK and the Samsung Accessory Service Framework provide an abstraction layer to manage connectivity and service profile messaging between applications. You do not need to rewrite basic use case code of a service profile for specific connectivity methods.
    
*   Q24 A
    The Accessory File Transfer Service has its own queuing mechanism. It queues all file transfer requests, even from multiple applications, and services them sequentially. Applications are not allowed to maintain their own queues for file transfers.
    
*   Q25 A
    No. The Accessory File Transfer Service uses its own thread for all file transfers. The file transfer updates are also handled asynchronously. This implementation allows simple and direct API calls to handle the user file transfer.
    

[1](#1) [2](#2) [3](#3) [4](#4) [5](#5)

## Event Materials

*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.93MB)](/common/download/check.do?actId=21&statPath=/event/accessory/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/accessory/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/accessory/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.94MB)](/common/download/check.do?actId=24&statPath=/event/accessory/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/accessory/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.79MB)](/common/download/check.do?actId=26&statPath=/event/accessory/download/MWC_SESSION_Group_play_V3.pdf)