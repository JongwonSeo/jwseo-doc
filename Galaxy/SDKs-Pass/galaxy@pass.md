1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pass](/galaxy/pass)

# Pass

## Essential Contents

*   [Pass SDK Version : 1.2.6Sept 14, 2017](/common/download/check.do?actId=1165)
*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/pass/index.html)
*   [Release Note](/galaxy/pass/releases)
*   [Programming Guide](/galaxy/pass/guide)

Note

We regret to inform you that all devices including upgrade will no longer be providing the Pass SDK from P OS. Applications can use Android Fingerprint API instead of Pass SDK.
![new](https://developer.samsung.com//sd2_images/common/text/new.png)

## Overview

Pass SDK allows you to use fingerprint recognition features in your application. With Pass SDK, you can provide reinforced security, by identifying whether the current user is the actual owner of the device.

![Figure 1: Pass SDK](https://developer.samsung.com//sd2_images/galaxy/content/sms_pass_03.jpg) 

<center>Figure 1: Pass SDK</center>

You can use the Pass SDK to:

*   Request fingerprint recognition
*   Cancel fingerprint recognition requests
*   Verify whether the fingerprint of the current user matches the fingerprint registered on the device
*   Register fingerprints through the Enroll screen
*   Get the index of the identified fingerprint from the array of registered fingerprints
*   Set the index of the fingerprint for recognition requests
*   Add a title for the user interface
*   Add a logo icon for the user interface
*   Set the transparency of elements outside the user interface
*   Set whether the user interface is dismissed or not when touching elements outside it
*   Broadcast actions when registered fingerprint is changed
*   Get the guide for poor quality
*   Set the button for the user interface
*   Change the standby string for the user interface

#### Requesting Fingerprint Recognition

![Figure 2: UI with the Password button (left) and without the Password button (middle) and the latest GUI (right)](https://developer.samsung.com//sd2_images/galaxy/content/sms_pass_04_151217.jpg) 

<center>Figure 2: UI with the Password button (left) and without the Password button (middle) and the latest GUI (right)</center>

You can use a default or customized user interface (UI) for fingerprint recognition. Pass SDK also provides fingerprint recognition without a UI.

The fingerprint recognition UI is available with or without a Password button. With the Password button, the user has the option of providing identification using a device password instead of a fingerprint. To verify if the device supports a Password button, you have to use the related API and test it first. Refer to figure 2.

#### Cancelling Fingerprint Recognition Requests

When fingerprint recognition is requested, the fingerprint sensor waits for the user input to read a fingerprint. If no fingerprint is read within 20 seconds, the request is automatically cancelled. Your application can also cancel the request directly before the 20 seconds are up. If the fingerprint recognition process is not required, cancel the request to save the devices battery.

#### Checking for Registered Fingerprints on the Device

You can verify whether registered fingerprints exist on the device. If there are no registered fingerprints, you can register new fingerprints.

#### Registering Fingerprints

If there are no fingerprints registered in the device, you can use Pass to prompt the user to jump to the Enroll screen where a fingerprint can be registered.

#### Getting the Index of the Identified Fingerprint

If fingerprint recognition is successful, you can get the index of the identified fingerprint from an array of registered fingerprint.

#### Getting a List of Indexes and Names of Registered Fingerprints

You can get a list of registered fingerprints on the device including their indexes and names.

#### Setting a Specific Fingerprint for Recognition

You can tell Pass to check against only one of the registered fingerprints by using its index. If you haven't specified any indexes or if this is set to 'null', the API automatically checks for a match against all registered fingerprints.

#### Adding a Title on the UI

You can add a custom title on the identification dialog.

#### Adding a Logo Icon on the UI

Pass allows you to set a logo. This logo would appear at the bottom left corner of the user interface.

#### Setting the Transparency of Background Elements

You can set a value that can make background elements transparent.

#### Setting Dialog Behavior when Background Elements are touched

If you choose to set this value to true, the user interface would close when a user touches background elements.

#### Broadcast actions when registered fingerprint is changed.

Samsung devices send broadcasts to apps when the registered fingerprint is changed. For example, if fingerprint is added or removed, and all fingerprints are removed, the broadcast receiver can get fingerprint broadcasts.

#### Getting the guide for the poor quality

If you fail the fingerprint recognition due to the poor quality, you can get guide for the reason.

#### Setting the button for the user interface

You can set the button that can connect your own menu after user interface close.

#### Changing the standby string for the user interface

You can set the own standby string on the identification dialog.

#### Restrictions

Pass SDK has the following restrictions:

*   Requires devices with Android 4.2 Jelly Bean (API level 17) or above.
*   Requires Fingerprint sensor.

## Technical Document

*   [API Reference](http://img-developer.samsung.com/onlinedocs/sms/pass/index.html)
*   [Release Note](/galaxy/pass/releases)
*   [Programming Guide](/galaxy/pass/guide)

## Samples

*   [PassSample App](/common/download/check.do?actId=1107)

## FAQ

*   Q01 A
    Yes, Pass retains stable with Swipe Sensor and compatible with Touch Sensor since v1.1.2.
    
*   Q02 A
    No, this is not supported. Fingerprint is enrolled on the device only. This means that every app has access to a shared database of fingerprints.
    
*   Q03 A
    The Pass SDK allows you to use the Samsung Fingerprint Service features in your application.
    
*   Q04 A
    Any Samsung Mobile device that supports the fingerprint sensor and the Fingerprint Service with Android 4.2 (Jelly Bean API 17) and above.
    
*   Q05 A
    No, this is not supported. Pass can only return ‘yes’ if enrolled and ‘no’ if not enrolled.
    

[1](#1)

## Videos

*   
*   

## Event Materials

*Apr 9, 2014[Samsung Developer Day 2014 at MWC](/what-is-new/blog/Samsung-Developer-Day-2014-at-MWC-Session-Videos)    
    *   [MWC\_SESSION\_Gear\_SDK\_V3.pdf (4.92MB)](/common/download/check.do?actId=21&statPath=/event/pass/download/MWC_SESSION_Gear_SDK_V3.pdf)
    *   [MWC\_SESSION\_SamsungMobile\_SDK\_V3.pdf (4.19MB)](/common/download/check.do?actId=22&statPath=/event/pass/download/MWC_SESSION_SamsungMobile_SDK_V3.pdf)
    *   [MWC\_SESSION\_S\_Health\_SDK\_V6.pdf (3.29MB)](/common/download/check.do?actId=23&statPath=/event/pass/download/MWC_SESSION_S_Health_SDK_V6.pdf)
    *   [MWC\_SESSION\_Multi\_Screen\_V3\_final.pdf (1.93MB)](/common/download/check.do?actId=24&statPath=/event/pass/download/MWC_SESSION_Multi_Screen_V3_final.pdf)
    *   [MWC\_SESSION\_Samsung\_GamePlatform\_V5.pdf (1.66MB)](/common/download/check.do?actId=25&statPath=/event/pass/download/MWC_SESSION_Samsung_GamePlatform_V5.pdf)
    *   [MWC\_SESSION\_Group\_play\_V3.pdf (1.78MB)](/common/download/check.do?actId=26&statPath=/event/pass/download/MWC_SESSION_Group_play_V3.pdf)