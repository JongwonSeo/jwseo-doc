1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pass](/galaxy/pass)
5.  [Technical Document](/galaxy/pass#techdocs)
6.  [Programming Guide](/galaxy/pass/guide)

# Programming Guide Sept 14, 2017

*   [Pass Programming Guide](/common/download/check.do?actId=1106)

Pass allows you to use fingerprint recognition in your application to identify users whose fingerprints have been registered in the device.

You can use Pass to

*   Provide better application security.
*   Increase the convenience of the user identification process.

Note

See attached programming guide pdf file for more information.