1.  Devices
2.  [Galaxy](/galaxy)
3.  [SDKs](/galaxy/sdks)
4.  [Pass](/galaxy/pass)
5.  [Technical Document](/galaxy/pass#techdocs)
6.  [Release Note](/galaxy/pass/releases)

# Release Note Sept 14, 2017

## Introduction

*   #### Release Version
    1.2.6
    
*   #### Release Date
    Sept 14, 2017
    
#### Release Contents
 <table class="sd2_table"><caption>Release Contents </caption><colgroup><col width="126px"> <col width="123px"> <col width="*"> </colgroup><tbody><tr><th class="sd2_tc" rowspan="2">SDK</th><td class="sub_th">Libraries</td><td>Provides the Samsung Pass SDK libraries.</td></tr><tr><td class="sub_th">Sample</td><td>Provides a sample application.</td></tr><tr><th rowspan="2">Documents</th><td class="sub_th">API References</td><td>For more information about Pass APIs, see <a class="sd2_link_go" href="http://img-developer.samsung.com/onlinedocs/sms/pass/index.html" target="_blank">API Reference</a>.</td></tr><tr><td class="sub_th">Programming Guides</td><td>The programming guide includes an overview, a Hello Pass application, and feature descriptions.</td></tr></tbody></table>
  

## Change History

*   [Samsung Pass SDK 1.2.6 (Sept 14, 2017)](http://developer.samsung.com/galaxy/pass/releases#)
    *   DEVICE\_FINGERPRINT\_UNIQUE\_ID, getRegisteredFingerprintUniqueID() are deprecated.
    
*   [Samsung Pass SDK 1.2.2 (Oct 27, 2016)](http://developer.samsung.com/galaxy/pass/releases#)
    *   Modify a description for "Return Value" in the Programming Guide.
    *   Change the logging functions.
    
*   [Samsung Pass SDK 1.2.1](http://developer.samsung.com/galaxy/pass/releases#)
    *   Update Sample App for enhanced stability.
    
*   [Samsung Pass SDK 1.2.1](http://developer.samsung.com/galaxy/pass/releases#)
    *   Added APIs as supplementary for identify.
        *   String getGuideForQuailityFailed () for sharing the guide string of the firmware.
        *   void onCompleted () in the IdentifyListener interface for notifying completion of identify.
        *   the event STATUS\_OPERATION\_DENIED is added which is sent through onFinished API.
    *   Added APIs for customizing UI.
        *   void setDialogButton ()
        *   void setStandbyString ()
    
*   [Samsung Pass SDK 1.2.0](http://developer.samsung.com/galaxy/pass/releases#)
    *   The API _isFeatureEnabled (DEVICE\_FINGERPRINT\_AVAILABLE\_PASSWORD)_ is added because some projects do not support the backup password of fingerprint on Android M(6.0) OS platform.
        *   The parameter _enablePassword_ in startIdentifyWithDialog() is only effective when calling the API isFeatureEnabled (DEVICE\_FINGERPRINT\_AVAILABLE\_PASSWORD) and the return value is true.
    *   The following Broadcasts are added:
        *   ACTION\_FINGERPRINT\_RESET is broadcasted when all registered fingerprint are removed.
        *   ACTION \_FINGERPRINT\_REMOVED is broadcasted when a specific registered fingerprint is removed.
        *   ACTION \_FINGERPRINT\_ADDED is broadcasted when a registered fingerprint is added.
    
*   [Samsung Pass SDK 1.1.4](http://developer.samsung.com/galaxy/pass/releases#)
    *   Samsung Pass SDK 1.1.4 has been released with enhanced stability.
    
*   [Samsung Pass SDK 1.1.3](http://developer.samsung.com/galaxy/pass/releases#)
    *   Samsung Pass SDK 1.1.3 has been released with enhanced stability for Swipe and Touch Fingerprint Sensor.
    
*   [Samsung Pass SDK 1.1.2](http://developer.samsung.com/galaxy/pass/releases#)
    *   Samsung Pass SDK 1.1.2 has been released with enhanced stability for Touch Fingerprint Sensor.
    
*   [Samsung Pass SDK 1.1.1](http://developer.samsung.com/galaxy/pass/releases#)
    *   Improved the API _isFeatureEnabled (DEVICE\_FINGERPRINT\_UNIQUE\_ID)_ is improved for returning exact value to the project which doesn’t support this API even though upgrading Android L OS platform.
        *   For checking whether this API is supported in the device, the API isFeatureEnabled (DEVICE\_FINGERPRINT\_UNIQUE\_ID) must be called before calling getRegisteredFingerprintUniqueID().
    
*   [Samsung Pass SDK 1.1.0](http://developer.samsung.com/galaxy/pass/releases#)
    *   Samsung Pass SDK 1.1.0 has been released with enhanced stability and better performance.
    
*   [Samsung Pass SDK 1.1.0 beta](http://developer.samsung.com/galaxy/pass/releases#)
    *   Added APIs as supplementary for Identify.
        *   int getIdentifiedFingerprintIndex()
        *   SparseArray<String> getIdentifiedFingerprintIndex()
        *   SparseArray<String> getRegisteredFingerprintUniqueID()
        *   void setIntendedFingerprintIndex()
    *   Added APIs for customizing UI.
        *   void setDialogTitle()
        *   void setDialogIcon()
        *   void setDialogBgTransparency()
        *   void setCanceledOnTouchOutside()
    
*   [Samsung Pass SDK 1.0.0 beta](http://developer.samsung.com/galaxy/pass/releases#)
    *   The Pass is separated from the Samsung Mobile SDK to make an efficient environment for the application development.
    *   New permission is required when you initialize Pass. Refer to programming guide for more information.
    

## Features

*   Request fingerprint recognition.
*   Cancel fingerprint recognition requests.
*   Verify whether the fingerprint of the current user matches the fingerprint registered on the device.
*   Register fingerprints through the Enroll screen of the Setting menu.
*   Get the index of the identified fingerprint from the array of registered fingerprints.
*   Set the index of the fingerprint for recognition requests.
*   Add a title for the user interface.
*   Add a logo icon for the user interface.
*   Set the transparency of elements outside the user interface.
*   Set whether the user interface is dismissed or not when touching elements outside it.
*   Get broadcasts from the device when registered fingerprint is changed.
*   Get the guide string of the reason when fingerprint recognition is failed due to the poor quality.
*   Add a own button for the user interface.
*   Change a standby string for the user interface.